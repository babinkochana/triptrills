 <?php
/**
 * Template Name: verinite-blog
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<div id="content" class="site-content content ">

    
	<div id="primary" class="page-fullwidth content-area">
		<main id="main" class="site-main " role="main">

			
				<article id="post-6098" class="post-6098 page type-page status-publish hentry">
		<div class="entry-content">
		<section style="background-image: url('http://codewave.co.in/Verinite/wp-content/uploads/2017/02/testi-bg.jpg'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1489750228574"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p class="blog-page-title">BLOG</p>

		</div>
	</div>
</div></div></div></div></section><section style="padding-top: 60px; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">            
            <div class="blog blog-post shortcode-blog-post standard-layout-post has-sidebar">
				<div class="blog-container " data-dir="http://codewave.co.in/Verinite/wp-content/themes/Verinite">
					<div class="row">
						<div class="col-sm-12 col-md-8 blog-post-content">
							<div class="content-area">
								<div class="site-main" role="main"><!--  carousel-posts -->

								
									
<article id="post-6435" class="post-6435 post type-post status-publish format-standard hentry category-events">
<?php 
                         // query
						 
                        $args = array(
						'post_type'   => 'blog_posts'
          /* 'tax_query' => array(
         array(
          'taxonomy' => 'case_studies_category',
          'field' => 'slug',
           'terms' => 'card-and-payments'
       
         )
		 
        ) */ 
       );
	 
       query_posts($args);

	
       ?>
	   <?php while ( have_posts() ) : the_post(); ?>
			<?php $terms = get_the_terms( get_the_ID(), 'blog_categories');
	   if( !empty($terms) ) {
	
	$term = array_pop($terms);
	// do something with $custom_field
	 // print_r($term);
} 
			?>
		<div class="entry-meta">
		<span class="cat-links blog_cat_links"><a href="http://codewave.co.in/Verinite/category/events/" rel="category tag">Events</a></span>		<span class="posted-on blog_posted_on"><a href="http://codewave.co.in/Verinite/event3/" rel="bookmark"><time class="entry-date published" datetime="2017-03-20T12:05:19+00:00">20th March 2017</time><time class="updated" datetime="2017-03-20T17:50:33+00:00">20th March 2017</time></a></span>		<span class="byline"> by <span class="author vcard"><a class="url fn n" href="http://codewave.co.in/Verinite/author/verinite/">Verinite</a></span></span>	</div><!-- .entry-meta -->
	
	<h3 class="entry-title verinite-blog-title"><a href="http://codewave.co.in/Verinite/event3/" rel="bookmark">Event3</a></h3>
	
	<div class="entry-content verinite-blog-post">
		<h5>Hurray!! Verinite signs up with one of a Top 5 Indian Retail Bank for their business critical technology rollout!!</h5>
<p>David and Goliath story reprises again as Verinite literally clinched a deal from mighty banking technology service providers and hence is entrusted by one of a top 5 Indian retail Bank to own up their impending credit cards launch.</p>
<p>The client (Bank) is a veteran Indian retail biggie who would want to foray into credit cards business and hence thoroughly evaluated Verinite (a flourishing start-up) against leading Banking technology service providers. Verinite stood tall and was chosen amongst all mammoths for its outmatched and successful track record to own and deliver winning card system engagements across industry leading card platforms.<br>
Undoubtedly, Verinite is turning out to be a huge contender for leading Banking technology service players in market today, especially for technology transformation engagements, as it is proving handy in Consulting as well Technology rollouts and is been delivering value proposition via its affiliation. Testimony to which, now Big Banks along small to mid-sized are also seeing a trusted technology partner in Verinite.<br>
We are glad to be associated with such a Bank who not only entrusted in our values and differentiated proposition but also honored our persistence hard work and perseverance.</p>
<style>
.call-to-action{display:none;}.author-bio{display:none;}#comments{display:none;}.blog-post{margin: 50px 0px 20px 0px;}.post-navigation{padding-bottom:0px!important;margin-bottom:30px!important;}<br /></style>
			</div><!-- .entry-content -->
	<footer class="entry-footer">
		        	        <div class=" post-sharing-buttons text-center">
		        <ul class="list-inline">
		            		                <li><a href="http://www.facebook.com/sharer.php?u=http://codewave.co.in/Verinite/event3//&amp;t=Event3"><i class="fa fa-facebook"></i></a></li>
		            		            		                <li><a href="https://twitter.com/intent/tweet?text=Event3-http://codewave.co.in/Verinite/event3/"><i class="fa fa-twitter"></i></a></li>
		            		        	             
		                <li><a href="https://plus.google.com/share?url=http://codewave.co.in/Verinite/event3/"><i class="fa fa-google-plus"></i></a></li>
		            		                         
		                <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://codewave.co.in/Verinite/event3/&amp;title=Event3"><i class="fa fa-linkedin"></i></a></li>
		            		        </ul>
		    </div>
	        </footer><!-- .entry-footer -->
			<?php endwhile; ?></ul>
										<?php wp_reset_query();?>
</article>

								
								</div><!-- #main -->
								<!-- Post Navigation -->
															</div><!-- #primary -->
						</div>
													<div class="col-sm-12 col-md-4 blog-post-sidebar">
								
<div id="secondary" class="widget-area sidebar" role="complementary">
	<aside id="text-3" class="widget widget_text">			<div class="textwidget"><p class="sidebar-subscribe-title">Subscribe To Blog Via Email</p>
<div role="form" class="wpcf7" id="wpcf7-f6195-p6098-o1" lang="en-GB" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/Verinite/blog/#wpcf7-f6195-p6098-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="6195">
<input type="hidden" name="_wpcf7_version" value="4.7">
<input type="hidden" name="_wpcf7_locale" value="en_GB">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6195-p6098-o1">
<input type="hidden" name="_wpnonce" value="825c5d1a78">
</div>
<div class="row cookie-contact-form-2">
<div class="col-sm-12 col-md-12">
<span class="wpcf7-form-control-wrap email-117"><input type="email" name="email-117" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email sidebar-email-field" aria-invalid="false" placeholder="Email Address"></span><br>
<input type="submit" value="Subscribe" class="wpcf7-form-control wpcf7-submit sidebar-subscribe-button"><span class="ajax-loader"></span>
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div>
		</aside><aside id="search-2" class="widget widget_search"><form role="search" method="get" id="searchform" class="search-form" action="http://codewave.co.in/Verinite/">
			<label> <span class="screen-reader-text">Search for:</span>
			<input type="search" title="Search for:" value="" placeholder="Search" name="s" id="s" class="search-field"></label>
			<input type="submit" id="searchsubmit" class="search-submit" value="">
		</form></aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<h4 class="widget-title">Recent Posts</h4>		<ul>
					<li>
				<a href="http://codewave.co.in/Verinite/news4/">David and Goliath story reprises</a>
						</li>
					<li>
				<a href="http://codewave.co.in/Verinite/news3/">Verinite turned 4 year old</a>
						</li>
					<li>
				<a href="http://codewave.co.in/Verinite/news2/">Verinite is once again chosen for</a>
						</li>
					<li>
				<a href="http://codewave.co.in/Verinite/news1/">God does not give special kids</a>
						</li>
				</ul>
		</aside>		<aside id="recent-comments-2" class="widget widget_recent_comments"><h4 class="widget-title">Recent Comments</h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link">thirumani02</span> on <a href="http://codewave.co.in/Verinite/product/less/#comment-8">Less</a></li><li class="recentcomments"><span class="comment-author-link">thirumani02</span> on <a href="http://codewave.co.in/Verinite/product/klenfield/#comment-7">Klenfield</a></li><li class="recentcomments"><span class="comment-author-link">thirumani02</span> on <a href="http://codewave.co.in/Verinite/product/crick/#comment-6">Crick</a></li><li class="recentcomments"><span class="comment-author-link">thirumani02</span> on <a href="http://codewave.co.in/Verinite/product/bateman/#comment-5">Bateman</a></li><li class="recentcomments"><span class="comment-author-link">thirumani02</span> on <a href="http://codewave.co.in/Verinite/product/babbit/#comment-4">Babbit</a></li></ul></aside><aside id="archives-2" class="widget widget_archive"><h4 class="widget-title">Archives</h4>		<ul>
			<li><a href="http://codewave.co.in/Verinite/2017/03/">March 2017</a></li>
		</ul>
		</aside><aside id="categories-2" class="widget widget_categories"><h4 class="widget-title">Categories</h4>		<ul>
	<li class="cat-item cat-item-52"><a href="http://codewave.co.in/Verinite/category/news/">News</a>
</li>
		</ul>
</aside></div><!-- #secondary -->
							</div>
											</div>
				</div>
			</div>      
		</div></div></div></div></section>
			</div><!-- .entry-content -->
</article>


				
			
		</main><!-- #main -->
	</div><!-- #primary -->
	</div>

<?php get_footer(); ?>
