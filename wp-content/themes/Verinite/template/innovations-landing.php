<?php
/**
 * Template Name: innovations-landing
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<div id="content" class="site-content content ">
    
	<div id="primary" class="page-fullwidth content-area">
		<main id="main" class="site-main " role="main">

			
				<article id="post-6276" class="post-6276 page type-page status-publish hentry">
		<div class="entry-content">
		<section class="banner-image" style="background-image: url('http://codewave.co.in/verinite/wp-content/uploads/2017/01/bg-2.jpg'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; "><div class="container"><div class="vc_row vc_row_fluid  vc_row-flex  vc_row-o-full-height  vc_row-o-columns-middle  " style="min-height: 77.5342vh;"><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1485966760599">
		<div class="wpb_wrapper">
			<p style="color: white; font-size: 30px; font-family: 'Open Sans';">Elevating the livelihoods of the drum makers, enabling survival</p>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1486042853179">
		<div class="wpb_wrapper">
			<p style="color: white; font-size: 30px; font-family: 'Open Sans';">of their craft of creating intricate carvings on the percussion instruments.</p>
<div style="width: 100%; float: left;">
<div class="col-sm-2 banner-list first">
<div class="banner-icon"><img src="http://codewave.co.in/verinite/wp-content/uploads/2017/02/identification-2.png"></div>
<div class="banner-text">
<p class="banner-heading">IDENTIFICATION</p>
<p class="banner-sub-heading">Lorem ipsum dolor sit amet, consectetur</p>
</div>
</div>
<div class="col-sm-2 banner-list">
<div class="banner-icon"><img src="http://codewave.co.in/verinite/wp-content/uploads/2017/02/link.png"></div>
<div class="banner-text">
<p class="banner-heading">THE MODEL</p>
<p class="banner-sub-heading">Lorem ipsum dolor sit amet, consectetur</p>
</div>
</div>
<div class="col-sm-2 banner-list">
<div class="banner-icon"><img src="http://codewave.co.in/verinite/wp-content/uploads/2017/02/flash.png"></div>
<div class="banner-text">
<p class="banner-heading">IMPACT</p>
<p class="banner-sub-heading">Lorem ipsum dolor sit amet, consectetur</p>
</div>
</div>
<div class="col-sm-2">
<div class="banner-icon"><img src="http://codewave.co.in/verinite/wp-content/uploads/2017/02/replication.png"></div>
<div class="banner-text">
<p class="banner-heading">REPLICATION</p>
<p class="banner-sub-heading">Lorem ipsum dolor sit amet, consectetur</p>
</div>
</div>
</div>

		</div>
	</div>
</div></div></div></div></section><section style="padding-top: 70px; padding-bottom: 60px; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><blockquote class="quote blockquote" style="color:#6d6d6d "><p style="text-align: center; line-height: 2em; font-size: 18px;"><em>The Dholak-Waale are an under-served community living in Thannisandra Bangalore who are traditional drum-makers for the past 500 years. A community of Mirasis, originally from Uttar Pradesh, the Dholak-Waale specialize in creating beautiful carvings on delicately crafted percussion instruments, a skill that has sustained across multiple generations. The art is now closer &nbsp;to an epitaph and needs to be preserved. They are an urban, nomadic, artisan community in an informal urban slum.</em></p>
</blockquote></div></div></div></div></section><section style="background-color: #f9f9f9; padding-top: 80px; padding-bottom: 100px; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="tabs wpb_tabs wpb_content_element  tabs-style nav-tabs-style-1 text-center" data-interval="">
			<ul class="nav nav-tabs list-inline h5 wpb_tour_tabs_wrapper ui-tabs ui-widget ui-widget-content ui-corner-all"><li class="active"><a data-toggle="tab" href="#tab-ab3d1e47-2f18-9">PROBLEMS</a></li><li class=""><a data-toggle="tab" href="#tab-5683ec0b-9053-5">SOLUTION</a></li><li class=""><a data-toggle="tab" href="#tab-1485941222667-2-1">IMPACT</a></li></ul>

			<div class=" tab-content">
			<div id="tab-ab3d1e47-2f18-9" class="tab-pane fade active in">
				<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-4 text-center"><div class="wpb_wrapper"><div class=" agni-image custom-image-container text-center"><div class="wpb_wrapper"><a><img style="" width="315" height="217" src="http://codewave.co.in/verinite/wp-content/uploads/2017/01/1-2-1.jpg" class="fullwidth-image attachment-large  attachment-large" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2017/01/1-2-1.jpg 315w, http://codewave.co.in/verinite/wp-content/uploads/2017/01/1-2-1-300x207.jpg 300w" sizes="(max-width: 315px) 100vw, 315px"></a></div></div></div></div><div class="wpb_column agni_column_container vc_col-sm-8 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p style="line-height: 2em; font-weight: 600; font-size: 13px; margin: 0 0 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
<p style="line-height: 2em; font-weight: 600; font-size: 13px; margin: 0 0 10px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
<div style="width: 50%; float: left;">
<ul class="inner-list">
<li class="custom-list-style">Lorem Ipsum is text</li>
<li class="custom-list-style">Lorem Ipsum is simply dummy text</li>
</ul>
</div>
<div style="width: 50%; float: left;">
<ul>
<li class="custom-list-style">Lorem Ipsum is text</li>
<li class="custom-list-style">Lorem Ipsum is simply dummy text</li>
</ul>
</div>

		</div>
	</div>
</div></div></div>
			</div> 
			<div id="tab-5683ec0b-9053-5" class="tab-pane fade  in">Empty tab. Edit page to add content here.
			</div> 
			<div id="tab-1485941222667-2-1" class="tab-pane fade  in">Empty tab. Edit page to add content here.
			</div> 
		</div>
	</div> </div></div></div></div></section><section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1482472992046" style="padding-top:60px;"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: 'Open Sans';font-weight: 600;">MORE STORIES</h2>


		</div>
	</div>
</div></div></div></div></section>
<section   ><div class="container-fluid verinite_videos" style="padding-bottom:60px;"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video1"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/Day7leNML_E?list=PLI10qW5Dz41oWI4T-S665l0zFhjKrnuMP" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>


	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div>
<div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video2"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/hI_CO00URYM" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video3"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/tyseiA7FIMI" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video4"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/z0DNQeYmygY?list=PLI10qW5Dz41r0ahpx7_emQ876FBHMi9-O" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div></div></div></section>
<section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class=" call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">HOW CAN YOU GET INVOLVED?</h4><p class="call-to-action-additonal">Join the verinite community and give back as an intern, volunteer, scientist or an investor</p></div><a class="btn  btn-primary btn-sm call-to-action-button" target="_self" href="#" style="margin-top: 10px; "> GET IN TOUCH</a></div></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main><!-- #main -->
	</div><!-- #primary -->
	</div>
	<?php get_footer(); ?>