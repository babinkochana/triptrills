 <?php
/**
 * Template Name: association-affiliation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<style>
html {
	overflow-x:hidden;
}
</style>
<link rel='stylesheet' id='js_composer_front-css'  href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>" type='text/css' media='all' />

<div id="primary" class="page-fullwidth content-area">
		<main id="main" class="site-main " role="main">
			<article id="post-4717" class="post-4717 page type-page status-publish hentry">
		<div class="entry-content">
		<section style="background-image: url('../wp-content/uploads/2017/02/banner-bg.jpg'); background-repeat:repeat; background-size:cover; background-position:left top; background-attachment:scroll; padding-top: 65px; padding-bottom: 70px; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p class="assoc-banner-title">We're looking for mutually valuable partnerships that will enhance<br class="hide-break">
our capabilities to serve our customers better</p>
<div style="width: 100%; float: left; margin-bottom: 30px;">
<div class="col-md-3 col-sm-4 col-xs-12 no-padding-left">
<div class="assoc-banner-icon"><img src="../wp-content/uploads/2017/02/factory.png"></div>
<div class="assoc-banner-text">
<p class="assoc-banner-heading">Business or Technology<br>
partnership</p>
</div>
</div>
<div class="col-md-3 col-sm-4 col-xs-12 partner-mobile-banner-responsive">
<div class="assoc-banner-icon"><img src="../wp-content/uploads/2017/02/in.png"></div>
<div class="assoc-banner-text">
<p class="assoc-banner-heading">Industry<br>
Affiliation</p>
</div>
</div>
</div>

		</div>
	</div>
<div class="banner-button text-left page-scroll"><a class="btn btn-alt btn-white btn-lg" target="_self" href="#partner_get_in_touch" style="margin-top: 15px; margin-bottom: px; margin-left: px; margin-right: px; border-radius:2px;">GET IN TOUCH</a></div></div></div></div></div></section><section><div class="container"><div class="vc_row vc_row_fluid     "><div class="assoc_tab_main wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="tabs wpb_tabs wpb_content_element  partnership_tabs nav-tabs-style-1 text-left" data-interval="">
			<ul class="nav nav-tabs list-inline h5 wpb_tour_tabs_wrapper ui-tabs ui-widget ui-widget-content ui-corner-all partnership-tab-ul"><li class="active"><a href="" data-toggle="tab" data-category="all" class="partnership_category">All</a></li><li class=""><a href="" data-toggle="tab" data-category="Business or Technology" class="partnership_category">Business or technology partnership</a></li><li class=""><a href="" data-toggle="tab" data-category="Industry Affiliation" class="partnership_category">Industry Affiliation</a></li></ul>

			<div class=" tab-content">
			<div class="tab-pane fade active in">
			<?php 
                         // query
						 
                        $args = array(
						'post_type'   => 'association'
          /* 'tax_query' => array(
         array(
          'taxonomy' => 'case_studies_category',
          'field' => 'slug',
           'terms' => 'card-and-payments'
       
         )
		 
        ) */ 
       );
	 
       query_posts($args);

	
       ?>
	    <ul style="list-style:none; padding-left:0px;"> 
	   <?php if (have_posts()) : while(have_posts()) : the_post(); $i++; if(($i % 2) == 0) :  ?>
			<?php $terms = get_the_terms( get_the_ID(), 'association_categories');
	   if( !empty($terms) ) {
	
	$term = array_pop($terms);
	// do something with $custom_field
	// print_r($term);
} 
			?>
			
			<li class="verinite_association" data-category="<?php echo $term->name; ?>"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-md-8 vc_col-sm-7 text-left assoc-right-text"><div class="wpb_wrapper">
				<div class="wpb_text_column wpb_content_element ">
					<div class="wpb_wrapper">
						<p class="assoc_post_heading"><?php the_title();?></p>
						<?php the_content();?>
					</div>
				</div>
			</div>
			</div>
			<div class="wpb_column agni_column_container vc_col-md-4 vc_col-sm-5 text-left"><div class="wpb_wrapper"><div class=" agni-image custom-image-container text-right"><div class="wpb_wrapper"><a>
											<?php 
												if ( has_post_thumbnail() ) {
												the_post_thumbnail();
											}  ?></a></div></div></div></div></div></li>
											
			<?php else : ?>
			<?php $terms = get_the_terms( get_the_ID(), 'association_categories');
	   if( !empty($terms) ) {
	
	$term = array_pop($terms);
	// do something with $custom_field
	// print_r($term);
} 
			?>
			 
			<li class="verinite_association" data-category="<?php echo $term->name; ?>"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-md-4 vc_col-sm-5 text-left"><div class="wpb_wrapper"><div class="animate animated fadeInLeft" data-animation-offset="100%" data-animation="fadeInLeft" style="animation-duration: 0.8s; animation-delay: 0s; visibility: visible;"><div class=" agni-image custom-image-container text-left"><div class="wpb_wrapper"><a>
											<?php 
												if ( has_post_thumbnail() ) {
												the_post_thumbnail();
											}  ?></a></div></div></div></div></div><div class="wpb_column agni_column_container vc_col-md-8 vc_col-sm-7 text-left assoc-left-text"><div class="wpb_wrapper"><div class="animate animated fadeInRight" data-animation-offset="100%" data-animation="fadeInRight" style="animation-duration: 0.8s; animation-delay: 0s; visibility: visible;">
				<div class="wpb_text_column wpb_content_element ">
					<div class="wpb_wrapper">
						<p class="assoc_post_heading"><?php the_title();?></p>
						<?php the_content();?>
					</div>
				</div>
			</div></div></div></div></li>
											<?php endif; endwhile; endif; ?></ul>
										<?php wp_reset_query();?>		 
			 </div>
			 
		</div>
	</div> </div></div></div></div></section><section id="partner_get_in_touch"><div class="container"><div class="vc_row vc_row_fluid     "><div class="assoc_contact_form wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p class="contact_partnerships">Contact for Partnerships</p>
<p class="partnership_sub_heading">Send us a message, one of us will get in touch within two business days.</p>

		</div>
	</div>
<div role="form" class="wpcf7" id="wpcf7-f5404-p4717-o1" lang="en-GB" dir="ltr">
<div class="screen-reader-response"></div>
<form action="<?php  echo get_site_url().'/partnership/#wpcf7-f5404-p4717-o1'?>" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="5404">
<input type="hidden" name="_wpcf7_version" value="4.7">
<input type="hidden" name="_wpcf7_locale" value="en_GB">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5404-p4717-o1">
<input type="hidden" name="_wpnonce" value="a7bd18a1c3">
</div>
<div class="row cookie-contact-form-2">
<div class="col-sm-12 col-md-12">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="col-md-6 col-sm-6 col-xs-12 partner-left">
                  <span class="wpcf7-form-control-wrap contact_name"><input type="text" name="contact_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required partnerform-font" aria-required="true" aria-invalid="false" placeholder="Name"></span>
             </div>
<div class="col-md-6 col-sm-6 col-xs-12 partner-right">
                  <span class="wpcf7-form-control-wrap contact_email"><input type="email" name="contact_email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email partnerform-font" aria-required="true" aria-invalid="false" placeholder="Email"></span>
             </div>
<div class="col-md-6 col-sm-6 col-xs-12 partner-left">
                  <span class="wpcf7-form-control-wrap contact_number"><input type="tel" name="contact_number" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel partnerform-font" aria-required="true" aria-invalid="false" placeholder="Mobile"></span>
             </div>
<div class="col-md-6 col-sm-6 col-xs-12 partner-right">
                  <span class="wpcf7-form-control-wrap contact_company"><input type="text" name="contact_company" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required partnerform-font" aria-required="true" aria-invalid="false" placeholder="Company"></span>
             </div>
<div class="col-md-6 col-sm-6 col-xs-12 partner-left">
                  <span class="wpcf7-form-control-wrap contact_job"><input type="text" name="contact_job" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required partnerform-font" aria-required="true" aria-invalid="false" placeholder="Job Title/Designation"></span>
             </div>
<div class="col-md-6 col-sm-6 col-xs-12 partner-right">
                  <span class="wpcf7-form-control-wrap contact_rel"><input type="text" name="contact_rel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required partnerform-font" aria-required="true" aria-invalid="false" placeholder="Relationship with Verinite"></span>
             </div>
<div class="col-md-12 col-sm-12 col-xs-12 partners-full">
                 <span class="wpcf7-form-control-wrap contact_msg"><grammarly-ghost spellcheck="false"><div style="position: absolute; color: transparent; overflow: hidden; white-space: pre-wrap; border-radius: 0px; box-sizing: border-box; height: 132px; width: 1110px; z-index: 0; border-width: 0px 0px 1px; padding: 30px 0px 5px; margin: -152px 0px 35px; border-style: solid; background: none 0% 0% / auto repeat scroll padding-box border-box rgba(0, 0, 0, 0); border-color: transparent !important; top: 0px; left: 0px;" data-id="18e51279-3b78-b759-77ed-9ebb19413407" data-gramm_id="18e51279-3b78-b759-77ed-9ebb19413407" data-gramm="gramm" data-gramm_editor="true" class="gr_ver_2" data-grammarly-reactid=".5" contenteditable="true"><span style="display:inline-block;line-height:24px;color:transparent;overflow:hidden;text-align:left;float:initial;clear:none;box-sizing:border-box;vertical-align:baseline;white-space:pre-wrap;width:100%;margin:0;padding:0;border:0;font:normal normal normal normal 15px / 24px Lato, Helvetica, Arial, sans-serif;font-size:15px;font-family:Lato, Helvetica, Arial, sans-serif;letter-spacing:0.3px;text-shadow:none;height:131px;" data-grammarly-reactid=".5.0"></span><br data-grammarly-reactid=".5.1"></div></grammarly-ghost><textarea name="contact_msg" cols="40" rows="4" class="wpcf7-form-control wpcf7-textarea partnerform-font" aria-invalid="false" placeholder="Message" data-gramm="true" data-txt_gramm_id="18e51279-3b78-b759-77ed-9ebb19413407" data-gramm_id="18e51279-3b78-b759-77ed-9ebb19413407" spellcheck="false" data-gramm_editor="true" style="z-index: auto; position: relative; line-height: 24px; font-size: 15px; transition: none; background: transparent !important;"></textarea><grammarly-btn><div style="z-index: 2; opacity: 1; transform: translate(1080px, -51px);" class="_e725ae-textarea_btn _e725ae-show _e725ae-field_hovered" data-grammarly-reactid=".0"><div class="_e725ae-transform_wrap" data-grammarly-reactid=".0.0"><div title="Protected by Grammarly" class="_e725ae-status" data-grammarly-reactid=".0.0.0">&nbsp;</div></div></div></grammarly-btn></span>
             </div></div>
<div class="col-sm-12" style="text-align:center;">
    <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit partnership_submit"><span class="ajax-loader"></span>
</div>
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form>
</div></div></div></div></div></section>
<section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class="cta call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">LOOKING FOR A BUSINESS PARTNERSHIP?</h4><p class="call-to-action-additonal">Send us a message, one of us will get in touch within two business days.</p></div><a class="btn btn-alt btn-white  call-to-action-button" target="_self" href="<?php  echo get_site_url().'/contact_us/'?>"> GET IN TOUCH</a></div></div></div></div></div></section>
			</div><!-- .entry-content -->
</article>

				
			
		</main><!-- #main -->
	</div>
<?php get_footer(); ?>
<script>
	jQuery(".partnership_category").click(function() {
  var selectedCategory = jQuery(this).attr("data-category");
  jQuery(".verinite_association").removeClass('showThisItem');
  jQuery(".verinite_association").addClass('hideThisItem');
	
  jQuery( "li.verinite_association" ).each(function() { 
    var dday=jQuery(this).attr("data-category");
	console.log(selectedCategory);
	console.log(dday);
	
      if( dday == selectedCategory ){
		  // jQuery('.leading-and-leasing').show();
		  // jQuery('[data-category="Leading & Leasing"]').show();
      jQuery(this).addClass('showThisItem');
      jQuery(this).removeClass('hideThisItem');     
    }
	if( selectedCategory == 'all' ) {
		jQuery(".verinite_association").removeClass('hideThisItem');
	}
	});
  });
</script>


