<?php
/**
 * Template Name: sample
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>


 <?php 
                         // query
                        $args = array(
         'post_type'   => 'case_studies',
         'tax_query' => array(
         array(
          'taxonomy' => 'case_studies_category',
          'field' => 'slug',
           'terms' => 'card-and-payments'
       
         )
        )
       );
       query_posts($args);
       ?>
                              
                    <?php while ( have_posts() ) : the_post(); ?>
					<div class="portfolio-thumbnail">
		                                    <?php 
        if ( has_post_thumbnail() ) {
         the_post_thumbnail();
          }  ?></div>
					<p><?php the_title_attribute();?></p>
					
					 <?php endwhile; ?>
                 <?php wp_reset_query();?>
					

<?php get_footer(); ?>
