 <?php
/**
 * Template Name: case studies
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<link rel='stylesheet' id='js_composer_front-css'  href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>" type='text/css' media='all' />

<div id="primary" class="page-fullwidth content-area">
		<main id="main" class="site-main " role="main">

			
				<article id="post-4721" class="post-4721 page type-page status-publish hentry">
		<div class="entry-content">
		<section style="background-image: url('../wp-content/uploads/2017/02/testi-bg.jpg'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; padding-top: 70px; padding-bottom: 60px; "><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="case_study_title">
<p class="casestudies_brochures_title">CASE STUDIES</p>
</div>

		</div>
	</div>
</div></div></div></div></section><section class="case_study_portfolio" style="margin-top: 40px; margin-bottom: 40px; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">        			    <div class="page-portfolio  shortcode-page-portfolio content-area" data-magnific-preloader-choice="1" data-magnific-prev="Previous" data-magnific-next="Next">
		        <div class="page-portfolio-container container-fluid  site-main" role="main" data-dir="../wp-content/themes/Verinite">
		            <div class="portfolio-filter container-fluid case-studies-full text-right"><ul id="filters" class="filter list-inline case-studies-filter"><li class="mobile_list"><a class="case_studies_button first-filter active" data-category="all" data-filter="" title="">All</a></li><li class="mobile_list"><a class="case_studies_button" data-category="Card and Payments" data-filter="" title="">Card &amp; Payments</a></li><li class="mobile_list"><a class="case_studies_button" data-category="Leading & Leasing" data-filter="" title="">Leading &amp; Leasing</a></li><li class="mobile_list last-filter-list"><a class="case_studies_button last-filter" data-category="Retail Banking" data-filter="" title="">Retail Banking</a></li></ul></div>		            <div class="row portfolio-container portfolio-fullwidth  has-infinite-scroll " data-grid="fitRows" style="position: relative; height: 828px;">
		                <?php 
                         // query
						 
                        $args = array(
						'post_type'   => 'case_studies'
          /* 'tax_query' => array(
         array(
          'taxonomy' => 'case_studies_category',
          'field' => 'slug',
           'terms' => 'card-and-payments'
       
         )
		 
        ) */ 
       );
	 
       query_posts($args);

	
       ?>
	    <ul style="list-style:none;"> 
	   <?php while ( have_posts() ) : the_post(); ?>
			<?php $terms = get_the_terms( get_the_ID(), 'case_studies_category');
	   if( !empty($terms) ) {
	
	$term = array_pop($terms);
	// do something with $custom_field
	// print_r($term);
} 
			?>
						<li class="case_studies_cat" data-category="<?php echo $term->name; ?>"> <div class="col-xs-12 col-sm-6 col-md-3 portfolio-column width1x all cards-payment leading-leasing all portfolio-hover-style-9 custom_width_portfolio" style="position: absolute; left: 0px; top: 0px;">
		                            		                            <div class="portfolio-post">
																		<?php $url=get_post_meta(get_the_ID(), 'url_label', TRUE); ?>
		                                <a href="<?=$url;?>" target="_blank"><div class="portfolio-thumbnail">
		                                    <?php 
												if ( has_post_thumbnail() ) {
												the_post_thumbnail();
											}  ?></div></a>
		                                			                               <a href="<?php echo get_post_meta(get_the_ID(), 'url_label', TRUE); ?>" target="_blank"><div class="pdf_title_section"><img src='../wp-content/uploads/2017/02/pdf-icon.png' class="pdf_icon" style="float:left;"><p class="pdf_title">&nbsp;&nbsp;&nbsp;<?php the_title();?></p></div></a>
			                                	                                    		                            </div>
		                            		                        </div></li>
																	<?php endwhile; ?></ul>
										<?php wp_reset_query();?>
										</div>
		            		        </div><!-- #main -->
		    </div><!-- #primary --> 
							
		</div></div></div></div></section><section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class="cta call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">READY TO GET STARTED?</h4><p class="call-to-action-additonal">Send us a brief note on your requirement, one of us will get in touch with you.</p></div><a class="btn btn-alt btn-white btn-lg call-to-action-button" target="_self" href="<?php  echo get_site_url().'/contact_us'?>" style="margin-top: 50px; "> GET IN TOUCH</a></div></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main><!-- #main -->
	</div>
<?php get_footer(); ?>
<script>
	jQuery(".case_studies_button").click(function() {
  var selectedCategory = jQuery(this).attr("data-category");
  jQuery(".case_studies_cat").removeClass('showThisItem');
  jQuery(".case_studies_cat").addClass('hideThisItem');
	
  jQuery( "li.case_studies_cat" ).each(function() { 
    var dday=jQuery(this).attr("data-category");
	// console.log(selectedCategory);
	// console.log(dday);
	
      if( dday == selectedCategory ){
		  // jQuery('.leading-and-leasing').show();
		  // jQuery('[data-category="Leading & Leasing"]').show();
      jQuery(this).addClass('showThisItem');
      jQuery(this).removeClass('hideThisItem');     
    }
	if( selectedCategory == 'all' ) {
		jQuery(".case_studies_cat").removeClass('hideThisItem');
	}
	});
  });
</script>
