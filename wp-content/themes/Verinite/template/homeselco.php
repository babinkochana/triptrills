 <?php
/**
 * Template Name: homeverinite
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<link rel='stylesheet' id='js_composer_front-css'  href='http://codewave.co.in/verinite/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1' type='text/css' media='all' />

<main id="main" class="site-main " role="main">
	<article id="post-4060" class="post-4060 page type-page status-publish hentry">
		<div class="entry-content">
		<div><?php putRevSlider("sliders")  ?></div>
		<section   style="margin-top: 5%; margin-bottom: 5%; ">
			<div class="container"><div class="vc_row vc_row_fluid     ">
				<div class="wpb_column agni_column_container agni_column vc_col-sm-4 text-left our-vision-img">
					<div class="wpb_wrapper">
						<div class=" agni-image custom-image-container text-left">
							<div class="wpb_wrapper">
								<a><img style="" width="407" height="438" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/infographic.png" class="fullwidth-image attachment-full  attachment-full" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/infographic.png 407w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/infographic.png 279w" sizes="(max-width: 407px) 100vw, 407px" /></a>
							</div>
						</div>
					</div>
				</div>
			<div class="wpb_column agni_column_container agni_column vc_col-sm-6 text-left vc_custom_1482312181128">
				<div class="wpb_wrapper">
					<div class="wpb_text_column wpb_content_element ">
						<div class="wpb_wrapper">
							<h2 style="text-align: left; font-size: 30px; color: #33414c; font-family: Oswald; font-weight: 600;">OUR VISION</h2>
								<p style="text-align: left; font-size: 20px; color: #737373; font-family: Open Sans;width: 130%;">Striving to build an ecosystem for bottom-up, socially inclusive innovations and enterprises to thrive. Bridging gaps in well-being, health, education and livelihoods.</p>
						</div>
					</div>
						<div class="our-button text-left page-scroll "><a class="btn btn-alt btn-default watch-video-button" target="_self" href="#" >Watch Video</a>
						</div>
				</div>
			</div>
</div>
		   </div>

		</section>

<div><?php putRevSlider("CSsliders") ?></div>


<section   style="margin-bottom: 5%; "><div class="container">
	<div class="vc_row vc_row_fluid     ">
		<div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
			<div class="wpb_wrapper">
				<link href="http://fonts.googleapis.com/css?family=Open+Sans%3A600" rel="stylesheet" property="stylesheet" type="text/css" media="all">
				<link href="http://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
				<div id="rev_slider_2_2_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="CSsliders" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

					<div class="vc_row wpb_row vc_inner vc_row-fluid our-work">
						<div class="wpb_column agni_column_container vc_col-sm-12 text-left">
							<div class="wpb_wrapper">
								<div class="wpb_text_column wpb_content_element our-work-section">
									<div class="wpb_wrapper">
										<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald;font-weight: 600;">OUR WORK</h2>
										<p style="text-align: center; color: #585858; font-size: 14px; font-family: Open Sans;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id=" "  >
	<div class="container-fluid">
		<div class="vc_row vc_row_fluid    has-fullwidth-column ">
			<div class="our-process wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
				<div class="wpb_wrapper">        			    
					<div class="page-portfolio  shortcode-page-portfolio content-area" data-magnific-preloader-choice="1" data-magnific-prev="Previous" data-magnific-next="Next">
		        		<div class="page-portfolio-container container-fluid  site-main" role="main" data-dir="http://codewave.co.in/verinite/wp-content/themes/verinite/cookie">
		            <div class="portfolio-filter container-fluid  text-right">
		            	<!-- <span class="filter-button" ><i class="pe-7f-filter"></i></span> -->
		            	<ul id="filters" class="filter list-inline">
		            		<!-- <li>
		            			<a class="active" href="#all" data-filter=".all" title=""></a>
		            		</li> -->
		            		<li>
		            			<a href="#all" data-filter=".all" title="">All</a>
		            		</li>
		            		<li>
		            			<a href="#education" data-filter=".education" title="">Education</a>
		            		</li>
		            		<li>
		            			<a href="#energy" data-filter=".energy" title="">Energy</a>
		            		</li>
		            		<li>
		            			<a href="#health" data-filter=".health" title="">Health</a>
		            		</li>
		            	</ul>
		            </div>		            
		          	<div class="row portfolio-container portfolio-fullwidth  has-infinite-scroll our-work-portfolio" data-grid="fitRows">
		                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all health all portfolio-hover-style-2 portfolio-img ">
		                    <div id="portfolio-post-4141" class="portfolio-post">
		                        <div class="portfolio-thumbnail">
		                            <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                
		                          </div>
		                            <div class="portfolio-caption-content">
			                            <div class="portfolio-content">
			                                <div class="portfolio-content-details">
			                                    <div class="portfolio-icon hide">
			                                    	<a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self">
			                                    		<span></span>
			                                    	</a>
			                                    </div>
			                                        <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self">Education1</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                             <div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                </div>
			                            </div>
			                        </div>
			                                	                                    		                            </div>
		  				</div>
		  				<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education all portfolio-hover-style-2 portfolio-img ">
		                    <div id="portfolio-post-4144" class="portfolio-post">
		                        <div class="portfolio-thumbnail">
		                                    <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/2-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/2-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		                               
		                        </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self"><span></span>
			                                	</a>
			                                </div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self">Education2</a></h5>
			                                <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                    <div class="read_more"><a href="#">Read more</a></div>
			                                </ul>
			                                    <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                    </div>
			                           	</div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		                </div>

		             <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education energy all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4147" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                             <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/3-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/3-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		                                
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self">
			                                	<span></span>
			                                	</a>
			                               	</div>
			                                     	<h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self">Education3</a></h5>
			                                    <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                        <div class="read_more"><a href="#">Read more</a></div>
			                                    </ul>
			                                    <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                    </div>
			                            </div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		            </div>
		            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy health all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4149" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                        <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		      
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self">
			                                	<span></span>
			                                	</a>
			                                </div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self">Education4</a></h5>
			                                    <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                            <div class="read_more"><a href="#">Read more</a></div>
			                                    </ul>
			                                   	<div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                    </div>
			                            </div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		            </div>
		            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4151" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                        <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/5-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/5-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self">
			                                	<span></span>
			                                	</a>
			                                </div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self">Education5</a></h5>
			                                    <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                            <div class="read_more"><a href="#">Read more</a></div>
			                                    </ul>
			                                       	<div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                        </div>
			                           	</div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		            </div>

		            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy health all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4153" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                      	<img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/6-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/6-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />	</div>
		                                			                               

		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self">
			                                	<span></span>
			                                	</a>
			                               	</div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self">Education6</a></h5>
			                                <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
			                                    <div class="read_more"><a href="#">Read more</a></div>
			                                </ul>
			                                   	<div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                    </div>
			                            </div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		            </div>

		            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4155" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                        <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self">
			                                	<span></span></a>
			                                </div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self">Education7</a></h5>
			                                    <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...
													 <div class="read_more"><a href="#">Read more</a></div>
			                                    </ul>
			                                    <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                    </div>
			                            </div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		        	</div>
		        	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education energy all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4157" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                        <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                               
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                <div class="portfolio-icon hide">
			                                	<a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self">
			                                	<span></span>
			                                	</a>
			                                </div>
			                                    <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self">Education8</a></h5>
			                                    <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                    </ul>
			                                    <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                	</div>
			                            </div>
			                        </div>
			                    </div>
			                                	                                    		                            </div>
		        	</div>
		        	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all health all portfolio-hover-style-2 portfolio-img ">
		                <div id="portfolio-post-4159" class="portfolio-post">
		                    <div class="portfolio-thumbnail">
		                    	<img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                
		                    </div>
		                        <div class="portfolio-caption-content">
			                        <div class="portfolio-content">
			                            <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self">Education9</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		        	</div>
		        	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education health all portfolio-hover-style-2 portfolio-img ">
		                            		                            <div id="portfolio-post-4161" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self">Education10</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		            	      
		          		 </div><!-- #main -->
		   		 	</div><!-- #primary --> 
							
					<!-- <div class=" text-center page-scroll"><a class="btn btn-default " target="_self" href="#" style="margin-top: 10px; margin-left: px; margin-right: px; border-radius:2px;">Load More</a> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="process-img"  style="padding-bottom: 9%;"><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1482387453361"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1482478473614">
		<div class="wpb_wrapper" style="    margin-bottom: 7%;">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald;font-weight: 600; margin-top: -21px;">OUR PROCESS</h2>
<p style="text-align: center; font-size: 15px; color: #33414c;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

		</div>
	</div>
<div class=" agni-image custom-image-container text-center"><div class="wpb_wrapper"><a><img style="" width="1063" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/process.png" class="fullwidth-image attachment-full  attachment-full" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/process.png 1063w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/process-300x67.jpg 300w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/process-768x171.jpg 768w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/process-1024x227.jpg 1024w" sizes="(max-width: 1063px) 100vw, 1063px" /></a></div></div></div></div></div></div></section>
<section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1482472992046"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald;font-weight: 600;">STORIES</h2>
<p style="text-align: center; font-size: 15px; color: #33414c;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

		</div>
	</div>
</div></div></div></div></section>
<section   ><div class="container-fluid verinite_videos"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video1"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/Day7leNML_E?list=PLI10qW5Dz41oWI4T-S665l0zFhjKrnuMP" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>


	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div>
<div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video2"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/hI_CO00URYM" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video3"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/tyseiA7FIMI" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left verinite-our-stories-video4"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9 ">
					<!-- <iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
					<iframe width="560" height="315" src="https://www.youtube.com/embed/z0DNQeYmygY?list=PLI10qW5Dz41r0ahpx7_emQ876FBHMi9-O" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element verinite-video-content">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<!-- <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div> -->
<div class="stories_nav">
	<div class="livehood">
		<a>Livehood</a>
	</div>
	<div class="micro-financing">
		<a>Micro Financing</a>
	</div>
</div>

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div></div></div></section>
<section class="call-to-action"  style="margin-top: 1%; padding-top: 5%; padding-bottom: 5%; ">
	<div class="container-fluid">
		<div class="vc_row vc_row_fluid    has-fullwidth-column ">
			<div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
				<div class="wpb_wrapper">
					<div class="call-to-action call-to-action call-to-action-style-2">
						<div class="call-to-action-description">
							<h4 class="call-to-action-heading">HOW CAN YOU GET INVOLVED? </h4>
							<p class="call-to-action-additonal">Join the verinite Community and give back as an intern,<br/>volunteer, scientist or an investor</p>
						</div>
							<a class="btn btn-alt btn-primary btn-lg call-to-action-button" target="_self" href="#" > GET IN TOUCH</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main><!-- #main -->

<?php get_footer(); ?>
