<?php
/**
 * Template Name: testcontact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<main id="main" class="site-main container" role="main">

			
				<article id="post-6642" class="post-6642 page type-page status-publish hentry">
			<header class="entry-header">
			<h1 class="entry-title">testcontact</h1>		</header><!-- .entry-header -->
		<div class="entry-content">
		<section><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div role="form" class="wpcf7" id="wpcf7-f6646-p6642-o1" dir="ltr" lang="en-GB">
<div class="screen-reader-response" role="alert">Thank you for your message. It has been sent.</div>
<form action="/verinite-staging/testcontact/#wpcf7-f6646-p6642-o1" method="post" class="wpcf7-form sent" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input name="_wpcf7" value="6646" type="hidden">
<input name="_wpcf7_version" value="4.7" type="hidden">
<input name="_wpcf7_locale" value="en_GB" type="hidden">
<input name="_wpcf7_unit_tag" value="wpcf7-f6646-p6642-o1" type="hidden">
<input name="_wpnonce" value="7a38b0ac69" type="hidden">
</div>
<p><label> Your Name (required)<br>
    <span class="wpcf7-form-control-wrap your-name"><input name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" type="text"></span> </label></p>
<p><label> Your Email (required)<br>
    <span class="wpcf7-form-control-wrap your-email"><input name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" type="email"></span> </label></p>
<p><label> Subject<br>
    <span class="wpcf7-form-control-wrap your-subject"><input name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" type="text"></span> </label></p>
<p><label> Your Message<br>
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
<p><span class="wpcf7-form-control-wrap file-841"><input name="file-841" size="40" class="wpcf7-form-control wpcf7-file wpcf7-validates-as-required" aria-required="true" aria-invalid="false" type="file"></span></p>
<p><input value="Send" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span></p>
<div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" style="display: block;" role="alert">Thank you for your message. It has been sent.</div></form></div></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main>

<?php get_footer(); ?>