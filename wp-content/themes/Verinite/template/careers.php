<?php
  /**
   * Template Name: careers
   *
   * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
   *
   * @package Agni Framework
   */
  
  get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php  echo get_site_url().'/wp-content/themes/Verinite/css/careers.css'?>">
<link rel='stylesheet' id='js_composer_front-css'  href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>" type='text/css' media='all' />
<div id="primary" class="page-fullwidth content-area">
  <main id="main" class="site-main " role="main">
    <article id="post-4862" class="post-4862 page type-page status-publish hentry">
      <div class="entry-content">
        <section style="background-image: url('<?php  echo get_site_url().'/wp-content/uploads/2017/02/Banner-BG.png'?>'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; ">
          <div class="container">
            <div class="vc_row vc_row_fluid">
              <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                <div class="wpb_wrapper">
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                      <div class="careers_title">
                        <h2 style="text-align: center; margin-top: 37px;">CURRENT OPENINGS</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="career-listing-maindiv">
          <section class="career-listing-subdiv">
            <div class="container">
              <div class="vc_row vc_row_fluid">
              <?php $career_listing= new Wp_Query(array(
              'post_type' => 'careers_listing',
              ));
               ?>

              <?php while($career_listing->have_posts()) : $career_listing->the_post(); ?>
                <div class="career-listing wpb_column agni_column_container agni_column vc_col-sm-6 text-left">
                  <div class="career-listing-sub">
                    <div class="wpb_wrapper">
                      <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="career-listing-detail wpb_column agni_column_container vc_col-sm-8 text-left">
                          <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                              <div class="wpb_wrapper">
                                <p class="career-listing-heading" style="text-transform: uppercase;" title="<?php the_title_attribute(); ?>"><?php the_title_attribute(); ?></p>
                                <p class="career-listing-location" title="<?php the_field('location') ?>"><?php the_field('location') ?></p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="career-listing-button wpb_column agni_column_container vc_col-sm-4 text-left">
                          <div class="wpb_wrapper">
                            <div class=" text-right page-scroll">
                              <a class="btn btn-default " target="_blank" href="<?php echo esc_url( get_permalink($post->ID) ); ?>">Apply now</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endwhile; ?>
              </div>
            </div>
          </section>
        </div>
        <section>
          <div class="container-fluid">
            <div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding ">
              <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                <div class="wpb_wrapper">
                  <div class="cta call-to-action call-to-action-style-2">
                    <div class="call-to-action-description career-calltoaction">
                      <h4 class="call-to-action-heading">LOOKING FOR AN OPPORTUNITY YOU DON'T FIND HERE?</h4>
                      <p class="call-to-action-additonal">Send us your profile. Our recruitment team will get in touch with you.</p>
                    </div>
                    <div class="call-to-action-button-div">
                    <a class="btn  btn-primary  call-to-action-button career-btn" target="_self" href="<?php echo get_site_url().'/career-apply-now/'?>">SEND YOUR PROFILE</a>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!-- .entry-content -->
    </article>
    <!-- #post-## -->
  </main>
  <!-- #main -->
</div>
<?php get_footer(); ?>