<?php
/**
 * Template Name: innovations
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
		<main id="main" class="site-main " role="main">

			
				<article id="post-5844" class="post-5844 page type-page status-publish hentry">
		<div class="entry-content">
		<section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><link href="http://fonts.googleapis.com/css?family=Oswald%3A700" rel="stylesheet" property="stylesheet" type="text/css" media="all"><link href="http://fonts.googleapis.com/css?family=Open+Sans%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div class="forcefullwidth_wrapper_tp_banner" id="rev_slider_3_1_forcefullwidth" style="position:relative;width:100%;height:auto;margin-top:0px;margin-bottom:0px"><div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="innovation-slider" data-source="gallery" style="margin: 0px auto; background-color: transparent; padding: 0px; height: 490px; position: absolute; overflow: visible; width: 1349px; left: -15px;">
<!-- START REVOLUTION SLIDER 5.3.1 fullwidth mode -->
	<div id="rev_slider_3_1" class="rev_slider fullwidthabanner revslider-initialised tp-simpleresponsive" style="max-height: 490px; margin-top: 0px; margin-bottom: 0px; height: 490px;" data-version="5.3.1" data-slideactive="rs-3">
<ul class="tp-revslider-mainul" style="visibility: visible; display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;">	<!-- SLIDE  -->
	<li data-index="rs-3" data-transition="notransition" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="" class="tp-revslider-slidesli active-revslide" style="width: 100%; height: 100%; overflow: hidden; z-index: 20; visibility: inherit; opacity: 1; background-color: rgba(255, 255, 255, 0);">
		<!-- MAIN IMAGE -->
		<div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%; visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"><!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="http://codewave.co.in/verinite/wp-content/uploads/2017/01/bg-1.jpg" alt="" title="bg (1)" width="1347" height="543" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="">--><div class="tp-bgimg defaultimg " style="background-color: rgba(0, 0, 0, 0); background-repeat: no-repeat; background-image: url(&quot;http://codewave.co.in/verinite/wp-content/uploads/2017/01/bg-1.jpg&quot;); background-size: cover; background-position: center center; width: 100%; height: 100%; opacity: 1; visibility: inherit; z-index: 20;" src="http://codewave.co.in/verinite/wp-content/uploads/2017/01/bg-1.jpg"></div></div>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-parallax-wrap" style="position: absolute; display: block; visibility: visible; left: 577px; top: 193px; z-index: 5;"><div class="tp-loop-wrap" style="position:absolute;display:block;;"><div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption   tp-resizeme" id="slide-3-layer-1" data-x="['center','center','center','right']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-41','-41','-41','-44']" data-fontsize="['40','40','40','30']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 40px; line-height: 22px; font-weight: 700; color: rgb(255, 255, 255); font-family: Oswald; visibility: inherit; transition: none; text-align: inherit; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">INNOVATION </div></div></div></div>

		<!-- LAYER NR. 2 -->
		<div class="tp-parallax-wrap" style="position: absolute; display: block; visibility: visible; left: 238px; top: 245px; z-index: 6;"><div class="tp-loop-wrap" style="position:absolute;display:block;;"><div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption   tp-resizeme" id="slide-3-layer-3" data-x="['center','center','center','right']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['11','11','11','11']" data-fontsize="['21','21','21','14']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 21px; line-height: 22px; font-weight: 400; color: rgb(255, 255, 255); font-family: &quot;Open Sans&quot;; visibility: inherit; transition: none; text-align: inherit; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </div></div></div></div>

		<!-- LAYER NR. 3 -->
		<div class="tp-parallax-wrap" style="position: absolute; display: block; visibility: visible; left: 350px; top: 281px; z-index: 7;"><div class="tp-loop-wrap" style="position:absolute;display:block;;"><div class="tp-mask-wrap" style="position: absolute; display: block; overflow: visible;"><div class="tp-caption   tp-resizeme" id="slide-3-layer-4" data-x="['center','center','center','right']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['47','47','47','47']" data-fontsize="['21','21','21','14']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:300,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]" data-textalign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 21px; line-height: 22px; font-weight: 400; color: rgb(255, 255, 255); font-family: &quot;Open Sans&quot;; visibility: inherit; transition: none; text-align: inherit; border-width: 0px; margin: 0px; padding: 0px; letter-spacing: 0px; min-height: 0px; min-width: 0px; max-height: none; max-width: none; opacity: 1; transform: matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1); transform-origin: 50% 50% 0px;">has been the industry's standard dummy text ever since the 1500s </div></div></div></div>
	</li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important; width: 0%;"></div>	<div class="tp-loader spinner0" style="display: none;"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
						/******************************************
				-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/

			var setREVStartSize=function(){
				try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					e.c = jQuery('#rev_slider_3_1');
					e.responsiveLevels = [1240,1024,778,480];
					e.gridwidth = [1240,1024,778,480];
					e.gridheight = [490,768,960,720];
							
					e.sliderLayout = "fullwidth";
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};
			
			setREVStartSize();
			
						var tpj=jQuery;
			
			var revapi3;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_3_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_3_1");
				}else{
					revapi3 = tpj("#rev_slider_3_1").show().revolution({
						sliderType:"standard",
jsFileLocation:"//codewave.co.in/verinite/wp-content/plugins/revslider/public/assets/js/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							onHoverStop:"off",
						},
						responsiveLevels:[1240,1024,778,480],
						visibilityLevels:[1240,1024,778,480],
						gridwidth:[1240,1024,778,480],
						gridheight:[490,768,960,720],
						lazyType:"none",
						shadow:0,
						spinner:"spinner0",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						disableProgressBar:"on",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});	/*ready*/
		</script>
		</div><div class="tp-fullwidth-forcer" style="width: 100%; height: 490px;"></div></div><!-- END REVOLUTION SLIDER --><div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div>
</div></div></div></div></section><section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="portfolio-class wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">        			    <div class="page-portfolio  shortcode-page-portfolio content-area" data-magnific-preloader-choice="1" data-magnific-prev="Previous" data-magnific-next="Next">
		        <div class="page-portfolio-container container-fluid  site-main" role="main" data-dir="http://codewave.co.in/verinite/wp-content/themes/verinite/cookie">
		            <div class="portfolio-filter container-fluid  text-right"><span class="filter-button"><i class="pe-7f-filter"></i></span><ul id="filters" class="filter list-inline"><li><a class="active" href="#all" data-filter=".all" title=""></a></li><li><a href="#all" data-filter=".all" title="">All</a></li></ul></div>		            <div class="row portfolio-container portfolio-fullwidth  has-infinite-scroll " data-grid="fitRows" style="position: relative; height: 322px;">
		                <div class="col-xs-12 col-sm-6 col-md-3 portfolio-column width1x all education health all portfolio-hover-style-1 " style="position: absolute; left: 0px; top: 0px;">
		                            		                            <div id="portfolio-post-4161" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px">		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a href="http://codewave.co.in/verinite/portfolio/education10/" target="_self">Education10</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-6 col-md-3 portfolio-column width1x all health all portfolio-hover-style-1 " style="position: absolute; left: 322px; top: 0px;">
		                            		                            <div id="portfolio-post-4159" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px">		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a href="http://codewave.co.in/verinite/portfolio/education9/" target="_self">Education9</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-6 col-md-3 portfolio-column width1x all education energy all portfolio-hover-style-1 " style="position: absolute; left: 644px; top: 0px;">
		                            		                            <div id="portfolio-post-4157" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px">		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a href="http://codewave.co.in/verinite/portfolio/education8/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a href="http://codewave.co.in/verinite/portfolio/education8/" target="_self">Education8</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-6 col-md-3 portfolio-column width1x all energy all portfolio-hover-style-1 " style="position: absolute; left: 966px; top: 0px;">
		                            		                            <div id="portfolio-post-4155" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px">		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a href="http://codewave.co.in/verinite/portfolio/education7/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a href="http://codewave.co.in/verinite/portfolio/education7/" target="_self">Education7</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div>		            </div>
		            		        </div><!-- #main -->
		    </div><!-- #primary --> 
							
		<div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div>
</div></div></div></div></section><section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class=" call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">HOW CAN YOU GET INVOLVED?</h4><p class="call-to-action-additonal">Join the verinite community and give back as an intern, volunteer, scientist or an investor</p></div><a class="btn  btn-primary btn-sm call-to-action-button" target="_self" href="#" style="margin-top: 10px; "> GET IN TOUCH</a></div></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main><!-- #main -->
		    
<?php get_footer(); ?>



<style>
.hentry{
	margin:0px;
}
</style>
</body></html>