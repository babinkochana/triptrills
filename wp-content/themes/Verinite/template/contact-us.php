<?php
   /**
    * Template Name: contact-us
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package Agni Framework
    */
   
   get_header(); ?>
<?php 
   $type=$_GET['type'];
   $typeToPrint='Contact us';
   if ($type!=null) {
      $type=preg_replace('/[^A-Za-z0-9\-\']/', ' ', $type); 
      $type=(string)$type;
      $typeToPrint=ucfirst($type);
   }
   ?>
<link rel="stylesheet" type="text/css" href="<?php  echo get_site_url().'/wp-content/themes/Verinite/css/contact-us.css '?>">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUAerIkLNGzq7Hg4VL0X0H9TKtMtKZFWk&libraries=places&sensor=false"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel='stylesheet' id='js_composer_front-css'  href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>" type='text/css' media='all' />
<div id="primary" class="page-fullwidth content-area">
   <main id="main" class="site-main " role="main">
      <article id="post-4862" class="post-4862 page type-page status-publish hentry">
         <div class="entry-content">
            <section style="background-image: url('<?php  echo get_site_url().'/wp-content/uploads/2017/02/Banner-BG.png'?>'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; ">
               <div class="container">
                  <div class="vc_row vc_row_fluid     ">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-center">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element ">
                              <div class="wpb_wrapper">
                                 <div class="careers_title">
                                    <h2 style="text-transform: uppercase;">GET IN TOUCH</h2>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section id="contact-form" class="contact-form" style="padding-top: 80px; padding-bottom: 40px; ">
               <div class="container">
                  <div class="vc_row vc_row_fluid">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 vc_col-md-8 text-left vc_custom_1449072504120">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element ">
                              <div class="wpb_wrapper enquiry">
                                 <h4>ENQUIRY</h4>
                                 <p>Please let us know what kind of information you are looking for by filling the form and we will get in touch with you at the earliest</p>
                              </div>
                           </div>
                           <div role="form" class="wpcf7" id="wpcf7-f841-p1773-o1" lang="en-US" dir="ltr">
                              <div class="screen-reader-response"></div>
                              <form action="<?php  echo get_site_url().'/contact_us/#wpcf7-f5597-p1773-o1'?>" method="post" class="wpcf7-form" novalidate="novalidate">
                                 <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="5597">
                                    <input type="hidden" name="_wpcf7_version" value="4.7">
                                    <input type="hidden" name="_wpcf7_locale" value="en_GB">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5597-p1773-o1">
                                    <input type="hidden" name="_wpnonce" value="7b1f991d98">
                                 </div>
                                 <div class="row cookie-contact-form-2">
                                    <div class="col-sm-12 col-md-12">
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_name"><input type="text" name="contact_name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name *"></span></div>
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_email"><input type="email" name="contact_email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *"></span></div>
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_number"><input type="tel" name="contact_number" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Mobile *"></span></div>
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_company"><input type="text" name="contact_company" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Company *"></span></div>
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_job"><input type="text" name="contact_job" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Job Title/ Designation *"></span></div>
                                       <div class="col-sm-6"><span class="wpcf7-form-control-wrap contact_rel"><input type="text" name="contact_rel" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Relationship with verinite *"></span></div>
                                       <div class="col-sm-12"><span class="wpcf7-form-control-wrap contact_type"><input type="text" name="contact_type" value="<?php  echo $typeToPrint; ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Contact us type" readonly></span></div>
                                       <div class="col-sm-12">
                                          <span class="wpcf7-form-control-wrap contact_msg">
                                             <textarea name="contact_msg" cols="3" rows="3" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea>
                                             <grammarly-btn>
                                                <div style="visibility: hidden; z-index: 2;" class="_e725ae-textarea_btn _e725ae-not_focused" data-grammarly-reactid=".0">
                                                   <div class="_e725ae-transform_wrap" data-grammarly-reactid=".0.0">
                                                      <div title="Protected by Grammarly" class="_e725ae-status" data-grammarly-reactid=".0.0.0">&nbsp;</div>
                                                   </div>
                                                </div>
                                             </grammarly-btn>
                                          </span>
                                       </div>
                                       <div class="col-sm-12" style="text-align: center;"><input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit submit_btn"></div>
                                    </div>
                                 </div>
                                 <div class="wpcf7-response-output wpcf7-display-none"></div>
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 vc_col-md-offset-1 vc_col-md-3 text-left address_main">
                        <div class="wpb_wrapper">
                           <div class="address_div">
                              <div class="agni_custom_heading page-scroll">
                                 <h4 style="text-align: left" class="vc_custom_heading agni_custom_heading_content address_content ">India</h4>
                                 <hr width="30%" align="left" class="address_content_hr">
                                 <div class="divide-line text-left "><span style="width:45px; height:1px; background-color:#000000"></span></div>
                              </div>
                              <div class="wpb_text_column wpb_content_element ">
                                 <div class="wpb_wrapper">
                                    <p>Verinite Technologies Pvt Ltd,<br>Office No.501, Icon Tower, 5th Floor, Plot B,<br>S.No. 114/115 Main Baner Road,<br>Baner, Pune - 411045</p>
                                 </div>
                              </div>
                              <div class="agni_custom_heading page-scroll">
                                 <div class="address_number">
                                    <p style="text-align: left" class="vc_custom_heading agni_custom_heading_content "><i class="pe-7s-phone address_icon"></i><span class="address_span">Tel: <a href="tel:+91-20-6629-7000">+91-20-6629-7000</a><br>Fax: +91-20-6629-7001</span></p>
                                 </div>
                              </div>
                           </div>
                           <div class="address_div">
                              <hr width="70%"  class="address_hr">
                           </div>
                           <div class="address_div">
                              <div class="agni_custom_heading page-scroll">
                                 <h4 style="text-align: left" class="vc_custom_heading agni_custom_heading_content address_content ">United Kingdom</h4>
                                 <hr width="30%" align="left" class="address_content_hr">
                                 <div class="divide-line text-left "><span style="width:45px; height:1px; background-color:#000000"></span></div>
                              </div>
                              <div class="wpb_text_column wpb_content_element ">
                                 <div class="wpb_wrapper">
                                    <p>Verinite Limited, 33, Blunts Wood Road,<br>Haywards Heath, West Sussex,<br>RH161ND, United Kingdom</p>
                                 </div>
                              </div>
                              <div class="agni_custom_heading page-scroll">
                                 <div class="address_number">
                                    <p style="text-align: left" class="vc_custom_heading agni_custom_heading_content "><i class="pe-7s-phone address_icon"></i><span class="address_span">Tel: <a href="tel:+44-144-441-4262">+44-144-441-4262</a><br>Fax: +44-144-441-4262</span></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section>
               <div class="container-fluid">
                  <div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding ">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                        <div class="wpb_wrapper">
                           <div id="map_canvas" class="map-canvas  map-canvas-style-1" style="height: 400px; position: relative; overflow: hidden;" data-map-style="1" data-map-accent-color="#22e3e5" data-map-drag="" data-map-zoom="16" data-map="<?php  echo get_site_url().'/wp-content/themes/Verinite/img/marker.png'?>" data-lng="73.77415" data-lat="18.565311" data-add1="Verinite Technologies Pvt Ltd" data-add2="No.501, Icon Tower, 5th Floor, Plot B," data-add3="S.No. 114/115 Main Baner Road, Baner, Pune - 411045" data-lng-2="" data-lat-2="" data-add1-2="" data-add2-2="" data-add3-2="" data-dir="<?php  echo get_site_url().'/wp-content/themes/Verinite'?>">
                              <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                                 <div class="gm-style" style="position: absolute; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; z-index: 0;">
                                    <div style="position: absolute; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; z-index: 0; cursor: pointer;" title="Verinite Technologies Pvt Ltd">
                                       <div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);">
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;">
                                             <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                                <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 374px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 630px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 630px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 374px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 374px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 630px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 118px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 886px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 118px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 886px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 886px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 118px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 1142px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: -138px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: -138px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 1142px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: -138px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; position: absolute; left: 1142px; top: 316px;"></div>
                                                </div>
                                             </div>
                                          </div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;">
                                             <div style="position: absolute; left: 0px; top: 0px; z-index: -1;">
                                                <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 374px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 630px; top: 60px;">
                                                      <canvas draggable="false" height="256" width="256" style="user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas>
                                                   </div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 630px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 374px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 374px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 630px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 118px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 886px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 118px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 886px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 886px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 118px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1142px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -138px; top: 60px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -138px; top: 316px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1142px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -138px; top: -196px;"></div>
                                                   <div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1142px; top: 316px;"></div>
                                                </div>
                                             </div>
                                          </div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 0;">
                                             <div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;">
                                                <div style="position: absolute; left: 374px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46197!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=52379" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 374px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46197!3i29328!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=52879" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 630px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46198!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=49105" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 630px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46198!3i29328!4i256!2m3!1e0!2sm!3i375062076!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=101252" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 630px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46198!3i29327!4i256!2m3!1e0!2sm!3i375062076!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=101002" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 374px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46197!3i29327!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=52629" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 886px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46199!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=45831" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 118px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46196!3i29328!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=56153" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 886px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46199!3i29327!4i256!2m3!1e0!2sm!3i375062076!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=97728" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 118px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46196!3i29327!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=55903" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 886px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46199!3i29328!4i256!2m3!1e0!2sm!3i375062076!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=97978" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 118px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46196!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=55653" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: -138px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46195!3i29328!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=59427" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: -138px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46195!3i29327!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=59177" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 1142px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46200!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=70341" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 1142px; top: 316px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46200!3i29328!4i256!2m3!1e0!2sm!3i375062136!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=30732" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: 1142px; top: 60px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46200!3i29327!4i256!2m3!1e0!2sm!3i375062136!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=30482" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                                <div style="position: absolute; left: -138px; top: -196px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i16!2i46195!3i29326!4i256!2m3!1e0!2sm!3i375062064!3m14!2sen-US!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjV8cy5lOmx8cC52Om9mZixzLnQ6NHxzLmU6bHxwLnY6b2ZmLHMudDoyfHMuZTpsfHAudjpvZmYscy50OjZ8cy5lOmx8cC52Om9mZixzLnQ6M3xzLmU6bC5pfHAudjpvZmYscC5oOiMwMGFhZmZ8cC5zOi0xMDB8cC5nOjIuMTV8cC5sOjEyLHMudDozfHMuZTpsLnQuZnxwLnY6b258cC5sOjI0LHMudDozfHMuZTpnfHAubDo1Nw!4e0&amp;token=58927" draggable="false" alt="" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div style="position: absolute; left: 0px; top: 0px; z-index: 2; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"></div>
                                       <div style="position: absolute; left: 0px; top: 0px; z-index: 3; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;"></div>
                                       <div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);">
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div>
                                          <div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div>
                                       </div>
                                       <iframe frameborder="0" style="z-index: -1; border: 0px none; position: absolute; height: 100%; width: 100%; padding: 0px; margin: 0px; left: 0px; top: 0px;"></iframe>
                                    </div>
                                    <div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;">
                                       <a target="_blank" href="https://maps.google.com/maps?ll=18.565311,73.77415&amp;z=16&amp;hl=en-US&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;">
                                          <div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google_white5.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div>
                                       </a>
                                    </div>
                                    <div style="background-color: white; padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 525px; top: 110px;">
                                       <div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div>
                                       <div style="font-size: 13px;">Map data ©2017 Google</div>
                                       <div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                    </div>
                                    <div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 75px; bottom: 0px; width: 127px;">
                                       <div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px;">
                                          <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                             <div style="width: 1px;"></div>
                                             <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                          </div>
                                          <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2017 Google</span></div>
                                       </div>
                                    </div>
                                    <div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;">
                                       <div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2017 Google</div>
                                    </div>
                                    <div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;">
                                       <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                          <div style="width: 1px;"></div>
                                          <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                       </div>
                                       <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en-US_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div>
                                    </div>
                                    <div style="cursor: pointer; width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv9.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 175px; user-select: none; border: 0px; padding: 0px; margin: 0px;"></div>
                                    <div draggable="false" class="gm-style-cc" style="user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;">
                                       <div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;">
                                          <div style="width: 1px;"></div>
                                          <div style="background-color: rgb(245, 245, 245); width: auto; height: 100%; margin-left: 1px;"></div>
                                       </div>
                                       <div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@18.565311,73.77415,16z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div>
                                    </div>
                                    <div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; user-select: none; position: absolute; bottom: 107px; right: 28px;">
                                       <div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;">
                                          <div draggable="false" style="user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; background-color: rgb(255, 255, 255); width: 28px; height: 55px;">
                                             <div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;">
                                                <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                             </div>
                                             <div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; background-color: rgb(230, 230, 230); top: 0px;"></div>
                                             <div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;">
                                                <div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="gm-svpc" controlwidth="28" controlheight="28" style="background-color: rgb(255, 255, 255); box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px;">
                                          <div style="position: absolute; left: 1px; top: 1px;"></div>
                                          <div style="position: absolute; left: 1px; top: 1px;">
                                             <div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                             <div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                             <div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                          </div>
                                       </div>
                                       <div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;">
                                          <div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; background-color: rgb(255, 255, 255); display: none;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                          <div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section>
               <div class="container-fluid">
                  <div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding ">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                        <div class="wpb_wrapper">
                           <div class="cta call-to-action call-to-action-style-2">
                              <div class="call-to-action-description career-calltoaction">
                                 <h4 class="call-to-action-heading">READY TO GET STARTED?</h4>
                                 <p class="call-to-action-additonal">Send us a brief note on your requirement, one of us will get in touch with you.</p>
                              </div>
                              <div class="call-to-action-button-div">
                                 <a class="btn  btn-primary  call-to-action-button career-btn" target="_self" href="#">GET IN TOUCH</a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!-- .entry-content -->
      </article>
      <!-- #post-## -->
   </main>
   <!-- #main -->
</div>
<script src="<?php  echo get_site_url().'/wp-content/themes/Verinite/js/fileuploadui.js'?>"></script>
<script>
   $( function() {
      $( "#form_date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
      });
    } );
   $("#file-upload").customFile();
</script>
<?php get_footer(); ?>