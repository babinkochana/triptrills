<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Agni Framework
 */

get_header(); ?>
<ul>
<?php $wp_query = new WP_Query(array('showposts' => 50, 'post_type' => array('blog_posts'),'orderby' => 'Asc'));?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>  

   <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php endwhile; ?>
<?php endif; ?>

</ul>
<?php get_footer(); ?>