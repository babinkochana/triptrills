<?php
  /**
   * Template Name: careers-detail
   *
   * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
   *
   * @package Agni Framework
   */
  
  get_header(); ?>
  <?php
    $post_7 = get_post($post->ID,ARRAY_A);
    $title = $post_7['post_title'];
    $content = get_post_meta( $post->ID, '', true );
    $location=$content['location'][0];
    $experience=$content['experience'][0];
    $joining_period=$content['joining_period'][0];
 /*   $job_description=$content['job_description'][0];
    $job_description1=$content['job_description1'][0];*/
    $requirements=$content['requirements'][0];
    $responsibilities=$content['responsibilities'][0];
    $knowledge_and_skills=$content['knowledge_and_skills'][0];

  ?>
<link rel="stylesheet" type="text/css" href="<?php  echo get_site_url().'/wp-content/themes/Verinite/css/careers-detail.css'?>">
<link rel='stylesheet' id='js_composer_front-css'  href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>" type='text/css' media='all' />
<div id="primary" class="page-fullwidth content-area">
  <main id="main" class="site-main " role="main">
    <article id="post-4862" class="post-4862 page type-page status-publish hentry">
      <div class="entry-content">
        <section style="background-image: url('<?php  echo get_site_url().'/wp-content/uploads/2017/02/Banner-BG.png'?>'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; ">
          <div class="container">
            <div class="vc_row vc_row_fluid     ">
              <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-center">
                <div class="wpb_wrapper">
                  <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper careers_title">
                      <h2 style="text-align: center; margin-top: 37px;"><?=$title?></h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="careers-detail-maindiv">
          <div class="container">
            <div class="vc_row vc_row_fluid     ">
              <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                <div class="wpb_wrapper">
                  <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column agni_column_container vc_col-sm-12 text-left careers-detail-row">
                      <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element careers-detail-row">
                          <div class="wpb_wrapper">
                            <h5>Responsibilities</h5>
                          </div>
                        </div>
                        <?=$requirements?>
                      </div>
                    </div>
                  </div>
                  <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column agni_column_container vc_col-sm-12 text-left careers-detail-row">
                      <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element careers-detail-row1">
                          <div class="wpb_wrapper">
                            <h5>Requirements</h5>
                          </div>
                        </div>
                        <?=$responsibilities?>
                      </div>
                    </div>
                  </div>
                  <?php 
                    if ($knowledge_and_skills!=null) 
                    {
                      echo '<div class="vc_row wpb_row vc_inner vc_row-fluid ">
                  <div class="wpb_column agni_column_container vc_col-sm-12 text-left">
                    <div class="wpb_text_column wpb_content_element careers-detail-row1">
                          <div class="wpb_wrapper">
                            <h5>Knowledge and Skills</h5>
                          </div>
                        </div>
                        '.$knowledge_and_skills.'
                      </div>
                  </div>';
                    }
                    else
                    {
                      echo "";
                    }
                    if ($location!=null) 
                    {
                      echo '<div class="vc_row wpb_row vc_inner vc_row-fluid ">
                  <div class="wpb_column agni_column_container vc_col-sm-12 text-left">
                    <div class="wpb_text_column wpb_content_element careers-detail-row1">
                          <div class="wpb_wrapper">
                            <h5>Location</h5>
                          </div>
                        </div><p>
                        '.$location.'</p>
                      </div>
                  </div>';
                    }
                    else
                    {
                      echo "";
                    }
                    if ($experience!=null) 
                    {
                      echo '<div class="vc_row wpb_row vc_inner vc_row-fluid ">
                  <div class="wpb_column agni_column_container vc_col-sm-12 text-left">
                    <div class="wpb_text_column wpb_content_element careers-detail-row1">
                          <div class="wpb_wrapper">
                            <h5>Experience</h5>
                          </div>
                        </div><p>
                        '.$experience.'</p>
                      </div>
                  </div>';
                    }
                    else
                    {
                      echo "";
                    }
                    if ($joining_period!=null) 
                    {
                      echo '<div class="vc_row wpb_row vc_inner vc_row-fluid ">
                  <div class="wpb_column agni_column_container vc_col-sm-12 text-left">
                    <div class="wpb_text_column wpb_content_element careers-detail-row1">
                          <div class="wpb_wrapper">
                            <h5>Joining Period</h5>
                          </div>
                        </div><p>
                        '.$joining_period.'</p>
                      </div>
                  </div>';
                    }
                    else
                    {
                      echo "";
                    }
                  ?>
                  </div>
                  <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    <div class="wpb_column agni_column_container vc_col-sm-12 text-left">
                      <div class="wpb_wrapper">
                        <div class=" text-left page-scroll career-detail-btn"><a class="btn btn-default call-to-action-button" href="<?php echo add_query_arg( '$p', $post->ID, get_site_url().'/career-apply-now/');?>">APPLY NOW</a></div>
                      </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <!-- .entry-content -->
    </article>
    <!-- #post-## -->
  </main>
  <!-- #main -->
</div>
<?php get_footer(); ?>