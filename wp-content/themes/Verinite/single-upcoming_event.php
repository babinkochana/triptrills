<?php
   /**
    * Template Name: careers-detail
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package Agni Framework
    */
   
   get_header(); ?>
<?php
   $post_7 = get_post($post->ID,ARRAY_A);
   $title = $post_7['post_title'];
   $post_name= $post_7['post_name'];
   $content = get_post_meta( $post->ID, '', true );
   $date=$content['date'][0];
   $pageContent=$content['page_content'][0];
   ?>
<div id="content" class="site-content content ">
   <link rel="stylesheet" type="text/css" href="<?php  echo get_site_url().'/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1'?>">
   <section class="blog blog-post blog-single-post no-sidebar">
      <div class="blog-container container-fluid">
         <div class="row">
            <div class="col-sm-12 col-md-12 blog-single-post-content sadbaskjhask">
               <div id="primary" class="content-area">
                  <main id="main" class="site-main" role="main">
                     <article id="post-6432" class="post-6432 post type-post status-publish format-standard hentry category-events">
                        <div class="entry-meta">
                           <span class="cat-links blog_cat_links"><a href="<?php  echo get_site_url().'/events/'?>" rel="category tag">Events</a></span>   <span class="posted-on blog_posted_on"><a href="<?php  echo get_site_url().'/upcoming_event/'.$post_name.'/'?>" rel="bookmark"><time class="entry-date published" datetime="2017-03-20T12:03:26+00:00"><?php echo$date; ?></time><time class="updated" datetime="2017-03-20T17:50:23+00:00"><?php echo$date; ?></time></a></span> 
                        </div>
                        <!-- .entry-meta -->
                        <h1 class="entry-title"><?php echo$title; ?></h1>
                        <div class="entry-content">
                           <!--     <h5>Hurray!! Verinite signs up with one of a Top 5 Indian Retail Bank for their business critical technology rollout!!</h5>
                              <p>David and Goliath story reprises again as Verinite literally clinched a deal from mighty banking technology service providers and hence is entrusted by one of a top 5 Indian retail Bank to own up their impending credit cards launch.</p>
                              <p>The client (Bank) is a veteran Indian retail biggie who would want to foray into credit cards business and hence thoroughly evaluated Verinite (a flourishing start-up) against leading Banking technology service providers. Verinite stood tall and was chosen amongst all mammoths for its outmatched and successful track record to own and deliver winning card system engagements across industry leading card platforms.<br>
                              Undoubtedly, Verinite is turning out to be a huge contender for leading Banking technology service players in market today, especially for technology transformation engagements, as it is proving handy in Consulting as well Technology rollouts and is been delivering value proposition via its affiliation. Testimony to which, now Big Banks along small to mid-sized are also seeing a trusted technology partner in Verinite.<br>
                              We are glad to be associated with such a Bank who not only entrusted in our values and differentiated proposition but also honored our persistence hard work and perseverance.</p> -->
                           <?php echo $pageContent; ?>
                        </div>
                        <!-- .entry-content -->
                        <footer class="entry-footer">
                           <div class=" portfolio-sharing-buttons text-center">
                              <ul class="list-inline">
                                 <li><a href="http://www.facebook.com/sharer.php?u=<?php  echo get_site_url().'/upcoming_event/'.$post_name.'/'?>//&amp;t=Events%204"><i class="fa fa-facebook"></i></a></li>
                                 <li><a href="https://twitter.com/intent/tweet?text=Events%204-<?php  echo get_site_url().'/upcoming_event/'.$post_name.'/'?>/"><i class="fa fa-twitter"></i></a></li>
                                 <li><a href="https://plus.google.com/share?url=<?php  echo get_site_url().'/upcoming_event/'.$post_name.'/'?>/"><i class="fa fa-google-plus"></i></a></li>
                                 <li><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php  echo get_site_url().'/upcoming_event/'.$post_name.'/'?>/&amp;title=Events%204"><i class="fa fa-linkedin"></i></a></li>
                              </ul>
                           </div>
                        </footer>
                        <!-- .entry-footer -->
                     </article>
                     <!-- #post-## -->                        
                  </main>
                  <!-- #main -->
               </div>
               <!-- #primary -->
            </div>
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>