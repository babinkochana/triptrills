<?php
   /**
    * Template Name: career-apply-now
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
    *
    * @package Agni Framework
    */
   
   get_header(); ?>
<link rel="stylesheet" type="text/css" href="/Verinite/wp-content/themes/Verinite/css/career-apply-now.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel='stylesheet' id='js_composer_front-css'  href='http://codewave.co.in/selco/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1' type='text/css' media='all' />
<div id="primary" class="page-fullwidth content-area">
   <main id="main" class="site-main " role="main">
      <article id="post-4862" class="post-4862 page type-page status-publish hentry">
         <div class="entry-content">
            <section style="background-image: url('http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Banner-BG.png'); background-repeat:repeat; background-size:cover; background-position:center center; background-attachment:scroll; ">
               <div class="container">
                  <div class="vc_row vc_row_fluid     ">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-center">
                        <div class="wpb_wrapper">
                           <div class="wpb_text_column wpb_content_element ">
                              <div class="wpb_wrapper">
                                 <div class="careers_title">
                                    <h2 style="text-transform: uppercase;">Apply Now</h2>
                                    <p>Looking for exciting career opportunities in banking and payment technology?</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="career-apply-maindiv">
               <div class="container">
                  <div class="vc_row vc_row_fluid     ">
                     <div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left">
                        <div class="wpb_wrapper">
                           <div role="form" class="wpcf7" id="wpcf7-f5285-p5269-o1" lang="en-US" dir="ltr">
                              <div class="screen-reader-response"></div>
                              <form action="/Verinite/career-apply-now/#wpcf7-f5285-p5269-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
                                 <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="5285">
                                    <input type="hidden" name="_wpcf7_version" value="4.6.1">
                                    <input type="hidden" name="_wpcf7_locale" value="en_US">
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f5285-p5269-o1">
                                    <input type="hidden" name="_wpnonce" value="89a7b9a890">
                                 </div>
                                 <div class="row cookie-contact-form-2">
                                    <div class="col-sm-12 col-md-12">
                                       <div class="col-sm-12">
                                          <div class="col-sm-12">
                                             <h5>Personal Details :</h5>
                                          </div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-392"><input type="text" name="text-392" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="First Name *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-393"><input type="text" name="text-393" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Last Name *"></span></div>
                                          <div class="col-xs-6 col-md-4"><span class="wpcf7-form-control-wrap text-394"><input type="text" id="form_date" name="text-394" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Date of birth(DD/MM/YYYY)*"></span></div>
                                          <div class="col-xs-6 col-md-4"><span class="wpcf7-form-control-wrap email-625"><input type="email" name="email-625" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *"></span></div>
                                          <div class="col-xs-6 col-md-4"><span class="wpcf7-form-control-wrap tel-360"><input type="tel" name="tel-360" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" placeholder="Mobile Number *"></span></div>
                                       </div>
                                       <div class="col-sm-12">
                                          <div class="col-sm-12">
                                             <h5>Professional Details :</h5>
                                          </div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-395"><input type="text" name="text-395" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Role applied for *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap number-202"><input type="number" name="number-202" value="" class="wpcf7-form-control wpcf7-number wpcf7-validates-as-number" aria-invalid="false" placeholder="Total years of expirence *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-397"><input type="text" name="text-397" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Relevant experience *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-398"><input type="text" name="text-398" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Current Employer *"></span></div>
                                          <div class="col-xs-6 col-md-4">
                                             <span class="wpcf7-form-control-wrap menu-183">
                                                <select name="menu-183" class="wpcf7-form-control wpcf7-select notice_period" aria-invalid="false">
                                                   <option value="Notice period *">Notice period *</option>
                                                   <option value="1">1</option>
                                                   <option value="2">2</option>
                                                   <option value="3">3</option>
                                                </select>
                                             </span>
                                          </div>
                                          <div class="col-xs-6 col-md-4"><span class="wpcf7-form-control-wrap text-399"><input type="text" name="text-399" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Current CTC *"></span></div>
                                          <div class="col-xs-6 col-md-4"><span class="wpcf7-form-control-wrap text-400"><input type="text" name="text-400" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Expected CTC *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap text-401"><input type="text" name="text-401" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Preferred location *"></span></div>
                                          <div class="col-xs-6"><span class="wpcf7-form-control-wrap file-544 "><div class="custom-file-upload"><input style=" padding: 17px; " type="file" name="file-544" size="40" class="wpcf7-form-control wpcf7-file form_file custom-file-input" id="file-upload" aria-invalid="false"></div></span></div>
                                       </div>
                                       <div class="col-sm-12 btn-div">
                                          <a href="" class="back-btn">Back</a>
                                          <input type="submit" value="Apply Now" class="wpcf7-form-control wpcf7-submit apply-btn"><span class="ajax-loader"></span>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="wpcf7-response-output wpcf7-display-none"></div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!-- .entry-content -->
      </article>
      <!-- #post-## -->
   </main>
   <!-- #main -->
</div>
<script src="/Verinite/wp-content/themes/Verinite/js/fileuploadui.js"></script>
<script>
$( function() {
  $("#form_date").datepicker({
  dateFormat: "dd-mm-yy",
  });

} );
$("#file-upload").customFile();
</script>
<?php get_footer(); ?>