<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
<link rel="stylesheet" type="text/css" href="http://codewave.co.in/Verinite/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1">
<?php
function agni_single(){
global $cookie_options, $post; 

$post_fullwidth = esc_attr( get_post_meta( $post->ID, 'post_fullwidth', true ) );
$post_sidebar = esc_attr( get_post_meta( $post->ID, 'post_sidebar', true ) );

if( $post_fullwidth == 'on' ){
	$post_fullwidth = '-fluid';
}
?>
<?php echo agni_page_header($post);?>
<section class="blog blog-post blog-single-post <?php echo $post_sidebar; ?>">
	<div class="blog-container container<?php echo $post_fullwidth; ?>">
		<div class="row">
			<div class="col-sm-12 col-md-<?php if( $post_sidebar != 'no-sidebar' ){ echo '8'; }else { echo '12'; } ?> blog-single-post-content sadbaskjhask">
				
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-meta">
								<?php agni_framework_post_cat(); ?>
								<?php agni_framework_post_date(); ?>
							</div><!-- .entry-meta -->

							<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

							<?php 
								if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it. ?>
								<p style="text-align:center;"><img src=<?php the_post_thumbnail_url(); ?> ></p>
							<?php } ?>

							<div class="entry-content">
								<?php the_content(); ?>
								<?php
									wp_link_pages( array(
										'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cookie' ),
										'after'  => '</div>',
									) );
								?>
								<?php agni_framework_post_tag(); ?>
							</div><!-- .entry-content -->

							<footer class="entry-footer">
								<?php if( $cookie_options['blog-sharing-panel'] == '1' ){?>
									<div class=" portfolio-sharing-buttons text-center">
										<ul class="list-inline">
											<?php  if($cookie_options['blog-sharing-icons'][1] == '1'){ ?>
												<li><a href="http://www.facebook.com/sharer.php?u=<?php esc_url( the_permalink() );?>/&amp;t=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>"><i class="fa fa-facebook"></i></a></li>
											<?php  }?>
											<?php  if($cookie_options['blog-sharing-icons'][2] == '1'){ ?>
												<li><a href="https://twitter.com/intent/tweet?text=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>-<?php esc_url( the_permalink() ); ?>"><i class="fa fa-twitter"></i></a></li>
											<?php  }?>
											<?php  if($cookie_options['blog-sharing-icons'][3] == '1'){ ?>             
												<li><a href="https://plus.google.com/share?url=<?php esc_url( the_permalink() );?>"><i class="fa fa-google-plus"></i></a></li>
											<?php  }?>
											<?php  if($cookie_options['blog-sharing-icons'][4] == '1'){ ?>             
												<li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php esc_url( the_permalink() );?>&title=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>"><i class="fa fa-linkedin"></i></a></li>
											<?php  }?>
										</ul>
									</div>
								<?php } ?>
							</footer><!-- .entry-footer -->
						</article><!-- #post-## -->

						<?php  if( $cookie_options['author-biography'] == '1' ){  ?>
	                        <div class="author-bio">
	                            <div class="author-avatar"><?php echo get_avatar( get_the_author_meta('email'), 100 ); ?></div>
	                            <div class="author-details">
	                                <h6 class="author-name"><?php the_author(); ?></h6>                
	                                <p class="author-description"><?php the_author_meta('description'); ?></p>
	                            </div>
	                        </div>
                        
                        <?php  } ?>	

                        <div class="portfolio-navigation-container blog-detail">
			            	<?php agni_framework_post_nav(); ?>
			            </div>  
						<?php //agni_framework_post_nav(); //the_post_navigation(); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>

					<?php endwhile; // End of the loop. ?>

					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
			<?php if( $post_sidebar != 'no-sidebar' ){ ?>
				
						
				<div class="col-sm-12 col-md-4 blog-post-sidebar">
					<?php get_sidebar(); ?>
				</div>
			<?php }?>
		</div>
	</div>
</section>
    	<section><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column-no-padding "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class="cta call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">READY TO GET STARTED?</h4><p class="call-to-action-additonal">Send us a brief note on your requirement, one of us will get in touch with you.</p></div><a class="btn  btn-primary  call-to-action-button" target="_self" href="/Verinite/contact_us/" style="margin-top: 50px; "> GET IN TOUCH</a></div></div></div></div></div></section>
<?php }
agni_single(); ?>

<?php get_footer(); ?>
<style>
	.custom_author_bio {
		width:100%;
		float:left;
		display:block;
		margin-left: 0px;
		margin-right: 0px;
		border-top: 0px;
		padding-top: 0px;
	}
	.custom-author-details {
		width: 100%;
		padding-left: 0px;
		float: left
	}
	.custom-avatar-image {
	    width: 100%;
		float: left;
		margin-left: auto;
		margin-right: auto;
	}
	.custom-avatar-image img {
		width: 290px;
		height: 290px;
		margin-left: auto;
		margin-right: auto;
	}
</style>