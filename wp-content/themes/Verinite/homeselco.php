 <?php
/**
 * Template Name: verinitehompage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Agni Framework
 */

get_header(); ?>
 <link rel='stylesheet' id='js_composer_front-css'  href='http://codewave.co.in/verinite/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1' type='text/css' media='all' />
<main id="main" class="site-main " role="main">

			
				<article id="post-4060" class="post-4060 page type-page status-publish hentry">
		<div class="entry-content">
		<section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><link href="http://fonts.googleapis.com/css?family=Oswald%3A700%2C300" rel="stylesheet" property="stylesheet" type="text/css" media="all"><link href="http://fonts.googleapis.com/css?family=Open+Sans%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="sliders" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.3.1 fullwidth mode -->
	<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.1">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/slider-bg-2.jpg"  alt="" title="slider-bg"  width="1349" height="501" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-1-layer-1" 
			 data-x="right" data-hoffset="30" 
			 data-y="center" data-voffset="-41" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5; white-space: nowrap; font-size: 40px; line-height: 22px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Oswald;">ALLEVIATING POVERTY 
 </div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-1-layer-3" 
			 data-x="right" data-hoffset="28" 
			 data-y="center" data-voffset="11" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; white-space: nowrap; font-size: 21px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Open Sans;">Alleviating poverty by enhancing access to renewable energy solutions. </div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-1-layer-4" 
			 data-x="right" data-hoffset="28" 
			 data-y="center" data-voffset="47" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 8; white-space: nowrap; font-size: 21px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Open Sans;">Enabling socially and environmentally inclusive development. </div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption rev-btn " 
			 id="slide-1-layer-6" 
			 data-x="right" data-hoffset="30" 
			 data-y="center" data-voffset="101" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="button" 
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(237, 176, 59, 0.75);bc:rgba(0, 0, 0, 1.00);bw:0 0 0 0;br:3px 3px 3px 3px;"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[12,12,12,12]"
			data-paddingright="[35,35,35,35]"
			data-paddingbottom="[12,12,12,12]"
			data-paddingleft="[35,35,35,35]"

			style="z-index: 10; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 300; color: rgba(255, 255, 255, 1.00);font-family:Oswald;background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 2.00);border-style:solid;border-width:2px 2px 2px 2px;border-radius:2px 2px 2px 2px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Read more
 </div>
	</li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
						/******************************************
				-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/

			var setREVStartSize=function(){
				try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					e.c = jQuery('#rev_slider_1_1');
					e.gridwidth = [1240];
					e.gridheight = [520];
							
					e.sliderLayout = "fullwidth";
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};
			
			setREVStartSize();
			
						var tpj=jQuery;
			
			var revapi1;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_1_1").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_1_1");
				}else{
					revapi1 = tpj("#rev_slider_1_1").show().revolution({
						sliderType:"standard",
jsFileLocation:"//codewave.co.in/verinite/wp-content/plugins/revslider/public/assets/js/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							onHoverStop:"off",
						},
						visibilityLevels:[1240,1024,778,480],
						gridwidth:1240,
						gridheight:520,
						lazyType:"none",
						shadow:0,
						spinner:"spinner0",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						disableProgressBar:"on",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});	/*ready*/
		</script>
		</div><!-- END REVOLUTION SLIDER --></div></div></div></div></section><section   style="margin-top: 5%; margin-bottom: 5%; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-4 text-left"><div class="wpb_wrapper"><div class=" agni-image custom-image-container text-left"><div class="wpb_wrapper"><a><img style="" width="407" height="438" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4.jpg" class="fullwidth-image attachment-full  attachment-full" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4.jpg 407w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-279x300.jpg 279w" sizes="(max-width: 407px) 100vw, 407px" /></a></div></div></div></div><div class="wpb_column agni_column_container agni_column vc_col-sm-6 text-left vc_custom_1482312181128"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: left; font-size: 30px; color: #344349; font-family: Oswald;">OUR VISION</h2>
<p style="text-align: left; font-size: 20px; color: #737373; font-family: Open Sans;">Striving to build an ecosystem for bottom-up, socially inclusive innovations and enterprises to thrive. Bridging gaps in well-being, health, education and livelihoods.</p>

		</div>
	</div>
<div class="our-button text-left page-scroll"><a class="btn btn-alt btn-default " target="_self" href="#" >Watch Video</a></div></div></div></div></div></section><section   style="margin-bottom: 5%; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><link href="http://fonts.googleapis.com/css?family=Open+Sans%3A600" rel="stylesheet" property="stylesheet" type="text/css" media="all"><link href="http://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div id="rev_slider_2_2_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="CSsliders" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.3.1 fullwidth mode -->
	<div id="rev_slider_2_2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.1">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-2" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1.jpg"  alt="" title="1"  width="1349" height="372" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-2-layer-1" 
			 data-x="center" data-hoffset="" 
			 data-y="100" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5; white-space: nowrap; font-size: 30px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Open Sans;border-color:rgba(255, 255, 255, 1.00);">Redefining `scale' through replication - Designing decentralized, </div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme" 
			 id="slide-2-layer-2" 
			 data-x="center" data-hoffset="" 
			 data-y="145" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 22px; font-weight: 600; color: rgba(255, 255, 255, 1.00);font-family:Open Sans;">replicable models and processes<span style="font-size: 30px;font-weight: 400px; ">for clean energy issues around the world </span> </div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption rev-btn " 
			 id="slide-2-layer-6" 
			 data-x="479" 
			 data-y="217" 
						data-width="['auto']"
			data-height="['auto']"
 
			data-type="button" 
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 1.00);bw:0 0 0 0;br:0px 0px 0px 0px;"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[23,23,23,23]"
			data-paddingright="[30,30,30,30]"
			data-paddingbottom="[23,23,23,23]"
			data-paddingleft="[30,30,30,30]"

			style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;background-color:rgba(255, 255, 255, 0);border-color:rgba(255, 255, 255, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:4px 4px 4px 4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Read Case Studies </div>
	</li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
						/******************************************
				-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/

			var setREVStartSize=function(){
				try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					e.c = jQuery('#rev_slider_2_2');
					e.gridwidth = [1240];
					e.gridheight = [380];
							
					e.sliderLayout = "fullwidth";
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
					
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};
			
			setREVStartSize();
			
						var tpj=jQuery;
			
			var revapi2;
			tpj(document).ready(function() {
				if(tpj("#rev_slider_2_2").revolution == undefined){
					revslider_showDoubleJqueryError("#rev_slider_2_2");
				}else{
					revapi2 = tpj("#rev_slider_2_2").show().revolution({
						sliderType:"standard",
jsFileLocation:"//codewave.co.in/verinite/wp-content/plugins/revslider/public/assets/js/",
						sliderLayout:"fullwidth",
						dottedOverlay:"none",
						delay:9000,
						navigation: {
							onHoverStop:"off",
						},
						visibilityLevels:[1240,1024,778,480],
						gridwidth:1240,
						gridheight:380,
						lazyType:"none",
						shadow:0,
						spinner:"spinner0",
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,
						shuffle:"off",
						autoHeight:"off",
						disableProgressBar:"on",
						hideThumbsOnMobile:"off",
						hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						debugMode:false,
						fallbacks: {
							simplifyAll:"off",
							nextSlideOnWindowFocus:"off",
							disableFocusListener:false,
						}
					});
				}
			});	/*ready*/
		</script>
		</div><!-- END REVOLUTION SLIDER --><div class="vc_row wpb_row vc_inner vc_row-fluid our-work"><div class="wpb_column agni_column_container vc_col-sm-12 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald; margin-top: 5%;">OUR WORK</h2>
<p style="text-align: center; color: #585858; font-size: 14px; font-family: Open Sans;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

		</div>
	</div>
</div></div></div></div></div></div></div></section><section id=" "  ><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="our-process wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper">        			    <div class="page-portfolio  shortcode-page-portfolio content-area" data-magnific-preloader-choice="1" data-magnific-prev="Previous" data-magnific-next="Next">
		        <div class="page-portfolio-container container-fluid  site-main" role="main" data-dir="http://codewave.co.in/verinite/wp-content/themes/verinite/cookie">
		            <div class="portfolio-filter container-fluid  text-right"><span class="filter-button" ><i class="pe-7f-filter"></i></span><ul id="filters" class="filter list-inline"><li><a class="active" href="#all" data-filter=".all" title=""></a></li><li><a href="#all" data-filter=".all" title="">All</a></li><li><a href="#education" data-filter=".education" title="">Education</a></li><li><a href="#energy" data-filter=".energy" title="">Energy</a></li><li><a href="#health" data-filter=".health" title="">Health</a></li></ul></div>		            <div class="row portfolio-container portfolio-fullwidth  has-infinite-scroll " data-grid="fitRows">
		                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all health all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4141" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self">Education1</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education1/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/1-1.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4144" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/2-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/2-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self">Education2</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education2/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/2.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education energy all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4147" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/3-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/3-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self">Education3</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education3/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/3.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy health all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4149" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="237" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg 237w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1-180x180.jpg 180w" sizes="(max-width: 237px) 100vw, 237px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self">Education4</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education4/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/4-1.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4151" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/5-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/5-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self">Education5</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education5/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/5.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy health all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4153" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/6-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/6-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self">Education6</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education6/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/6.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all energy all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4155" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/7-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self">Education7</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education7/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/7.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education energy all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4157" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/8-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self">Education8</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Energy</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education8/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/8.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all health all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4159" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/9-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self">Education9</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education9/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/9.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div><div class="col-xs-12 col-sm-4 col-md-3 col-lg-2_5 portfolio-column width1x all education health all portfolio-hover-style-2 ">
		                            		                            <div id="portfolio-post-4161" class="portfolio-post">
		                                <div class="portfolio-thumbnail">
		                                    <img width="236" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg 236w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-150x150.jpg 150w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/10-180x180.jpg 180w" sizes="(max-width: 236px) 100vw, 236px" />		                                </div>
		                                			                                <div class="portfolio-caption-content">
			                                    <div class="portfolio-content">
			                                        <div class="portfolio-content-details">
			                                            <div class="portfolio-icon hide"><a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><span></span></a></div>
			                                            <h5 class="portfolio-title"><a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self">Education10</a></h5>
			                                            <ul class="portfolio-category list-inline">
			                                              <!--   <li>All</li><li>Education</li><li>Health</li> -->
			                                                 Lorem Ipsum is simply dummy text of the...<div class="read_more"><a href="#">Read more</a></div>
			                                            </ul>
			                                            <div class="portfolio-meta">
			                                           <!--      <a  href="http://codewave.co.in/verinite/portfolio/education10/" target="_self"><i class="fa fa-link"></i></a>
			                                                <a href="http://codewave.co.in/verinite/wp-content/uploads/2016/12/10.jpg" class="portfolio-attachment"><i class="fa fa-image"></i></a> -->
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                                	                                    		                            </div>
		                            		                        </div>		            </div>
		            		        </div><!-- #main -->
		    </div><!-- #primary --> 
							
		<div class=" text-center page-scroll"><a class="btn btn-default " target="_self" href="#" style="margin-top: px; margin-bottom: 4%; margin-left: px; margin-right: px; border-radius:2px;">Load More</a></div></div></div></div></div></section><section class="process-img"  style="margin-top: 8%; padding-top: 5%; padding-bottom: 5%; "><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1482387453361"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1482478473614">
		<div class="wpb_wrapper">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald; margin-top: 5%;">OUR PROCESS</h2>
<p style="text-align: center; font-size: 15px; color: #33414c;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

		</div>
	</div>
<div class=" agni-image custom-image-container text-center"><div class="wpb_wrapper"><a><img style="" width="1063" height="236" src="http://codewave.co.in/verinite/wp-content/uploads/2016/12/our-process.jpg" class="fullwidth-image attachment-full  attachment-full" alt="" srcset="http://codewave.co.in/verinite/wp-content/uploads/2016/12/our-process.jpg 1063w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/our-process-300x67.jpg 300w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/our-process-768x171.jpg 768w, http://codewave.co.in/verinite/wp-content/uploads/2016/12/our-process-1024x227.jpg 1024w" sizes="(max-width: 1063px) 100vw, 1063px" /></a></div></div></div></div></div></div></section><section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left vc_custom_1482472992046"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h2 style="text-align: center; font-size: 30px; color: #33414c; font-family: Oswald;">STORIES</h2>
<p style="text-align: center; font-size: 15px; color: #33414c;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

		</div>
	</div>
</div></div></div></div></section><section   ><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column ">


<?php $verinitevideos = new Wp_Query(array(
                           'post_type' => 'verinitevideos',
                           'posts_per_page' => 4
                                 )); ?>
<?php while($verinitevideos->have_posts()) : $verinitevideos->the_post(); ?>
<div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9">
                    <?php the_field('videos') ?>
					<!-- <iframe src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe> -->
				</div>	
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div>
  <?php endwhile; ?>
<!-- <div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9">
					<iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div> --><!-- <div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9">
					<iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div> --><!-- <div class="wpb_column agni_column_container agni_column vc_col-sm-3 text-left"><div class="wpb_wrapper"><div  class="verinite_video custom-video embed-responsive embed-responsive-16by9">
					<iframe width="854" height="480" src="https://www.youtube.com/embed/-aLhTdIT0Lk" frameborder="0" allowfullscreen></iframe>
				</div>	
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard &#8230;</p>

		</div>
	</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Livehood"><button>Livehood</button></div>

		</div>
	</div>
</div></div><div class="wpb_column agni_column_container vc_col-sm-6 text-left"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Micro_Financing"><button>Micro Financing</button></div>

		</div>
	</div>
</div></div></div>
	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<div class="Women_Enterpreneurs"><button>Women Enterpreneurs</button></div>

		</div>
	</div>
</div></div> --></div></div></section><section class="call-to-action"  style="margin-top: 8%; padding-top: 5%; padding-bottom: 5%; "><div class="container-fluid"><div class="vc_row vc_row_fluid    has-fullwidth-column "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"><div class="call-to-action call-to-action call-to-action-style-2"><div class="call-to-action-description"><h4 class="call-to-action-heading">HOW CAN YOU GET INVOLVED? </h4><p class="call-to-action-additonal">Join the verinite Community and give back as an intern,volunteer, scientist or an investor</p></div><a class="btn btn-alt btn-primary btn-lg call-to-action-button" target="_self" href="#" > GET IN TOUCH</a></div></div></div></div></div></section><section   ><div class="container"><div class="vc_row vc_row_fluid     "><div class="wpb_column agni_column_container agni_column vc_col-sm-12 text-left"><div class="wpb_wrapper"></div></div></div></div></section>
			</div><!-- .entry-content -->
</article><!-- #post-## -->


				
			
		</main><!-- #main -->
<?php get_footer(); ?>

