<?php
get_header(); ?>

<section class="blog blog-post <?php echo esc_attr($cookie_options['archive-layout']); ?>-layout-post <?php echo esc_attr($cookie_options['archive-sidebar']);?>">
	<div class="blog-container container">
		<div class="row">
			<div class="col-sm-7 col-md-8 blog-post-content" data-blog-grid="<?php echo esc_attr($cookie_options['blog-grid-layout']); ?>">
				<div id="primary" class="content-area">
					<main id="main" class="site-main" role="main">

						<?php /* Start the Loop */ ?>
						<?php $wp_query = new WP_Query(array('showposts' => 50, 'post_type' => array('blog'),'orderby' => 'Asc'));?>
						<?php if (have_posts()) : ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-meta">
		<?php echo agni_framework_post_cat(); ?>
		<?php echo agni_framework_post_date(); ?>
		<?php echo agni_framework_post_author(); ?>
	</div><!-- .entry-meta -->

	<?php the_title( sprintf( '<p class="entry-title verinite-blog-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></p>' ); ?>

	<div class="entry-content verinite-blog-post">
		<?php
			the_excerpt( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s', 'cookie' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cookie' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<footer class="entry-footer">
		<?php //agni_framework_post_author(); ?>
        <?php if( $cookie_options['blog-sharing-panel'] == '1' ){?>
	        <div class=" post-sharing-buttons text-center">
		        <ul class="list-inline">
		            <?php  if($cookie_options['blog-sharing-icons'][1] == '1'){ ?>
		                <li><a href="http://www.facebook.com/sharer.php?u=<?php esc_url( the_permalink() );?>/&amp;t=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>"><i class="fa fa-facebook"></i></a></li>
		            <?php  }?>
		            <?php  if($cookie_options['blog-sharing-icons'][2] == '1'){ ?>
		                <li><a href="https://twitter.com/intent/tweet?text=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>-<?php esc_url( the_permalink() ); ?>"><i class="fa fa-twitter"></i></a></li>
		            <?php  }?>
		        	<?php  if($cookie_options['blog-sharing-icons'][3] == '1'){ ?>             
		                <li><a href="https://plus.google.com/share?url=<?php esc_url( the_permalink() );?>"><i class="fa fa-google-plus"></i></a></li>
		            <?php  }?>
		            <?php  if($cookie_options['blog-sharing-icons'][4] == '1'){ ?>             
		                <li><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php esc_url( the_permalink() );?>&title=<?php echo esc_html( str_replace( ' ', '%20', the_title('', '', false) ) ); ?>"><i class="fa fa-linkedin"></i></a></li>
		            <?php  }?>
		        </ul>
		    </div>
	    <?php } ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->

						<?php endwhile; ?>
						<!-- Loop Ends -->

					<?php endif; ?>

					</main><!-- #main -->
					<?php if( $cookie_options['blog-navigation'] == '1' ){ agni_page_navigation( '', $number_navigation = 'post-number-navigation' ); }else{ the_posts_navigation(); } ?>
				</div><!-- #primary -->
			</div>
				<div class="col-sm-5 col-md-4 blog-post-sidebar">
					<?php get_sidebar(); ?>
				</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
<style>
	.byline {
		display:inline-block;
		margin-left:5px;
	}
	.blog_posted_on {
		display:inline-block;
	}
	.entry-title:after {
		display:none;
	}
	.verinite-blog-title {
		font-size: 24px;
		font-weight: 500;
		margin-top: 15px;
		margin-bottom: 20px !important;
	}
	.sidebar-subscribe-button {
		    background-color: transparent;
    border: 1px solid !important;
    border-color: #55c0da !important;
    padding: 15px 25px;
    color: #55c0da !important;
    margin-bottom: 0px !important;	
	}
.sidebar-subscribe-button:hover {
	color:#fff !important;
	background-color: #55c0da !important;
}
</style>