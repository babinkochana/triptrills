<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
?>
<?php global $cookie_options; ?>
<ul class="products row" data-shop-grid="<?php echo $cookie_options['shop-grid-layout']; ?>">