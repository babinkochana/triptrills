<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
function agni_product_thumbnail(){
	global $post, $product, $woocommerce, $cookie_options ;

	$attachment_ids = $product->get_gallery_attachment_ids();

	if ( $attachment_ids ) {
		$loop 		= 0;
		$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
		?>
		<?php if( $cookie_options['shop-single-thumbnail-style'] == 'single-product-hover-style-2' ){
			if ( has_post_thumbnail() ) {

				$single_image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
				$single_image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
				$single_image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
				$single_image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'cookie-grid-thumbnail' ), array(
					'title'	=> $single_image_title,
					'alt'	=> $single_image_title
					) );
			}
		} ?>
		<div class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

			foreach ( $attachment_ids as $attachment_id ) {

				//$classes = array( 'zoom' );

				if ( $loop === 0 || $loop % $columns === 0 ) {
					$classes[] = 'first';
				}

				if ( ( $loop + 1 ) % $columns === 0 ) {
					$classes[] = 'last';
				}

				$image_class = implode( ' ', $classes );
				$props       = wc_get_product_attachment_props( $attachment_id, $post );

				if ( ! $props['url'] ) {
					continue;
				}

				if( $cookie_options['shop-single-thumbnail-style'] == 'single-product-hover-style-2' ){
					echo apply_filters( 
						'woocommerce_single_product_image_thumbnail_html', 
						sprintf( 
							'<a href="%s" class="%s" title="%s" data-standard="%s">%s</a>', 
							esc_url( $props['url'] ),
							esc_attr( $image_class ),
							esc_attr( $props['caption'] ), 
							esc_url( $props['url'] ),
							wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'thumbnail' ), 0, $props )
						), 
						$attachment_id, 
						$post->ID, 
						esc_attr( $image_class ) 
					);
				}
				else{
					echo apply_filters( 
						'woocommerce_single_product_image_thumbnail_html', 
						sprintf( 
							'<a href="%s" class="%s" title="%s">%s</a>', 
							esc_url( $props['url'] ),
							esc_attr( $image_class ),
							esc_attr( $props['caption'] ), 
							wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'thumbnail' ), 0, $props )
						), 
						$attachment_id, 
						$post->ID, 
						esc_attr( $image_class )
					);
				}
				/*echo apply_filters(
					'woocommerce_single_product_image_thumbnail_html',
					sprintf(
						'<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>',
						esc_url( $props['url'] ),
						esc_attr( $image_class ),
						esc_attr( $props['caption'] ),
						wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $props )
					),
					$attachment_id,
					$post->ID,
					esc_attr( $image_class )
				);*/

				$loop++;
			}

		?></div>
		<?php
	}
}
agni_product_thumbnail();
