���X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6721";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-01-11 14:19:30";s:13:"post_date_gmt";s:19:"2016-01-11 14:19:30";s:12:"post_content";s:4163:"We are all aware that social media has had a tremendous impact on our culture, in business, on the world-at-large.  Social media websites are some of the most popular haunts on the Internet. They have revolutionized the way people communicate and socialize on the Web.

Many employers today are looking at the social media accounts of potential employees to get an idea of the type of person they might be hiring. They’re not the only ones — lending companies are also getting in on the act.

There are some interesting challenges that lending companies are facing in the developed countries. Looking at India, we are now seeing emergence of the alternate credit scoring practices and patterns. The obvious reasons being growing middle class and lack of financial access. Mobile penetration has been on rapid rise and that means people also have access to some of the systems that come with mobile phones, such as social media.

In such an environment, financial institutions are going to end up with a very detailed data about people’s mobile phone use and as well as access to information about social network like Facebook, Twitter etc. This creates an alternate way of getting data about people’s personalities. This idea of using social networks to provide an insight into the credit risk of a specific borrower is efficacious in emerging markets where the middle class is growing at a blinding pace and loan requirements are on an all-time high while people are lacking sufficient credit history.

There are quite a few organizations (e.g. Lenddo, Klout, Hello Soda, Friendly Score, Demyst Data etc.) that have emerged who collect information from social media and then crunch it.  They run the numbers and figures out whether customer is a good person in terms of creditworthiness — and whether he should be deserving a loan. And it gives this information to the lender, which then decides to essentially qualify customer for a loan using both traditional (credit score) and non-traditional data.

It has been increasingly difficult for financial institutions to give small loans profitably owing to the detailed and exhaustive lending procedures. Personal guarantees are required to process loans and that discourages small business owners who don’t wish to put their personal assets on the line. Also due to a host of business owners using personal credit to run their business, their credit score deflates to 700 although they are actually credit worthy.

We believe this practice using social media data for supplementing credit scoring can stimulate and electrify economic growth.

All the emerging nations, and the entire financial world are talking about empowering the middle class.  Something that pulls people out of poverty to the doors of opportunities for a better life and moves up not just personally but takes the growth of the country up. This whole social media scoring system will help aspiring people from all backgrounds to look up to business possibilities and that will have a huge impact on the economic growth of our country.

Imagine a youth, belonging to a family who has been into farming since decades, getting access to loans and opening a business. Due to lack of credit history, this would have been a distant dream.

The key pros of this system of credit scoring are:
<ul>
 	<li>People, even if you haven’t met them but are friends of your friends have the ability to affect your credit score.</li>
 	<li>The world is connected much better and at higher degrees.</li>
 	<li>People across all walks of life can avail loans irrespective of their financial status.</li>
 	<li>Being morally upright and clean in your dealing have become more important than ever before.</li>
 	<li>Greater work opportunities will emerge for the middleclass which will indirectly spike the nation’s economy.</li>
</ul>
Traditional data analysts have long ignored the value of social networks to provide access to credit but  are now waking up to this trend that holds a lot of potential. We believe “Social Network will matter” and contribute immensely towards decision making process in future.";s:10:"post_title";s:62:"HOW ABOUT USING SOCIAL MEDIA FOR SUPPLEMENTING CREDIT SCORING?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:61:"how-about-using-social-media-for-supplementing-credit-scoring";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 11:43:35";s:17:"post_modified_gmt";s:19:"2017-04-04 10:43:35";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:58:"http://codewave.co.in/Verinite/?post_type=blog&#038;p=6721";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"blog";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}