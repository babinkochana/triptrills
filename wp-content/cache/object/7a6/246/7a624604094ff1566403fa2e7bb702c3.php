k��X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:4674;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-02-21 09:37:43";s:13:"post_date_gmt";s:19:"2017-02-21 09:37:43";s:12:"post_content";s:5342:"[vc_row bg_choice="2" bg_image="4681" padding_top="30px" padding_bottom="50px"][vc_column][agni_image img_url="4682" img_size="thumbnail" alignment="center" img_link="1" class="banner-logo"][vc_column_text]
<p style="text-align: center; font-size: 30px; font-weight: 600; font-family: 'Poppins'; color: #868788;">We had a clear vision of what Verinite represents and
wanted to communicate that effectively through our visual identity.

[/vc_column_text][/vc_column][/vc_row][vc_row padding_top="60px" padding_bottom="20px"][vc_column][vc_column_text css=".vc_custom_1487859300649{margin-bottom: 0px !important;}"]
<p style="text-align: center; font-size: 19px; font-family: 'Poppins'; color: rgba(0, 0, 0, .9); line-height: 2em; font-weight: 500;">Creating an identity for our company was an interesting exercise. We had a clear vision of what
Verinite represents and wanted to communicate that effectively through our visual identity.
It was a fortunate coincidence that the alphabets 'i' and 't' are together in the word Verinite.
By highlighting the two alphabets in a different colour, we were able to clearly express
our line of business.

[/vc_column_text][/vc_column][/vc_row][vc_row padding_top="40px" padding_bottom="50px"][vc_column width="1/2"][agni_image img_url="4676" img_link="1"][/vc_column][vc_column width="1/2" el_class="right-text-padding"][vc_column_text el_class="visual-small"]
<p class="visual-id-header">WHY DID WE CHOOSE TO WRITE
VERINITE IN LOWER CASE?

<p class="visual-id-text">All the alphabets in the logo are written in lower-case to reflect our humility which is at the core of Verinite. Irrespective of the heights we may achieve, we seek to retain this value and remain rooted to the ground.</p>

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column css=".vc_custom_1487864287579{padding-top: 30px !important;}"][vc_column_text]
<p class="meaning-heading" style="text-align: center;">WHAT DOES THE SYMBOL REPRESENT?</p>
<p class="meaning-text" style="text-align: center;">We seek to attain perfection in everything that we undertake and so we used a 'perfect square' as the
base of our symbol. We created a 'v' and 'n' inside the symbol, because the word 'Verinite' is composed
of two parts, 'Veritas' (which means integrity in Latin) and 'Infinite'.

[/vc_column_text][/vc_column][/vc_row][vc_row padding_top="40px" padding_bottom="40px"][vc_column width="1/2" el_class="left-text-padding"][vc_column_text el_class="visual-perfect"]
<p class="visual-id-header">PERFECT SQUARE - PERFECTION</p>
<p class="visual-id-text">The square in our logo stands for perfection and our intention to be consistently progressive, heading to achieve perfection in everything we do with integrity at the heart of our work.</p>

[/vc_column_text][/vc_column][vc_column width="1/2"][agni_image img_url="4677" alignment="right" img_link="1"][/vc_column][/vc_row][vc_row padding_top="30px" padding_bottom="30px"][vc_column width="1/2"][agni_image img_url="4678" img_link="1"][/vc_column][vc_column width="1/2" el_class="right-text-padding"][vc_column_text el_class="visual-arrow"]
<p class="visual-id-header">ARROW - FORWARD MOVEMENT</p>
<p class="visual-id-text">The forward arrow represents our attitude to making progress consistently and facing challenges. It reflects our collective ability to keep moving forward and make progress.</p>

[/vc_column_text][/vc_column][/vc_row][vc_row padding_top="30px" padding_bottom="30px"][vc_column width="1/2" el_class="left-text-padding"][vc_column_text el_class="visual-our-logo"]
<p class="visual-id-header">OUR LOGO - OUR BRAND IDENTITY</p>
<p class="visual-id-text">The logo stands as an inspiration for us to adopt a progressive mindset at work, constantly working towards perfection and staying closely aligned to our core values as we progress.</p>

[/vc_column_text][/vc_column][vc_column width="1/2"][agni_image img_url="4679" alignment="right" img_link="1"][/vc_column][/vc_row][vc_row padding_top="50px" padding_bottom="80px"][vc_column width="1/2"][agni_image img_url="4680" img_link="1"][/vc_column][vc_column width="1/2" el_class="right-text-padding"][vc_column_text el_class="visual-significance"]
<p class="visual-id-header">WHAT IS THE SIGNIFICANCE OF
CHOSEN COLOURS?

<p class="visual-id-text">The vibrant blue reflects our positive energy and our modern outlook. It is also the colour of the sky, because Verinite is a place that offers unlimited opportunities. The vibrancy of the blue has been softened by a more solemn grey, which reflects our extensive experience in this industry</p>

<div class="col-md-6 col-sm-6 col-xs-12 extra-class">
<ul class="inner-list">
	<li class="custom-list-style">WINDOW OF OPPORTUNITY</li>
</ul>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 extra-class">
<ul class="inner-list">
	<li class="custom-list-style">VERITAS AND INFINITY</li>
</ul>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding"][vc_column][agni_call_to_action type="2" value="GET IN TOUCH" url="http://codewave.co.in/Verinite/contact_us/" style="alt" choice="white" size="lg" button_margin_top="60" quote="READY TO GET STARTED?" additional_quote="Send us a brief note on your requirement, one of us will get in touch with you." class="cta"][/vc_column][/vc_row]";s:10:"post_title";s:14:"Brand Identity";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"brand-identity";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-03-30 09:11:13";s:17:"post_modified_gmt";s:19:"2017-03-30 08:11:13";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:44:"http://codewave.co.in/Verinite/?page_id=4674";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}