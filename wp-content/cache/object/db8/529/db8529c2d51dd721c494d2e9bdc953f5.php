k��X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:5962;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-03-15 08:32:10";s:13:"post_date_gmt";s:19:"2017-03-15 08:32:10";s:12:"post_content";s:8916:"[vc_row bg_choice="2" bg_image="6005" el_class="banner_itserv_ver"][vc_column][vc_column_text css=".vc_custom_1489655121479{margin-bottom: 20px !important;}"]
<p class="itservice-title"><span class="itservice-titleclr">INFORMATION SECURITY</span></p>
<p class="itservice-headtxt">Helping organizations ensure their IT systems and operations are functioning securely and optimally.</p>

[/vc_column_text][vc_column_text]
<div style="width: 100%; float: left;">
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="../wp-content/uploads/2017/03/inf_secimga.png" /></div>
<div class="banner-textser">
<p class="banner-headingser">SECURITY
AUDITS

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="../wp-content/uploads/2017/03/inf_secimgb.png" /></div>
<div class="banner-textser">
<p class="banner-headingser">VULNERABILITY
TESTING

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="../wp-content/uploads/2017/03/inf_secimgc.png" /></div>
<div class="banner-textser">
<p class="banner-headingser">RISK
ASSESSMENTS

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first webapp_serv">
<div class="banner-icon"><img src="../wp-content/uploads/2017/03/inf_secimgd.png" /></div>
<div class="banner-textser">
<p class="banner-headingser">TRAININGS &amp;
CERTIFICATIONS

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first webapp_serv">
<div class="banner-icon"><img src="../wp-content/uploads/2017/03/inf_secimge.png" /></div>
<div class="banner-textsera">
<p class="banner-headingser">SECURITY
CONSULTING

</div>
</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row bg_color="#ffffff" padding_top="25px" padding_bottom="25px"][vc_column width="1/2" css=".vc_custom_1488529891035{background-color: #ffffff !important;}"][vc_column_text css=".vc_custom_1490100027526{margin-bottom: 0px !important;background-color: #ffffff !important;}" el_class="right-text-padding-verser"]
<p class="infosecu_secimga"><img src="../wp-content/uploads/2017/03/mobsec_secimg.png" alt="null" /></p>

[/vc_column_text][/vc_column][vc_column width="1/2" css=".vc_custom_1488804551327{background-color: #ffffff !important;}"][vc_column_text css=".vc_custom_1489578718571{padding-top: 35px !important;}"]
<p class="business_banking">Information Technology has become all-pervasive today as organizations extensively take to IT Implementations. In view of the risks that characterize information and technology environments, it is critical for organizations to establish assurance that their implementations are functioning at an optimal and secure level. Verinite offers an array of information security service offerings to assist Banking industry.</p>

[/vc_column_text][/vc_column][/vc_row][vc_row bg_color="#fbfbfb" padding_top="60px" padding_bottom="60px"][vc_column el_class="app_mangser_txt_indp"][vc_tabs el_class="tabs1_indep_verini"][vc_tab active="active" title="SECURITY TESTING SERVICES" tab_id="e14ba23f-f52b-5"][vc_column_text]
<p class="devt_app_txtlist">Applications form the lifeblood of information system environments. Banks are heavily dependent on Core Banking Applications, Card Management Applications or E-Banking Applications. Online e-Commerce companies rely heavily on their public facing revenue generating commerce portals. However, it is common knowledge that these applications are developed, deployed or implemented in a less-secure manner, leaving them open to exploitation by internal and external threat sources.</p>
<p class="devt_app_txtlist">Elements such as routers, firewalls, switches, end user devices and their operating systems form the life of the underlying networks. These play a critical role of availing services within and outside the organization. It's important that infrastructure elements are insulated from external and internal security threats and attacks.</p>
<p class="devt_app_txtlist">With a wealth of industry experience, Verinite offers a range of security testing services whose value proposition is to ensure a cleaner and healthier security posture for our clients.</p>
<p class="devt_app_txtlist">Services offered here include:</p>

<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Web Application Penetration Testing</li>
	<li class="custom-list-style">Mobile Application Penetration Testing</li>
	<li class="custom-list-style">Application (Web and Mobile) Security Audits</li>
	<li class="custom-list-style">Secure Code Reviews</li>
</ul>
</div>
<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Application Risk Assessments</li>
	<li class="custom-list-style">Network Security Vulnerability Assessment and Penetration Testing</li>
	<li class="custom-list-style">Network Configuration Review</li>
	<li class="custom-list-style">Firewall Rules Review</li>
</ul>
</div>
[/vc_column_text][/vc_tab][vc_tab title="SECURITY TRAINING SERVICES" tab_id="b20d2915-6c9c-2"][vc_column_text]
<p class="devt_app_txtlist">“Knowledge is Power” - this phrase has held true in the Information Security domain. With the ever growing demand for Information Security as an essential ingredient within IT Operations across industry verticals, organizations need to constantly update their knowledge levels on key information security areas. Verinite training services, help organizations meet their information security knowledge objectives.</p>
<p class="devt_app_txtlist">Our flagship training programs include:</p>

<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Certified Web Application Security Professional</li>
	<li class="custom-list-style">Certified Enterprise Security Professional</li>
	<li class="custom-list-style">Certified Secure Mobile Application Developer</li>
	<li class="custom-list-style">Certified (IT) Risk Champion</li>
</ul>
</div>
<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Certified PCI-DSS Champion</li>
	<li class="custom-list-style">Certified ISO27001 Champion</li>
	<li class="custom-list-style">Enterprise Security Awareness Workshops</li>
</ul>
</div>
[/vc_column_text][/vc_tab][vc_tab title="SECURITY CONSULTING SERVICES" tab_id="1489510536307-2-9"][vc_column_text]
<p class="devt_app_txtlist">Developing and implementing an enterprise wide information security program is critical to organizations that depend on information systems. Verinite helps organizations build and run a dependable security program.</p>
<p class="devt_app_txtlist">Services offered here include:</p>

<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Enterprise IT Risk Assessments (based on ISO 31000, FRAP, OCTAVE and NIST)</li>
	<li class="custom-list-style">Information Security Management System (ISMS) Implementation</li>
	<li class="custom-list-style">Information Security Policy Framework Design/Re-Engineering</li>
	<li class="custom-list-style">Bring Your Own Device (BYOD) Consulting</li>
	<li class="custom-list-style">Secure Application Development (Secure- SDLC) Consulting</li>
</ul>
</div>
<div class="devt_app_ser_consul">
<ul class="inner-list">
	<li class="custom-list-style">Compliance Consulting and Implementation on</li>
</ul>
<p class="list_info_ver">1) PCI- DSS / PA- DSS (3.0)</p>
<p class="list_info_ver">2) ISO 27001 (2005/ 2013)</p>
<p class="list_info_ver">3) HIPAA</p>
<p class="list_info_ver">4) SAS- 70</p>

</div>
[/vc_column_text][/vc_tab][/vc_tabs][/vc_column][/vc_row][vc_row margin_top="60px"][vc_column][vc_column_text]
<p class="custom-heading"><span style="color: #55c0da;">CASE STUDIES</span> IT SERVICES</p>

[/vc_column_text][agni_posts posttype="portfolio" portfolio_fullwidth="1" portfolio_gutter="1" portfolio_hover_style="5" portfolio_categories="case-studies" posts_per_page="3" order="ASC"][agni_button value="VIEW ALL" url="http://codewave.co.in/Verinite/case-studies/" style="alt" size="lg" alignment="center" radius="2px" margin_bottom="20px" class="button-default"][/vc_column][/vc_row][vc_row bg_color="#f9f9f9" padding_top="70px" padding_bottom="50px" margin_top="50"][vc_column][agni_testimonials testimonial_categories="verinite-testimonials" circle_avatar="1" alignment="center" testimonial_autoplay="" testimonial_loop="" testimonial_pagination=""][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding"][vc_column][agni_call_to_action type="2" value="GET IN TOUCH" url="http://codewave.co.in/Verinite/contact_us/" style="alt" choice="white" size="lg" button_margin_top="50" quote="READY TO GET STARTED?" additional_quote="Send us a brief note on your requirement, one of us will get in touch with you." class="cta"][/vc_column][/vc_row]";s:10:"post_title";s:20:"Information Security";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:20:"information-security";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-03-21 15:26:50";s:17:"post_modified_gmt";s:19:"2017-03-21 15:26:50";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:44:"http://codewave.co.in/Verinite/?page_id=5962";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}