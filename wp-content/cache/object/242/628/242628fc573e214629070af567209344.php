j��X<?php exit; ?>a:1:{s:7:"content";O:7:"WP_Post":24:{s:2:"ID";i:6887;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-04-04 08:14:10";s:13:"post_date_gmt";s:19:"2017-04-04 07:14:10";s:12:"post_content";s:25211:"[vc_row bg_choice="2" padding_bottom="20px" bg_image="6592" padding_top="100px"][vc_column animation="fadeInLeft" animation_delay="0.5" animation_duration="1" animation_offset="100%"][vc_column_text css=".vc_custom_1490946402951{margin-bottom: 20px !important;}"]
<p class="slider-title">Transforming banking, lending and payments<br class="hide-break" /> with the power of digital<span class="watch-video custom-video-link custom-iframe-style-2">    <a style="color: #fff;" href="https://www.youtube.com/watch?v=PZXX3ldOeF8">watch video <img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/play-button.png" /></a></span></p>
[/vc_column_text][vc_column_text]
<div style="width: 100%; float: left;">
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/cards-and-payment_1b61fcc20fb2fd67e777f021caf5d004.png" /></div>
<div class="banner-text">
<p class="banner-heading">Payment &amp;
Card Solutions</p>

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/banking_86e017efc0167daff8c21fe4444aa8f7.png" /></div>
<div class="banner-text">
<p class="banner-heading">Retail Banking
Solutions</p>

</div>
</div>
<div class="col-md-2 col-sm-4 col-xs-12 first">
<div class="banner-icon"><img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/leasing_f033fa2ee2ce18f1a7db465c15d32599.png" /></div>
<div class="banner-text">
<p class="banner-heading">Lending &amp;
Leasing Solutions</p>

</div>
</div>
</div>
[/vc_column_text][agni_button value="Learn more" url="#learn_more_scroll" style="alt" choice="white" size="lg" radius="2px" margin_top="40px" margin_bottom="30px" class="banner-button"][vc_column_text]
<div style="width: 100%; float: left;">
<div class="home-banner-partners"><img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/rec_d519234a923863eea56519b066a5c3da.png" /></div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding" margin_top="100px"][vc_column][agni_image img_url="6815" alignment="center" img_link="1" class="graph-image"][/vc_column][/vc_row][vc_row padding_top="100px" padding_bottom="70px"][vc_column width="1/3" animation="fadeInUp" el_class="features" animation_delay="0" animation_duration="0.8" animation_offset="100%"][agni_image img_url="6609" img_size="thumbnail" img_link="1" class="features-image"][agni_service text_i_icon="2" title="" class="verinite-features-1"]
<p class="verinite-features-heading">STRATEGIC ADVICE</p>
<p class="verinite-features-subheading">Verinite specializes in providing advice to banks and financial institutions on leveraging IT to empower their business.
<button class="tags">MOBILITY</button><br class="tags-break" /><button class="tags">DIGITAL</button><br class="tags-break" /><button class="tags">CONSULTING SERVICES</button><button class="tags">HELP DESK</button></p>
[/agni_service][/vc_column][vc_column width="1/3" animation="fadeInUp" el_class="features" animation_delay="0.2" animation_duration="0.8" animation_offset="100%"][agni_image img_url="6614" img_size="thumbnail" img_link="1" class="features-image"][agni_service text_i_icon="2" title="" class="verinite-features-1"]
<p class="verinite-features-heading">EXPERT PROJECT DELIVERY</p>
<p class="verinite-features-subheading">Verinite prides on being a delivery centric organization with a culture of honoring commitments and execution excellence.
<button class="tags">APP DEVELOPMENT</button><br class="tags-break" /><button class="tags">APP SECURITY</button><br class="tags-break" /><button class="tags">IT SERVICES</button></p>
[/agni_service][/vc_column][vc_column width="1/3" animation="fadeInUp" el_class="features" animation_delay="0.4" animation_duration="0.8" animation_offset="100%"][agni_image img_url="6612" img_size="thumbnail" img_link="1" class="features-image"][agni_service text_i_icon="2" title="" class="verinite-features-1"]
<p class="verinite-features-heading">HIGH-QUALITY SUPPORT</p>
<p class="verinite-features-subheading">Verinite has truly disrupted the support space by pioneering 1GOAL engagement model to drive accountability with clients.
<button class="tags">HELP DESK</button><br class="tags-break" /><button class="tags">TECH SUPPORT</button><button class="tags">APP MAINTAINANCE</button></p>
[/agni_service][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding" el_id="learn_more_scroll"][vc_column align="center" width="1/2" animation="fadeInLeft" animation_delay="0.2" animation_duration="0.8" animation_offset="100%" el_class="home-video-div"][vc_row_inner css=".vc_custom_1491285176466{margin-right: 0px !important;margin-left: 0px !important;background-image: url(http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Video-BG-1.jpeg?id=6743) !important;}" el_class="verinite-responsive-video"][vc_column_inner align="center" el_class="verinite_video_home"][agni_video video_type="3" iframe_style="2" icon="ion-play" icon_style="background" width="100" height="100" background_color="rgba(240,241,242,0.01)" color="rgba(240,241,242,0.01)" hover_icon_style="background" hover_background_color="rgba(240,241,242,0.01)" hover_color="rgba(240,241,242,0.01)" iframe_url="https://www.youtube.com/watch?v=PZXX3ldOeF8" id="verinite_video_play_icon"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/2" animation="fadeInRight" css=".vc_custom_1488552304965{padding-right: 50px !important;padding-bottom: 25px !important;padding-left: 50px !important;background-color: #f4f9fd !important;}" el_class="responsive-what-to-do" animation_delay="0.2" animation_duration="0.8" animation_offset="100%"][vc_column_text el_class="what_to_do_div"]
<p class="what-we-do">What we do</p>
<p class="business">We enable businesses in finance<br class="hide-break" /> embrace digital.</p>
<p class="business-para">Verinite seeks to establish itself as a provider of high quality IT services for the banking sector. Our vision is based on the foundation of a strong value system and thus helps us to work harmoniously with different stakeholders, delivering value at each step of the way.</p>
<p class="business-para">Our value system is a thread that binds the entire organization together</p>

<div class="list-div">
<ul class="inner-list">
 	<li class="custom-list-style">Integrity</li>
 	<li class="custom-list-style">Excellence</li>
</ul>
</div>
<div class="list-div">
<ul class="inner-list">
 	<li class="custom-list-style">Responsibility</li>
 	<li class="custom-list-style">Execution</li>
</ul>
</div>
<div class="list-div">
<ul class="inner-list">
 	<li class="custom-list-style">Sharing</li>
</ul>
</div>
[/vc_column_text][vc_row_inner el_class="button-row"][vc_column_inner el_class="about-button-column" width="1/3"][agni_button value="MORE ABOUT US" url="http://codewave.co.in/Verinite/about_us/" style="alt" size="lg" radius="3px" margin_top="45px" margin_bottom="15px" class="about-us"][/vc_column_inner][vc_column_inner el_class="home-watch-video-div" width="2/3"][vc_column_text el_class="responsive-watch-video"]
<div class="verinite_video_responsive custom-video-link custom-iframe-style-2"><span class="video-text-responsive">    <a href="https://www.youtube.com/watch?v=PZXX3ldOeF8">watch video <img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/play-button-1.png" /></a></span></div>
[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row padding_top="80px" padding_bottom="60px"][vc_column][vc_column_text css=".vc_custom_1487264947375{margin-bottom: 18px !important;}"]
<p class="custom-heading"><span style="color: #55c0da;">OUR </span>SERVICES</p>
[/vc_column_text][vc_row_inner el_class="verinite-services-row-first"][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="0" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-3"]
<p class="service-text-heading">IT SERVICES</p>
<p class="service-text-sub-heading">Application Development &amp; Integration Application Management <a href="[siteurl]/it-services/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="0.2" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-5"]
<p class="service-text-heading">INDEPENDENT TESTING</p>
<p class="service-text-sub-heading">Domain Service, Technical Service Transformational Service <a href="[siteurl]/independent-testing/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="0.4" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-1"]
<p class="service-text-heading">CONSULTING SERVICES</p>
<p class="service-text-sub-heading">Digital Strategy, Technology Consulting Solution Architecture <a href="[siteurl]/consulting-services/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="verinite-services-row-two"][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="0.6" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-2"]
<p class="service-text-heading">MOBILITY SERVICES</p>
<p class="service-text-sub-heading">Mobile Application Development &amp; Management Mobile Testing Services <a href="[siteurl]/mobility-services/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="0.8" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-4"]
<p class="service-text-heading">INFORMATION SECURITY</p>
<p class="service-text-sub-heading">Security Testing Services, Security Training Services Security Consulting Services  <a href="[siteurl]/information-security/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][vc_column_inner width="1/3" animation="fadeInUp" animation_delay="1" animation_duration="0.8" animation_offset="100%"][agni_service choice="3" icon="fa fa-glass" icon_style="background" radius="50%" hover_icon_style="border" title="" class="services-6"]
<p class="service-text-heading">OUTSOURCED PRODUCT DEVELOPMENT</p>
<p class="service-text-sub-heading">Product Development, Quality Assurance Product Support  <a href="[siteurl]/outsourced-product/"><span class="read-more">read more..</span></a></p>
[/agni_service][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row bg_choice="2" bg_image_position="center top" padding_bottom="50px" bg_image="6588"][vc_column][vc_column_text el_class="verinite-block"]
<p class="verinite-block-text"><span style="font-weight: 900;">verinite</span> is a place where people can unleash their true potential. We<br class="hide-break" /> recognize effort and energies that go into building something<br class="hide-break" /> positive, than the success or failure of the outcome</p>
[/vc_column_text][agni_button value="OUR CULTURE AND VALUES" url="http://codewave.co.in/Verinite/vision-and-values/" style="alt" choice="white" size="lg" alignment="center" radius="2px" class="our-culture"][/vc_column][/vc_row][vc_row margin_top="100px" margin_bottom="40px" el_class="verinite-container"][vc_column][vc_column_text]
<p class="custom-heading"><span style="color: #55c0da;">OUR </span>CASE STUDIES</p>
[/vc_column_text][agni_posts posttype="portfolio" portfolio_fullwidth="1" portfolio_gutter="1" portfolio_hover_style="5" portfolio_categories="case-studies" posts_per_page="3" order="ASC"][agni_button value="VIEW ALL" url="http://codewave.co.in/Verinite/case-studies/" style="alt" size="lg" alignment="center" radius="2px" margin_bottom="20px" class="button-default"][/vc_column][/vc_row][vc_row bg_choice="2" padding_top="100px" padding_bottom="50px" bg_image="6613"][vc_column][agni_testimonials testimonial_categories="verinite-testimonials" circle_avatar="1" alignment="center" testimonial_autoplay="" testimonial_loop=""][/vc_column][/vc_row][vc_row padding_top="70px" padding_bottom="60px" padding_right=" " padding_left=" "][vc_column][vc_column_text]
<p class="custom-heading"><span style="color: #55c0da;">LEADERSHIP </span>TEAM</p>
[/vc_column_text][vc_column_text]
<div class="grid-team " data-team-autoplay="true" data-team-autoplay-timeout="5000" data-team-autoplay-hover="true" data-team-loop="true" data-team-pagination="true">
<div class="row">
<div class="col-xs-12 col-sm-4 col-md-3">
<div id="member-post-4660" class="member-content member-post post-4660 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">ASHISH KATKAR</h5>
<p class="member-designation-text">Co-founder</p>
<p class="member-description">Ashish co-founded Verinite as an organization intensely committed to excellence, values and ethics.</p>

<div class="member-meta">
<div id="mine"><a class="ashish_open" href="#ashish"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3">
<div id="member-post-4662" class="member-content member-post post-4662 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep.jpeg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep.jpeg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep-257x300.jpeg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">SANKHADEEP</h5>
<p class="member-designation-text">Co-founder</p>
<p class="member-description">Sankhadeep is the Head of Engineering in Verinite. He comes with a technical background</p>

<div class="member-meta">
<div id="mine1"><a class="sankadeep_open" href="#sankadeep"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3">
<div id="member-post-6587" class="member-content member-post post-6587 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">Debasis Mohanty</h5>
<p class="member-designation-text">Head of Delivery</p>
<p class="member-description">Debasis Mohanty is the Head of Delivery in Verinite.</p>

<div class="member-meta">
<div id="mine2"><a class="debasis_open" href="#debasis"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3">
<div id="member-post-6823" class="member-content member-post post-6823 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">Jayathi</h5>
<p class="member-designation-text">Manager HR</p>
<p class="member-description">Jayati Chaudhuri joined Verinite Technologies in August 2016 as Manager HR and Operations.</p>

<div class="member-meta">
<div id="mine3"><a class="jayati_open" href="#jayati"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text]
<div class="mainprovnew">
<div id="ashish" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis"></div>
<div class="main-content-provident">
<div class="contentprovmain">
<div class="verinite-col-3"><img src="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg" /></div>
<div class="verinite-col-9">
<h2>ASHISH KATKAR</h2>
Ashish co-founded Verinite as an organization intensely committed to excellence, values and ethics. His innate ability to build strong relationships with customers, nurture robust delivery methods, and develop teams &amp; leaders are the driving force for building a domain focused technology services company.

Ashish's experience in delivery, program and project management spans the course of 12 years with clients such as Citibank, First Data, Arab Financial Services, Network International and HDFC Bank.

Prior to co-founding Verinite, Ashish was the Head of Global Presales &amp; ISEU Business Unit at Attra where he spearheaded the company's growth and global expansion. His dual role meant responsibilities that spanned across new customer acquisition, contract negotiations, managing delivery, account mining and strengthening customer relationships. He was instrumental in generating a revenue of $5 Million USD from customers spread across India, Middle East, Singapore &amp; UK.

Earlier in his career, Ashish worked with Capgemini (erstwhile Kanbay) for over 8 years where he grew rapidly to become one of the youngest managers charged with the responsibility to lead a $4 Million USD portfolio of customer accounts spread across India &amp; the Middle East.

Ashish has an engineering degree in Computer Science from VIT, University of Pune.

</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>
[/vc_column_text][vc_column_text]
<div class="mainprovnew">
<div id="sankadeep" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis"></div>
<div class="main-content-provident">
<div class="contentprovmain">
<div class="verinite-col-3"><img src="../wp-content/uploads/2017/03/Sankhadeep.jpeg" /></div>
<div class="verinite-col-9">
<h2>SANKHADEEP</h2>
Sankhadeep is the Head of Engineering in Verinite. He comes with a technical background with a vast experience on end to end solution design and architecture. He has spent a considerable part of his career in the payments and cards domain designing end to end solutions, innovating products and building interfaces between multiple applications. He has been associated with some of the leading banking clients across multiple geographies (specifically India, Middle East, South East Asia and America). This has helped him to understand the business across these geographies and create a product which addresses the issues in the Collection space across these geographies.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text]
<div class="mainprovnew">
<div id="debasis" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis"></div>
<div class="main-content-provident">
<div class="contentprovmain">
<div class="verinite-col-3"><img src="../../wp-content/uploads/2017/03/Debasis-Mohanty.jpg" /></div>
<div class="verinite-col-9">
<h2>Debasis Mohanty</h2>
Debasis Mohanty is the Head of Delivery in Verinite. Over the last 11 years of his professional career as a Technology and functional expert in Financial Services domain, he has played multiple roles from a Software Developer to Technical Architect to Program Manager. In his current role, he is responsible for managing delivery of Verinite Services Offerings for all Clients.

Prior to joining Verinite, he has worked as a Project Manager at Capgemini. Debasis has managed projects for Financial institutions across Middle East, India and South East Asia. He has been responsible for running a portfolio of projects across Card Management System, Switch, ATM, and POS. He has also led multiple initiatives involving Pre-Sales, Solutioning, system implementation and system migration. This experience helps him to anticipate Clients’ pain points better, and provide proactive solutions during any technology initiatives.

Debasis has a Bachelor of Technology degree from National Institute of Technology, Rourkela.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text]
<div class="mainprovnew">
<div id="jayati" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis"></div>
<div class="main-content-provident">
<div class="contentprovmain">
<div class="verinite-col-3"><img src="../../wp-content/uploads/2017/03/Jayati.jpg" /></div>
<div class="verinite-col-9">
<h2>Jayati Chaudhuri</h2>
Jayati Chaudhuri joined Verinite Technologies in August 2016 as Manager Human Resources and Operations. She spent more than 7 years in Sciformix Technologies in the HR Generalist profile. Her core expertise being into Recruitments, Induction and Reward and Recognition. During her tenure at Sciformix she streamlined the entire Induction process for India location and was responsible for streamlining and handling the Reward and Recognition program of the organization. She has won awards for her contributions towards recruitments, induction and Reward and Recognition.

At Verinite, Jayati will be responsible for handling the entire gamut of Human Resources. Her target for the next couple of years is to get the organization to a 100 level employee strength. Have a gender ratio balance in the organization and most importantly increase the level of employee engagement.

Jayati truly believes in being the face of the organization. She feels that every individual has the ability to achieve what he / she wants, the only differentiator is the will power of the individual. As an interviewer also, she does not limit her job to just interviewing candidates but also guides them when required, whether they are selected or not and they become the brand ambassadors of the company.

Jayati in her free time likes to spend time with her family who are her strength. She has a penchant for Rajasthani paintings and craft. Occasionally she also writes on her blog.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding"][vc_column][agni_call_to_action type="2" value="GET IN TOUCH" url="http://codewave.co.in/Verinite/contact_us/" button_margin_top="50" quote="READY TO GET STARTED?" additional_quote="Send us a brief note on your requirement, one of us will get in touch with you." class="cta"][vc_column_text]<script>
jQuery(document).ready(function () {
jQuery('#ashish').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#sankadeep').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#debasis').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#jayati').popup({
transition: 'all 0.3s',
scrolllock: true
});
});
</script>[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:18:"Verinite home page";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"inherit";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:16:"4222-revision-v1";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 08:14:10";s:17:"post_modified_gmt";s:19:"2017-04-04 07:14:10";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:4222;s:4:"guid";s:48:"http://codewave.co.in/Verinite/4222-revision-v1/";s:10:"menu_order";i:0;s:9:"post_type";s:8:"revision";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}