���X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6904";s:11:"post_author";s:1:"3";s:9:"post_date";s:19:"2015-12-07 10:34:09";s:13:"post_date_gmt";s:19:"2015-12-07 10:34:09";s:12:"post_content";s:5059:"“Revenue is vanity. Profit is sanity. Cash is reality.” – Who knows better about this other than an entrepreneur? Anyone who runs business will know that cash is like oxygen.

Over last 4 years, I have been running Verinite as a lean start-up and one of the key focus areas has been maintaining a healthy cash flow that not only supports current operations but enables growth as well.

Start-ups and small businesses often struggle when it comes to cash flow. It is intertwined with a number of challenges, which make it extremely difficult to manage. We faced same issue when we started off. One of the biggest contributor to the problem was ineffective management of the account receivable collections from our clients.

Upon review we realized that we couldn’t continue in the same manner and needed to make interesting changes to the A/R collections process. Here’s what we did:

We became authoritative

According to the norm, in the ITeS industry, you offer a payment term of 30 days. However, that’s not carved in stone. You are well within your rights to negotiate those terms to favor you. We decided to be aggressive about payment terms and honestly tell them about the terms. Being resourceful and innovative helped us to a great extent in this endeavor. We did not hide anything including the fact that we are a budding startup. That showed them clearly that cash flow was a very important aspect of our business. We were authoritative when we asked them for 15-day payment terms as opposed to 30-day terms. Even though it seems like a risk, we are proud that we took this decision because thanks to this simple step, a great chunk of our clients (60%) pays us within 20 days! To accelerate cash flow even further, we made use of incentives. Clients who paid us within 10 days were given 2.5% discount. Two of our clients now pay us within 8 days. Both these clients are also startups and just like us, they know how important every penny is!

Takeaways:

Be aggressive while negotiating
Be transparent
Offer incentives
Our invoicing process was clear and accurate

<p style="text-align:center";><img src="[siteurl]/wp-content/uploads/2017/04/invoice-002.jpg" alt="null" /></p>

The invoice is a crucial part of cash flow. A lot rides on the clarity you provide in your invoice. Go through the entire contract thoroughly before furnishing the invoice. We did that and went to the extent of having it reviewed in a 3-tier process. The perfect invoice has no ambiguity or loophole. This detailed review process ensures that. We also gave account managers the responsibility to release the final invoice and to collect the payment.

Takeaways:

Go by the contract
Have a detailed reviewing process
Give the responsibility to account managers
We built a strong internal collections process and system

We realized that only when we have a strong internal collections process and system could the cash flow process be smooth. Hence, we started working on it. We created an AR aging report and gave the finance department the responsibility to follow-up with the account managers every week.

We mapped the client payment process

Having a clear process internally is just one part of ensuring clarity. The other part is to understand the client payment process. Now, this differs with the client. That is why it is essential to dig deeper. Understand how the payment process works in the client organization. Also, mapping the stakeholders involved within this process and building healthy relationships with them helps to a considerable extent. We always made it a point to stay in touch with the project owner so that he pushes the payment internally.

Takeaways:

Understand payment process in the client organization
Build relationships with key stakeholders
Have the project owner push for payments
Special Case Scenario: In case the client is facing cash flow troubles


<p style="text-align:center";><img src="[siteurl]/wp-content/uploads/2017/04/cashflow-2-002.jpg" alt="null" /></p>

While some clients are inherently troublesome, other clients have genuine reasons. They may have cash flow problems themselves or there may be hindrances. One of our clients faced a similar issue. They were not able to pay us since they were raising a Series A. We came to a mutual agreement with them so they could pay us only a percentage of the total due within the agreed timelines and the remaining after Series A.  This support that we gave them lead them to pay us within 10 days henceforth. Plus, the loyalty that we managed to build with them was a huge bonus.

Takeaways:

Be supportive
You can see that simple, innovative steps helped us improve our cash flow while keeping our client relationships in tact. Being a startup is a difficult but the road can be smoothened with authoritative steps and transparency.

Remember a sale is not a successful sale until you collect the money from client. Do share your thoughts or innovative ways that have helped in boosting A/R collections.";s:10:"post_title";s:54:"HOW WE IMPROVED CASH FLOW BY BOOSTING A/R COLLECTIONS?";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:52:"how-we-improved-cash-flow-by-boosting-ar-collections";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 11:40:36";s:17:"post_modified_gmt";s:19:"2017-04-04 10:40:36";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:58:"http://codewave.co.in/Verinite/?post_type=blog&#038;p=6904";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"blog";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}