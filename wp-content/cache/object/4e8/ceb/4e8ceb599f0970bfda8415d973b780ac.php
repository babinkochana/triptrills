k��X<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:4825;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-02-27 05:58:04";s:13:"post_date_gmt";s:19:"2017-02-27 05:58:04";s:12:"post_content";s:23559:"[vc_row bg_choice="2" bg_image_repeat="no-repeat" bg_image_size="auto" bg_image_position="right center" margin_top="30px" bg_image="4829" padding_right="0px" padding_left="0px" margin_right="30px" el_class="about_us_banner"][vc_column animation="fadeInLeft" animation_delay="0.5" animation_duration="1"][vc_column_text]
<div class="about_heading_small">
<h1>Inch wide, mile deep focus</h1>
</div>
<div class="about_heading_big">
<h1>Revolutionizing Banking
Technology</h1>
</div>
<div class="about_tags_outer">
<div class="tags-custom tag1">Passionate People</div>
<div class="tags-custom tag2">Firm Values</div>
<div class="tags-custom tag3">Innovative Ideas</div>
<div class="tags-custom tag4">Deep Domain Expertise</div>
<div class="tags-custom tag5">Emerging markets</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding" bg_choice="2" bg_image="6711"][vc_column white="white" align="center" width="1/4" css=".vc_custom_1489116055380{margin-top: 70px !important;margin-bottom: 70px !important;border-right-width: 1px !important;border-right-color: #e8e8e8 !important;}" el_class="mile_stone1"][agni_milestone style="2" icon="fa fa-calendar-o" icon_size="" mile_font_size="30" white="white" mile="2011" title="May - Founded" class="founded"][/vc_column][vc_column white="white" align="center" width="1/4" css=".vc_custom_1489055447064{margin-top: 70px !important;margin-bottom: 70px !important;border-right-width: 1px !important;border-right-color: #e8e8e8 !important;}" el_class="mile_stone2"][agni_milestone style="2" icon="fa fa-calendar-o" icon_size="" mile_font_size="30" white="white" mile="200" title="Projects Completed" class="projects"][/vc_column][vc_column white="white" align="center" width="1/4" css=".vc_custom_1489055454734{margin-top: 70px !important;margin-bottom: 70px !important;border-right-width: 1px !important;border-right-color: #e8e8e8 !important;}" el_class="mile_stone3"][agni_milestone style="2" icon="fa fa-calendar-o" icon_size="" mile_font_size="30" mile_suffix="+" white="white" mile="55" title="Team Members" class="members"][/vc_column][vc_column white="white" align="center" width="1/4" css=".vc_custom_1489055477624{margin-top: 70px !important;margin-bottom: 70px !important;}" el_class="mile_stone4"][agni_milestone style="2" icon="fa fa-calendar-o" icon_size="" mile_font_size="30" mile_suffix="+" white="white" mile="30" title="Clients" class="partners"][/vc_column][/vc_row][vc_row][vc_column el_class="about_empty1"][vc_empty_space height="60px"][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding" el_class="client_people"][vc_column width="1/2" animation="slideInLeft" animation_duration="0.8" animation_offset="95%"][agni_image img_url="4943" alignment="center" img_link="1"][/vc_column][vc_column width="1/2" animation="slideInRight" animation_duration="0.8" animation_offset="95%" el_class="client_people_content"][vc_empty_space height="80px" el_class="clienttext_space"][vc_column_text]
<div class="about_section">
<p class="about_client">CLIENT FIRST, PEOPLE FIRST</p>
<p class="about_clienttext">A philosophy that is framed on our walls and in our minds. It has lead us to where we are today and we believe will continue to lead us to where we want to be.</p>
<p class="about_learn"><a href="[siteurl]/vision-and-values/">Learn more about our values &amp; vision</a></p>

</div>
[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column el_class="about_empty2"][vc_empty_space height="60px"][/vc_column][/vc_row][vc_row padding_top="80" el_class="about_what_we_do_top" bg_color="#f9ffff"][vc_column align="center" el_class="about_what_we_do"][vc_column_text]<span style="color: #58c0dd;">WHAT</span> WE DO[/vc_column_text][/vc_column][vc_column width="1/4" animation="fadeInUp" animation_duration="0.8" animation_offset="95%" animation_delay="0" el_class="service_1"][agni_service choice="6" icon="fa fa-heart" icon_style="border" border_color="#696969" hover_icon_style="border" hover_radius="50%" hover_border_color="#696969" hover_color="#58bfde" title="CONSULTING SERVICES" class="about_consulting"]Verinite specializes in providing strategic advice to banks and financial institutions on leveraging <span id="consulting_read_more" class="about_read_more"> read more...</span><span id="consulting_more"> IT to empower their business. With a core specialization in the financial services industry our talent pool comes from diverse backgrounds with a unique capability to speak both business and technology which allows us to deliver accurate advice and strategic solutions in the ever changing business environment.<span id="consulting_show_less" class="about_read_more"> show less</span></span>

<script>
jQuery('#consulting_read_more').click(function(){
jQuery("#consulting_more").css('display','inline'); 
jQuery('#consulting_read_more').hide();
});
jQuery('#consulting_show_less').click(function(){
jQuery("#consulting_more").hide() 
jQuery('#consulting_read_more').css('display','inline');
});
</script>[/agni_service][/vc_column][vc_column width="1/4" animation="fadeInUp" animation_duration="0.8" animation_offset="95%" animation_delay="0.2" el_class="service_2"][agni_service choice="6" icon="fa fa-heart" icon_style="border" border_color="#696969" hover_icon_style="border" hover_radius="50%" hover_border_color="#696969" hover_color="#58bfde" title="ENGINEERING SERVICES" class="about_engineering"]Verinite prides on being a delivery centric organization with a culture of honoring <span id="engineering_read_more" class="about_read_more"> read more...</span><span id="engineering_more">commitments and execution excellence. Clients engage us for a specific experience we've had or implement newest technology or business process but mostly we are engaged because we are experts at Project Delivery; that's what we do best. It's what we call Integrated Project Delivery Faster, Cheaper, Better and more Sustainable.<span id="engineering_show_less" class="about_read_more"> show less</span></span>

<script>
jQuery('#engineering_read_more').click(function(){
jQuery("#engineering_more").css('display','inline'); 
jQuery('#engineering_read_more').hide();
});
jQuery('#engineering_show_less').click(function(){
jQuery("#engineering_more").hide();
jQuery('#engineering_read_more').css('display','inline'); 
});
</script>[/agni_service][/vc_column][vc_column width="1/4" animation="fadeInUp" animation_delay="0.4" animation_duration="0.8" animation_offset="95%" el_class="service_3"][agni_service choice="6" icon="fa fa-heart" icon_style="border" border_color="#696969" hover_icon_style="border" hover_radius="50%" hover_border_color="#696969" hover_color="#58bfde" title="SUPPORT SERVICES" class="about_support"]Verinite has truly disrupted the support space by pioneering 1GOAL engagement model <span id="support_read_more" class="about_read_more"> read more...</span><span id="support_more">to drive accountability and result-based relationship with clients. Traditional engagement models do not satisfactorily meet the current client needs for vendor control and budget management. The 1GOAL model ties success with clear and measurable outcomes, thus allowing client and Verinite to focus on same goals (one goal) as true partners.<span id="support_show_less" class="about_read_more"> show less</span></span>

<script>
jQuery('#support_read_more').click(function(){
jQuery("#support_more").css('display','inline'); 
jQuery('#support_read_more').hide();
});
jQuery('#support_show_less').click(function(){
jQuery("#support_more").hide();
jQuery('#support_read_more').css('display','inline'); 
});
</script>[/agni_service][/vc_column][vc_column width="1/4" animation="slideInUp" animation_delay="0.6" animation_duration="0.8" animation_offset="95%" el_class="service_4"][agni_service choice="6" icon="fa fa-heart" icon_style="border" border_color="#696969" color="#6b6b6d" hover_icon_style="border" hover_radius="50%" hover_border_color="#696969" hover_color="#58bfde" title="HOME GROWN PRODUCTS" class="about_home_grown"]Verinite has over the years, designed and developed home grown products to help <span id="homegrown_read_more" class="about_read_more"> read more...</span><span id="homegrown_more"> businesses in the banking industry, specially in payments and cards space. KolleXor, a Verinite product intelligently automates payment collections and makes the process hassle free, friendly and non-intrusive with the power of technology. With this, you could save time, money and efforts to manually remind customers to pay dues. GoSelfie, another Verinite product enables quick launch of selfie printed banking cards by providing a ready to deploy web solution with required features for customer image upload and other related operations support.<span id="homegrown_show_less" class="about_read_more"> show less</span></span>

<script>
jQuery('#homegrown_read_more').click(function(){
jQuery("#homegrown_more").css('display','inline'); 
jQuery('#homegrown_read_more').hide();
});
jQuery('#homegrown_show_less').click(function(){
jQuery("#homegrown_more").hide();
jQuery('#homegrown_read_more').css('display','inline'); 
});
</script>[/agni_service][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column" bg_choice="2" bg_image_repeat="no-repeat" bg_image="5002"][vc_column white="white" css=".vc_custom_1489058619494{padding-top: 35px !important;padding-left: 60px !important;}" el_class="about_focus"][vc_column_text]
<div class="about_quotes">Our focus on emerging markets brings us
great opportunities to simply banking with tech,
where it matters</div>
<div class="about_quotes_mobile">Our focus on emerging markets brings usgreat opportunities to simply banking with tech, where it matters</div>
[/vc_column_text][/vc_column][/vc_row][vc_row padding_top="70px" padding_bottom="60px"][vc_column][vc_column_text css=".vc_custom_1488529261558{margin-bottom: 42px !important;}"]
<p class="custom-heading"><span style="color: #55c0da;">LEADERSHIP </span>TEAM</p>

[/vc_column_text][vc_column_text]
<div class="grid-team " data-team-autoplay="true" data-team-autoplay-timeout="5000" data-team-autoplay-hover="true" data-team-loop="true" data-team-pagination="true">
<div class="row">
<div class="col-xs-12 col-sm-4 col-md-3 team_member_div">
<div id="member-post-4660" class="member-content member-post post-4660 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">ASHISH KATKAR</h5>
<p class="member-designation-text">Co-founder</p>
<p class="member-description">Ashish co-founded Verinite as an organization intensely committed to excellence, values and ethics.</p>

<div class="member-meta">
<div id="mine"><a class="ashish_open" href="#ashish"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3 team_member_div">
<div id="member-post-4662" class="member-content member-post post-4662 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep.jpeg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep.jpeg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep-257x300.jpeg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">SANKHADEEP</h5>
<p class="member-designation-text">Co-founder</p>
<p class="member-description">Sankhadeep is the Head of Engineering in Verinite. He comes with a technical background</p>

<div class="member-meta">
<div id="mine1"><a class="sankadeep_open" href="#sankadeep"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3 team_member_div">
<div id="member-post-6587" class="member-content member-post post-6587 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">Debasis Mohanty</h5>
<p class="member-designation-text">Head of Delivery</p>
<p class="member-description">Debasis Mohanty is the Head of Delivery in Verinite.</p>

<div class="member-meta">
<div id="mine2"><a class="debasis_open" href="#debasis"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-xs-12 col-sm-4 col-md-3 team_member_div">
<div id="member-post-6823" class="member-content member-post post-6823 team type-team status-publish hentry team_types-verinite-leadership">
<div class="member-thumbnail"><img class="attachment-cookie-grid-thumbnail size-cookie-grid-thumbnail" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati.jpg" sizes="(max-width: 262px) 100vw, 262px" srcset="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati.jpg 262w, http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati-257x300.jpg 257w" alt="" width="262" height="306" /></div>
<div class="member-caption-content">
<div class="member-content">
<div class="member-content-details">
<h5 class="member-title">Jayathi</h5>
<p class="member-designation-text">Manager HR</p>
<p class="member-description">Jayati Chaudhuri joined Verinite Technologies in August 2016 as Manager HR and Operations.</p>

<div class="member-meta">
<div id="mine3"><a class="jayati_open" href="#jayati"><button class="btn btn-alt btn-default leader_read_more">Read more</button></a></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text el_class="member_popup"]
<div class="mainprovnew">
<div id="ashish" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis ashish_close"><img class="close-btn-team" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/04/close.png" /></div>
<div class="main-content-provident">
<div class="contentprovmain vc_col-sm-12">
<div class="vc_col-sm-4"><img style="width: 100%;" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/02/Ashish-Katkar.jpg" /></div>
<div class="vc_col-sm-8">
<h2 class="member_name">ASHISH KATKAR</h2>
Ashish co-founded Verinite as an organization intensely committed to excellence, values and ethics. His innate ability to build strong relationships with customers, nurture robust delivery methods, and develop teams &amp; leaders are the driving force for building a domain focused technology services company.

Ashish's experience in delivery, program and project management spans the course of 12 years with clients such as Citibank, First Data, Arab Financial Services, Network International and HDFC Bank.

Prior to co-founding Verinite, Ashish was the Head of Global Presales &amp; ISEU Business Unit at Attra where he spearheaded the company's growth and global expansion. His dual role meant responsibilities that spanned across new customer acquisition, contract negotiations, managing delivery, account mining and strengthening customer relationships. He was instrumental in generating a revenue of $5 Million USD from customers spread across India, Middle East, Singapore &amp; UK.

Earlier in his career, Ashish worked with Capgemini (erstwhile Kanbay) for over 8 years where he grew rapidly to become one of the youngest managers charged with the responsibility to lead a $4 Million USD portfolio of customer accounts spread across India &amp; the Middle East.

Ashish has an engineering degree in Computer Science from VIT, University of Pune.

</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://cdn.rawgit.com/vast-engineering/jquery-popup-overlay/1.7.13/jquery.popupoverlay.js"></script>[/vc_column_text][vc_column_text el_class="member_popup"]
<div class="mainprovnew">
<div id="sankadeep" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis sankadeep_close"><img class="close-btn-team" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/04/close.png" /></div>
<div class="main-content-provident">
<div class="contentprovmain vc_col-sm-12">
<div class="vc_col-sm-4"><img style="width: 100%;" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Sankhadeep.jpeg" /></div>
<div class="vc_col-sm-8">
<h2 class="member_name">SANKHADEEP</h2>
Sankhadeep is the Head of Engineering in Verinite. He comes with a technical background with a vast experience on end to end solution design and architecture. He has spent a considerable part of his career in the payments and cards domain designing end to end solutions, innovating products and building interfaces between multiple applications. He has been associated with some of the leading banking clients across multiple geographies (specifically India, Middle East, South East Asia and America). This has helped him to understand the business across these geographies and create a product which addresses the issues in the Collection space across these geographies.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text el_class="member_popup"]
<div class="mainprovnew">
<div id="debasis" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis debasis_close"><img class="close-btn-team" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/04/close.png" /></div>
<div class="main-content-provident">
<div class="contentprovmain vc_col-sm-12">
<div class=" vc_col-sm-4"><img style="width: 100%;" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Debasis-Mohanty.jpg" /></div>
<div class=" vc_col-sm-8">
<h2 class="member_name">Debasis Mohanty</h2>
Debasis Mohanty is the Head of Delivery in Verinite. Over the last 11 years of his professional career as a Technology and functional expert in Financial Services domain, he has played multiple roles from a Software Developer to Technical Architect to Program Manager. In his current role, he is responsible for managing delivery of Verinite Services Offerings for all Clients.

Prior to joining Verinite, he has worked as a Project Manager at Capgemini. Debasis has managed projects for Financial institutions across Middle East, India and South East Asia. He has been responsible for running a portfolio of projects across Card Management System, Switch, ATM, and POS. He has also led multiple initiatives involving Pre-Sales, Solutioning, system implementation and system migration. This experience helps him to anticipate Clients’ pain points better, and provide proactive solutions during any technology initiatives.

Debasis has a Bachelor of Technology degree from National Institute of Technology, Rourkela.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][vc_column_text el_class="member_popup"]
<div class="mainprovnew">
<div id="jayati" class="white_content" style="display: none;">
<div class="provident-innerpop">
<div class="closethis jayati_close"><img class="close-btn-team" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/04/close.png" /></div>
<div class="main-content-provident">
<div class="contentprovmain vc_col-sm-12">
<div class="vc_col-sm-4"><img style="width: 100%;" src="http://codewave.co.in/Verinite/wp-content/uploads/2017/03/Jayati.jpg" /></div>
<div class="vc_col-sm-8">
<h2 class="member_name">Jayati Chaudhuri</h2>
Jayati Chaudhuri joined Verinite Technologies in August 2016 as Manager Human Resources and Operations. She spent more than 7 years in Sciformix Technologies in the HR Generalist profile. Her core expertise being into Recruitments, Induction and Reward and Recognition. During her tenure at Sciformix she streamlined the entire Induction process for India location and was responsible for streamlining and handling the Reward and Recognition program of the organization. She has won awards for her contributions towards recruitments, induction and Reward and Recognition.

At Verinite, Jayati will be responsible for handling the entire gamut of Human Resources. Her target for the next couple of years is to get the organization to a 100 level employee strength. Have a gender ratio balance in the organization and most importantly increase the level of employee engagement.

Jayati truly believes in being the face of the organization. She feels that every individual has the ability to achieve what he / she wants, the only differentiator is the will power of the individual. As an interviewer also, she does not limit her job to just interviewing candidates but also guides them when required, whether they are selected or not and they become the brand ambassadors of the company.

Jayati in her free time likes to spend time with her family who are her strength. She has a penchant for Rajasthani paintings and craft. Occasionally she also writes on her blog.

</div>
</div>
</div>
</div>
</div>
</div>
[/vc_column_text][/vc_column][/vc_row][vc_row bg_choice="2" bg_image="4618" padding_top="80px" padding_bottom="45px" el_class="verinite_service_testimonials"][vc_column][agni_testimonials testimonial_categories="verinite-testimonials" circle_avatar="1" alignment="center" testimonial_autoplay="" testimonial_loop=""][/vc_column][/vc_row][vc_row fullwidth="has-fullwidth-column-no-padding"][vc_column][agni_call_to_action type="2" value="GET IN TOUCH" url="http://codewave.co.in/Verinite/contact_us/" style="alt" choice="white" size="lg" button_margin_top="" quote="READY TO GET STARTED?" additional_quote="Send us a brief note on your requirement, one of us will get in touch with you." class="cta"][vc_column_text el_class="member_popup"]<script>
jQuery(document).ready(function () {
jQuery('#ashish').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#sankadeep').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#debasis').popup({
transition: 'all 0.3s',
scrolllock: true
});
jQuery('#jayati').popup({
transition: 'all 0.3s',
scrolllock: true
});
});
</script>[/vc_column_text][/vc_column][/vc_row]";s:10:"post_title";s:8:"About Us";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:8:"about_us";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-04-04 12:05:05";s:17:"post_modified_gmt";s:19:"2017-04-04 11:05:05";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:44:"http://codewave.co.in/Verinite/?page_id=4825";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}